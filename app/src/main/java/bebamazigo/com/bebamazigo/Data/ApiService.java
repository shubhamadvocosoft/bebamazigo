package bebamazigo.com.bebamazigo.Data;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.util.StaticData;

/**
 * Created by Advosoft2 on 1/22/2018.
 */

public class ApiService {

    /*public static String BASE_URL = "http://139.59.71.150:3006/api/";*/
    public static String BASE_URL = "https://api.mybebamzigo.com/api/";
    /*public static String ONLY_BASE_URL = "http://139.59.71.150:3006";*/
    public static String ONLY_BASE_URL = "https://api.mybebamzigo.com";
    /*public static String BASE_URL_IMAGE = "http://139.59.71.150:3006";*/
    public static String BASE_URL_IMAGE = "https://api.mybebamzigo.com";
    public static String LOG_IN = BASE_URL + "People/login";
    public static String LOG_OUT = BASE_URL + "People/logout";
    public static String SIGN_UP = BASE_URL + "People/signup";
    public static String GET_INFO = BASE_URL + "People/getMyInfo?access_token=";
    public static String EDIT_PROFILE = BASE_URL + "People/editProfile";
    public static String UPLOAD_PROFILE_PIC = BASE_URL + "People/uploadProfilePic";
    public static String UPDATE_FCM = BASE_URL + "People/updateToken";
    public static String UPDATE_DRIVER_LOCATION = BASE_URL + "People/updateDriverLoc";
    public static String GET_DRIVER_LOCATION = BASE_URL + "People/getDriverCurrentLoc?access_token=";

    public static String VERIFY_MOBILE = BASE_URL + "Otps/verifyMobile";
    public static String RESEND_OTP = BASE_URL + "Otps/resendOtp";

    public static String FORGET_PASSWORD = BASE_URL + "People/resetPasswordRequest";
    public static String SUBMIT_NEW_PASSWORD = BASE_URL + "People/updateResetPassword";


    public static String GIVE_RATING = BASE_URL + "Ratings/giveRating";

    public static String GET_NEAR_DRIVER = BASE_URL + "People/findNearDriver?access_token=";
    public static String REQUEST_DRIVER = BASE_URL + "Requestfordrivers/makeRequest";
    public static String CONFIRM_REQUEST = BASE_URL + "Requestfordrivers/confirmByDriver";
    public static String GET_REQUEST = BASE_URL + "Requestfordrivers/getRequests?access_token=";
    public static String GET_Notification = BASE_URL + "Notifications/getNotifications?access_token=";

    public static String GET_VEHICLE_TYPE = BASE_URL + "VehicleTypes/getVehicleType?access_token=";
    public static String ADD_VEHICLE = BASE_URL + "Vehicles/addVehicle";
    public static String GET_MY_VEHICLE = BASE_URL + "Vehicles/getMyVehicles?access_token=";
    public static String GET_VEHICLE_Req = BASE_URL + "Requestfordrivers/getRequests?access_token=";
    public static String Vehicle_ACCEPT = BASE_URL + "Requestfordrivers/confirmByDriver";
    public static String Vehicle_Reject = BASE_URL + "Requestfordrivers/rejectByDriver";

    public static String ADD_JOB = BASE_URL + "Jobs/createJob";
    public static String JOB_PRICING = BASE_URL + "Jobs/getJobPricing?access_token=";
    public static String GET_JOB = BASE_URL + "Jobs/getJobs?access_token=";
    public static String GET_JOB_ID = BASE_URL + "Jobs/getJobById?access_token=";
    public static String JOB_ACCEPT = BASE_URL + "JobApplies/applyForJob";
    public static String GET_NEARBY_JOB = BASE_URL + "Jobs/getNearJobs?access_token=";
    public static String START_JOB = BASE_URL + "Jobs/startJob";
    public static String END_JOB = BASE_URL + "Jobs/endJob";
    public static String COMPLETE_JOB = BASE_URL + "Jobs/completeJob?access_token=";
    public static String CANCEL_JOB = BASE_URL + "Jobs/cancelJob?access_token=";
    public static String Get_VehicalById = BASE_URL + "Vehicles/getVehicleById?access_token=";
    public static String Get_VehicalAssesPrice = BASE_URL + "Vehicles/getAssessPrice?access_token=";
    public static String VehicleAssesment = BASE_URL + "Vehicles/assessment";
    public static String PeopleInfo = BASE_URL + "People/getPeopleInfo?access_token=";
    public static String Remove_DRIVER = BASE_URL + "Requestfordrivers/removeVehicle";

    public static String FuelReq = BASE_URL + "Fuelrequests/getFuelRequest?access_token=";
    public static String MakeFuelReq = BASE_URL + "Fuelrequests/makeFuelRequest";


    public static String GET_WALLET = BASE_URL + "Wallets/getWallet?access_token=";
    public static String GET_Recipt = BASE_URL + "Jobs/getReceiptJobs?access_token=";
    public static String GET_Transaction = BASE_URL + "Transactions/getTransaction";
    public static String Add_MONEY_MPESA = BASE_URL + "Wallets/addMPesaMoney";
    public static String Add_MONEY_CARD = BASE_URL + "Wallets/addCardMoney";
    public static String Withdraw_Money = BASE_URL + "Wallets/withdrawMoney";
    public static String GET_MPesaAddMoneyResponse = BASE_URL + "Transactions/getSingleTransaction?access_token=";


    public static String FB_TOKEN = BASE_URL_IMAGE + "/auth/facebook/token";
    private static ApiService apiService;
    ProgressDialog dialog;
    private Activity activity;

    private ApiService(Activity activity) {
        this.activity = activity;
        dialog = new ProgressDialog(activity);
    }

    private ApiService() {

    }

    public static synchronized ApiService getInstance(Activity act) {
        if (apiService == null) {
            apiService = new ApiService(act);
        }
        return apiService;
    }

    public static synchronized ApiService getInstance() {
        if (apiService == null) {
            apiService = new ApiService();
        }
        return apiService;
    }


    public void makePostCall(final Context context, String url, final JSONObject params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                MyApplication.stop_progressbar();
                responseCallback.onResponseSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        JSONObject errorObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        if (errorObject.optJSONObject("error").optString("code").equalsIgnoreCase("LOGIN_FAILED_MOBILE_NOT_VERIFIED")) {
                            MyApplication.setSharedPrefString(StaticData.SP_USER_ID, errorObject.optJSONObject("error").optJSONObject("details").optString("userId"));
                            MyApplication.getInstance().resendOTP(activity,errorObject.optJSONObject("error").optJSONObject("details").optString("userId"));
                        }
                        MyApplication.showToast(context, errorObject.optJSONObject("error").optString("message"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                return params;
            }

            @Override
            public byte[] getBody() {
                return params.toString().getBytes();
            }

        };
        MyApplication.getInstance().addToRequestQueue(request);
    }

    public void makePostCallAuth(final Context context, String url, final JSONObject params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                MyApplication.stop_progressbar();
                responseCallback.onResponseSuccess(response);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        JSONObject errorObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        MyApplication.showToast(context, errorObject.optJSONObject("error").optString("message"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Authorization", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
                Log.d("Authorization", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
                return params;
            }

            @Override
            public byte[] getBody() {
                if (params != null)
                    return params.toString().getBytes();
                else
                    return null;
            }

        };
        MyApplication.getInstance().addToRequestQueue(request);
    }

    public void makeGetCallArray(final Context context, String url, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                JSONObject object = new JSONObject();
                try {
                    object.put("array", response);
                    MyApplication.stop_progressbar();
                    responseCallback.onResponseSuccess(object);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        JSONObject errorObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        MyApplication.showToast(context, errorObject.optJSONObject("error").optString("message"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);
            }
        });
        MyApplication.getInstance().addToRequestQueue(request);

    }

    public void multipartCall(final Context context, String url, final Map<String, DataPart> params, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST, url, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                try {
                    MyApplication.stop_progressbar();
                    responseCallback.onResponseSuccess(new JSONObject(new String(response.data)));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                try {
                    String errorMsg = new String(error.networkResponse.data, "UTF-8");
                    JSONObject obj=new JSONObject(errorMsg);
                    MyApplication.showToast(context, obj.getJSONObject("error").optString("message"));
                    Log.e("Error Volley", errorMsg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                responseCallback.onError(error);

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Authorization", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                return params;
            }
        };
        MyApplication.getInstance().addToRequestQueue(multipartRequest);
    }

    public void makeGetCall(final Context context, String url, final OnResponse responseCallback) {
        MyApplication.initialize_progressbar(context);
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        MyApplication.stop_progressbar();
                        responseCallback.onResponseSuccess(response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyApplication.stop_progressbar();
                if (error.networkResponse != null) {
                    try {
                        JSONObject errorObject = new JSONObject(new String(error.networkResponse.data, "UTF-8"));
                        MyApplication.showToast(context, errorObject.optJSONObject("error").optString("message"));
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                responseCallback.onError(error);
            }
        });
        MyApplication.getInstance().addToRequestQueue(getRequest);
    }


    public interface OnResponse {
        void onResponseSuccess(JSONObject response);

        void onError(VolleyError volleyError);

    }
}
