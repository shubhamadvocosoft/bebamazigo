package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    @SerializedName("title")
    @Expose
    String title;
    @SerializedName("body")
    @Expose
    String body;
    @SerializedName("Date")
    @Expose
    String Date;
    @SerializedName("type")
    @Expose
    String type;
    @SerializedName("peopleId")
    @Expose
    String peopleId;
    @SerializedName("badge")
    @Expose
    String badge;
    @SerializedName("sound")
    @Expose
    String sound;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }

    public String getBadge() {
        return badge;
    }

    public void setBadge(String badge) {
        this.badge = badge;
    }

    public String getSound() {
        return sound;
    }

    public void setSound(String sound) {
        this.sound = sound;
    }
}
