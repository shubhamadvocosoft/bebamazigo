package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FuelModel implements Serializable {
    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("jobId")
    @Expose
    String jobId;
    @SerializedName("driverId")
    @Expose
    String driverId;
    @SerializedName("mobileNumber")
    @Expose
    String mobileNumber;
    @SerializedName("amount")
    @Expose
    Integer amount;
    @SerializedName("status")
    @Expose
    String status;
    @SerializedName("createdAt")
    @Expose
    String createdAt;
    @SerializedName("updatedAt")
    @Expose
    String updatedAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
