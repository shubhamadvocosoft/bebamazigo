package bebamazigo.com.bebamazigo.Model;


public class FirebaseLocation {

    private String currntLong;

    private String jobId;

    private String jobStatus;

    private String driverId;

    private String currntLatt;

    private long timestamp;

    public void setCurrntLong(String currntLong) {
        this.currntLong = currntLong;
    }

    public String getCurrntLong() {
        return currntLong;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    public String getJobStatus() {
        return jobStatus;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setCurrntLatt(String currntLatt) {
        this.currntLatt = currntLatt;
    }

    public String getCurrntLatt() {
        return currntLatt;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public FirebaseLocation(String jobId, String currntLatt, String currntLong, String jobStatus, String driverId, long timestamp) {
        this.currntLong = currntLong;
        this.jobId = jobId;
        this.jobStatus = jobStatus;
        this.driverId = driverId;
        this.currntLatt = currntLatt;
        this.timestamp = timestamp;
    }

    public FirebaseLocation() {
    }


}