package bebamazigo.com.bebamazigo.Model;


import com.google.gson.annotations.SerializedName;


public class UserModel{

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("created")
	private String created;

	@SerializedName("id")
	private String id;

	@SerializedName("ttl")
	private int ttl;

	@SerializedName("userId")
	private String userId;

	@SerializedName("user")
	private User user;

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	public void setCreated(String created){
		this.created = created;
	}

	public String getCreated(){
		return created;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTtl(int ttl){
		this.ttl = ttl;
	}

	public int getTtl(){
		return ttl;
	}

	public void setUserId(String userId){
		this.userId = userId;
	}

	public String getUserId(){
		return userId;
	}

	public void setUser(User user){
		this.user = user;
	}

	public User getUser(){
		return user;
	}

	@Override
 	public String toString(){
		return 
			"UserModel{" + 
			"access_token = '" + accessToken + '\'' + 
			",created = '" + created + '\'' + 
			",id = '" + id + '\'' + 
			",ttl = '" + ttl + '\'' + 
			",userId = '" + userId + '\'' + 
			",user = '" + user + '\'' + 
			"}";
		}
}