package bebamazigo.com.bebamazigo.Model;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;


public class TripModel implements Serializable {

    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("weight")
    @Expose
    private String weight;
    @SerializedName("source")
    @Expose
    private Source source;
    @SerializedName("destination")
    @Expose
    private Destination destination;
    @SerializedName("isApproved")
    @Expose
    private Boolean isApproved;
    @SerializedName("budget")
    @Expose
    private Integer budget;
    @SerializedName("netPrice")
    @Expose
    private Integer netPrice;
    @SerializedName("typeOfVehicle")
    @Expose
    private String typeOfVehicle;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("didCustGiveRate")
    @Expose
    private Boolean didCustGiveRate;
    @SerializedName("didDriveGiveRate")
    @Expose
    private Boolean didDriveGiveRate;
    @SerializedName("natureOfGoods")
    @Expose
    private String natureOfGoods;
    @SerializedName("selectedRoute")
    @Expose
    private String selectedRoute;
    @SerializedName("requiredType")
    @Expose
    private String requiredType;
    @SerializedName("creatorId")
    @Expose
    private String creatorId;
    @SerializedName("valueOfGoods")
    @Expose
    private Integer valueOfGoods;
    @SerializedName("typeOfJob")
    @Expose
    private String typeOfJob;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("duration")
    @Expose
    private Duration duration;
    @SerializedName("vehicleTonnage")
    @Expose
    private Integer vehicleTonnage;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("jobOtp")
    @Expose
    private Integer jobOtp;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("dist")
    @Expose
    private Dist dist;
    @SerializedName("creator")
    @Expose
    private Creator creator;
    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("endDate")
    @Expose
    private String endDate;
    @SerializedName("vehicleType")
    @Expose
    private VehicalType vehicalType;

    public VehicalType getVehicalType() {
        return vehicalType;
    }

    public void setVehicalType(VehicalType vehicalType) {
        this.vehicalType = vehicalType;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Boolean getIsApproved() {
        return isApproved;
    }

    public void setIsApproved(Boolean isApproved) {
        this.isApproved = isApproved;
    }

    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    public Integer getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(Integer netPrice) {
        this.netPrice = netPrice;
    }

    public String getTypeOfVehicle() {
        return typeOfVehicle;
    }

    public void setTypeOfVehicle(String typeOfVehicle) {
        this.typeOfVehicle = typeOfVehicle;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDidCustGiveRate() {
        return didCustGiveRate;
    }

    public void setDidCustGiveRate(Boolean didCustGiveRate) {
        this.didCustGiveRate = didCustGiveRate;
    }

    public Boolean getDidDriveGiveRate() {
        return didDriveGiveRate;
    }

    public void setDidDriveGiveRate(Boolean didDriveGiveRate) {
        this.didDriveGiveRate = didDriveGiveRate;
    }

    public String getNatureOfGoods() {
        return natureOfGoods;
    }

    public void setNatureOfGoods(String natureOfGoods) {
        this.natureOfGoods = natureOfGoods;
    }

    public String getSelectedRoute() {
        return selectedRoute;
    }

    public void setSelectedRoute(String selectedRoute) {
        this.selectedRoute = selectedRoute;
    }

    public String getRequiredType() {
        return requiredType;
    }

    public void setRequiredType(String requiredType) {
        this.requiredType = requiredType;
    }

    public String getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(String creatorId) {
        this.creatorId = creatorId;
    }

    public Integer getValueOfGoods() {
        return valueOfGoods;
    }

    public void setValueOfGoods(Integer valueOfGoods) {
        this.valueOfGoods = valueOfGoods;
    }

    public String getTypeOfJob() {
        return typeOfJob;
    }

    public void setTypeOfJob(String typeOfJob) {
        this.typeOfJob = typeOfJob;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Integer getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(Integer vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getJobOtp() {
        return jobOtp;
    }

    public void setJobOtp(Integer jobOtp) {
        this.jobOtp = jobOtp;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Dist getDist() {
        return dist;
    }

    public void setDist(Dist dist) {
        this.dist = dist;
    }

    public Creator getCreator() {
        return creator;
    }

    public void setCreator(Creator creator) {
        this.creator = creator;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getApproved() {
        return isApproved;
    }

    public void setApproved(Boolean approved) {
        isApproved = approved;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {

        return new ToStringBuilder(this).append("title", title).append("weight", weight).append("source", source).append("destination", destination).append("isApproved", isApproved).append("budget", budget).append("netPrice", netPrice).append("typeOfVehicle", typeOfVehicle).append("date", date).append("description", description).append("didCustGiveRate", didCustGiveRate).append("didDriveGiveRate", didDriveGiveRate).append("natureOfGoods", natureOfGoods).append("selectedRoute", selectedRoute).append("requiredType", requiredType).append("creatorId", creatorId).append("valueOfGoods", valueOfGoods).append("typeOfJob", typeOfJob).append("distance", distance).append("duration", duration).append("vehicleTonnage", vehicleTonnage).append("status", status).append("jobOtp", jobOtp).append("createdAt", createdAt).append("updatedAt", updatedAt).append("dist", dist).append("creator", creator).append("id", id).append("endDate", endDate).append("driverId", driverId).toString();
    }


    public class Destination implements Serializable {

        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("location")
        @Expose
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

    }

    public class Distance implements Serializable {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("value")
        @Expose
        private String value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }


    public class Duration implements Serializable {

        @SerializedName("text")
        @Expose
        private String text;
        @SerializedName("value")
        @Expose
        private String value;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

    public class Location implements Serializable {

        @SerializedName("lat")
        @Expose
        private Double lat;
        @SerializedName("lng")
        @Expose
        private Double lng;

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lng) {
            this.lng = lng;
        }

    }

    public class SelectedRoute implements Serializable {

        @SerializedName("lat")
        @Expose
        private String lat;
        @SerializedName("lng")
        @Expose
        private String lng;

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public LatLng getLatLng() {
            return new LatLng(Double.parseDouble(lat), Double.parseDouble(lng));
        }
    }

    public class Source implements Serializable {

        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("location")
        @Expose
        private Location location;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

    }

    public class Creator implements Serializable {

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("name")
        @Expose
        private String name;

        @SerializedName("mobile")
        @Expose
        private String mobile;

        @SerializedName("profileImage")
        @Expose
        private String profileImage;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("email", email).append("name", name).append("mobile", mobile).append("profileImage", profileImage).toString();
        }

    }

    public class Dist implements Serializable {

        @SerializedName("calculated")
        @Expose
        private Double calculated;

        public Double getCalculated() {
            return calculated;
        }

        public void setCalculated(Double calculated) {
            this.calculated = calculated;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("calculated", calculated).toString();
        }

    }

    public class VehicalType implements Serializable {
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("imgUrl")
        @Expose
        private String imgUrl;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("bodyType")
        @Expose
        private List<BodyType> bodyTypes;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<BodyType> getBodyTypes() {
            return bodyTypes;
        }

        public void setBodyTypes(List<BodyType> bodyTypes) {
            this.bodyTypes = bodyTypes;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }

    public class BodyType implements Serializable {
        @SerializedName("name")
        @Expose
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}