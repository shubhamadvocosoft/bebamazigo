package bebamazigo.com.bebamazigo.Model;

/**
 * Created by Advosoft2 on 2/12/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;
import java.util.List;


public class Vehicle implements Serializable {

    private static final long serialVersionUID = 1L;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("vehicleNumber")
    @Expose
    private String vehicleNumber;

    public Vehicaldata getVehicaldata() {
        return vehicaldata;
    }

    public void setVehicaldata(Vehicaldata vehicaldata) {
        this.vehicaldata = vehicaldata;
    }

    @SerializedName("vehicle")
    @Expose
    private  Vehicaldata vehicaldata;

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @SerializedName("owner")
    @Expose
    private Owner owner;

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("registrationNumber")
    @Expose
    private String registrationNumber;
    @SerializedName("vehicleMake")
    @Expose
    private String vehicleMake;
    @SerializedName("ton")
    @Expose
    private Integer ton;
    @SerializedName("bodyType")
    @Expose
    private String bodyType;
    @SerializedName("logBook")
    @Expose
    private List<String> logBook = null;
    @SerializedName("ntsaIns")
    @Expose
    private List<String> ntsaIns = null;
    @SerializedName("insuranceCopy")
    @Expose
    private List<String> insuranceCopy = null;
    @SerializedName("approveStatus")
    @Expose
    private String approveStatus;
    @SerializedName("isDriverAlloted")
    @Expose
    private Boolean isDriverAlloted;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("vehicleId")
    @Expose
    private String vehicleId;
    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("vehicleTypeId")
    @Expose
    private String vehicleTypeId;
    @SerializedName("vehicleModel")
    @Expose
    private String vehicleModel;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("assesmentStart")
    @Expose
    private String assesmentStart;
    @SerializedName("assessmentEnd")
    @Expose
    private String assessmentEnd;
    @SerializedName("adminId")
    @Expose
    private String adminId;
    @SerializedName("vehicleType")
    @Expose
    private VehicleType vehicleType;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public Integer getTon() {
        return ton;
    }

    public void setTon(Integer ton) {
        this.ton = ton;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public List<String> getLogBook() {
        return logBook;
    }

    public void setLogBook(List<String> logBook) {
        this.logBook = logBook;
    }

    public List<String> getNtsaIns() {
        return ntsaIns;
    }

    public void setNtsaIns(List<String> ntsaIns) {
        this.ntsaIns = ntsaIns;
    }

    public List<String> getInsuranceCopy() {
        return insuranceCopy;
    }

    public void setInsuranceCopy(List<String> insuranceCopy) {
        this.insuranceCopy = insuranceCopy;
    }

    public String getApproveStatus() {
        return approveStatus;
    }

    public void setApproveStatus(String approveStatus) {
        this.approveStatus = approveStatus;
    }

    public Boolean getIsDriverAlloted() {
        return isDriverAlloted;
    }

    public void setIsDriverAlloted(Boolean isDriverAlloted) {
        this.isDriverAlloted = isDriverAlloted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAssesmentStart() {
        return assesmentStart;
    }

    public void setAssesmentStart(String assesmentStart) {
        this.assesmentStart = assesmentStart;
    }

    public String getAssessmentEnd() {
        return assessmentEnd;
    }

    public void setAssessmentEnd(String assessmentEnd) {
        this.assessmentEnd = assessmentEnd;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("type", type).append("vehicleNumber", vehicleNumber).append("registrationNumber", registrationNumber).append("vehicleMake", vehicleMake).append("ton", ton).append("bodyType", bodyType).append("logBook", logBook).append("ntsaIns", ntsaIns).append("insuranceCopy", insuranceCopy).append("approveStatus", approveStatus).append("isDriverAlloted", isDriverAlloted).append("id", id).append("vehicleId", vehicleId).append("ownerId", ownerId).append("vehicleTypeId", vehicleTypeId).append("vehicleModel", vehicleModel).append("createdAt", createdAt).append("assesmentStart", assesmentStart).append("assessmentEnd", assessmentEnd).append("adminId", adminId).append("vehicleType", vehicleType).toString();
    }

    public class Ton implements Serializable {
        private static final long serialVersionUID = 1L;

        @SerializedName("value")
        @Expose
        private Integer value;
        @SerializedName("unit")
        @Expose
        private String unit;

        public Integer getValue() {
            return value;
        }

        public void setValue(Integer value) {
            this.value = value;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("value", value).append("unit", unit).toString();
        }

    }


    public class VehicleType implements Serializable {
        private static final long serialVersionUID = 1L;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("make")
        @Expose
        private Boolean make;
        @SerializedName("model")
        @Expose
        private Boolean model;
        @SerializedName("bodyType")
        @Expose
        private List<Object> bodyType = null;
        @SerializedName("ton")
        @Expose
        private List<Ton> ton = null;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("jobType")
        @Expose
        private List<String> jobType = null;
        @SerializedName("imgUrl")
        @Expose
        private String imgUrl;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Boolean getMake() {
            return make;
        }

        public void setMake(Boolean make) {
            this.make = make;
        }

        public Boolean getModel() {
            return model;
        }

        public void setModel(Boolean model) {
            this.model = model;
        }

        public List<Object> getBodyType() {
            return bodyType;
        }

        public void setBodyType(List<Object> bodyType) {
            this.bodyType = bodyType;
        }

        public List<Ton> getTon() {
            return ton;
        }

        public void setTon(List<Ton> ton) {
            this.ton = ton;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public List<String> getJobType() {
            return jobType;
        }

        public void setJobType(List<String> jobType) {
            this.jobType = jobType;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("name", name).append("make", make).append("model", model).append("bodyType", bodyType).append("ton", ton).append("type", type).append("id", id).append("jobType", jobType).append("imgUrl", imgUrl).toString();
        }

    }

    public class Owner implements Serializable{
        @SerializedName("id")
        @Expose
        String id;
        @SerializedName("email")
        @Expose
        String email;
        @SerializedName("companyName")
        @Expose
        String companyName;
        @SerializedName("profileImage")
        @Expose
        String profileImage;
        @SerializedName("name")
        @Expose
        String name;
        @SerializedName("mobile")
        @Expose
        String mobile;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }

    public class Vehicaldata implements Serializable{
        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("type")
        @Expose
        private String type;

        public String getVehicalNumber() {
            return vehicalNumber;
        }

        public void setVehicalNumber(String vehicalNumber) {
            this.vehicalNumber = vehicalNumber;
        }

        @SerializedName("vehicleNumber")
        @Expose
        private String vehicalNumber;

        @SerializedName("driverId")
        @Expose
        private String driverId;
        @SerializedName("registrationNumber")
        @Expose
        private String registrationNumber;
        @SerializedName("vehicleMake")
        @Expose
        private String vehicleMake;
        @SerializedName("ton")
        @Expose
        private Integer ton;
        @SerializedName("bodyType")
        @Expose
        private String bodyType;
        @SerializedName("logBook")
        @Expose
        private List<String> logBook = null;
        @SerializedName("ntsaIns")
        @Expose
        private List<String> ntsaIns = null;
        @SerializedName("insuranceCopy")
        @Expose
        private List<String> insuranceCopy = null;
        @SerializedName("approveStatus")
        @Expose
        private String approveStatus;
        @SerializedName("isDriverAlloted")
        @Expose
        private Boolean isDriverAlloted;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("ownerId")
        @Expose
        private String ownerId;
        @SerializedName("vehicleTypeId")
        @Expose
        private String vehicleTypeId;
        @SerializedName("vehicleModel")
        @Expose
        private String vehicleModel;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("assesmentStart")
        @Expose
        private String assesmentStart;
        @SerializedName("assessmentEnd")
        @Expose
        private String assessmentEnd;
        @SerializedName("adminId")
        @Expose
        private String adminId;

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getRegistrationNumber() {
            return registrationNumber;
        }

        public void setRegistrationNumber(String registrationNumber) {
            this.registrationNumber = registrationNumber;
        }

        public String getVehicleMake() {
            return vehicleMake;
        }

        public void setVehicleMake(String vehicleMake) {
            this.vehicleMake = vehicleMake;
        }

        public Integer getTon() {
            return ton;
        }

        public void setTon(Integer ton) {
            this.ton = ton;
        }

        public String getBodyType() {
            return bodyType;
        }

        public void setBodyType(String bodyType) {
            this.bodyType = bodyType;
        }

        public List<String> getLogBook() {
            return logBook;
        }

        public void setLogBook(List<String> logBook) {
            this.logBook = logBook;
        }

        public List<String> getNtsaIns() {
            return ntsaIns;
        }

        public void setNtsaIns(List<String> ntsaIns) {
            this.ntsaIns = ntsaIns;
        }

        public List<String> getInsuranceCopy() {
            return insuranceCopy;
        }

        public void setInsuranceCopy(List<String> insuranceCopy) {
            this.insuranceCopy = insuranceCopy;
        }

        public String getApproveStatus() {
            return approveStatus;
        }

        public void setApproveStatus(String approveStatus) {
            this.approveStatus = approveStatus;
        }

        public Boolean getDriverAlloted() {
            return isDriverAlloted;
        }

        public void setDriverAlloted(Boolean driverAlloted) {
            isDriverAlloted = driverAlloted;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getOwnerId() {
            return ownerId;
        }

        public void setOwnerId(String ownerId) {
            this.ownerId = ownerId;
        }

        public String getVehicleTypeId() {
            return vehicleTypeId;
        }

        public void setVehicleTypeId(String vehicleTypeId) {
            this.vehicleTypeId = vehicleTypeId;
        }

        public String getVehicleModel() {
            return vehicleModel;
        }

        public void setVehicleModel(String vehicleModel) {
            this.vehicleModel = vehicleModel;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getAssesmentStart() {
            return assesmentStart;
        }

        public void setAssesmentStart(String assesmentStart) {
            this.assesmentStart = assesmentStart;
        }

        public String getAssessmentEnd() {
            return assessmentEnd;
        }

        public void setAssessmentEnd(String assessmentEnd) {
            this.assessmentEnd = assessmentEnd;
        }

        public String getAdminId() {
            return adminId;
        }

        public void setAdminId(String adminId) {
            this.adminId = adminId;
        }
    }
}