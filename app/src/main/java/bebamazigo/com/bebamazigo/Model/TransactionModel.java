package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TransactionModel implements Serializable {
    @SerializedName("paymentStatus")
    @Expose
    String paymentStatus;
    @SerializedName("type")
    @Expose
    String type;
    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("peopleId")
    @Expose
    String peopleId;
    @SerializedName("method")
    @Expose
    String method;
    @SerializedName("reason")
    @Expose
    String reason;
    @SerializedName("createdAt")
    @Expose
    String createdAt;
    @SerializedName("amount")
    @Expose
    int amount;

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeopleId() {
        return peopleId;
    }

    public void setPeopleId(String peopleId) {
        this.peopleId = peopleId;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
