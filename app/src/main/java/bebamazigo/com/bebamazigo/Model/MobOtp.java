package bebamazigo.com.bebamazigo.Model;


import com.google.gson.annotations.SerializedName;

public class MobOtp{

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("otp")
	private int otp;

	@SerializedName("expireAt")
	private String expireAt;

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setExpireAt(String expireAt){
		this.expireAt = expireAt;
	}

	public String getExpireAt(){
		return expireAt;
	}

	@Override
 	public String toString(){
		return 
			"MobOtp{" + 
			"createdAt = '" + createdAt + '\'' + 
			",otp = '" + otp + '\'' + 
			",expireAt = '" + expireAt + '\'' + 
			"}";
		}
}