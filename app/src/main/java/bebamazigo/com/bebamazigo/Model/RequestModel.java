package bebamazigo.com.bebamazigo.Model;

/**
 * Created by Advosoft2 on 2/15/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class RequestModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("ownerId")
    @Expose
    private String ownerId;
    @SerializedName("vehicleId")
    @Expose
    private String vehicleId;
    @SerializedName("driverId")
    @Expose
    private String driverId;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("owner")
    @Expose
    private Owner owner;
    @SerializedName("vehicle")
    @Expose
    private Vehicle vehicle;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("status", status).append("id", id).append("ownerId", ownerId).append("vehicleId", vehicleId).append("driverId", driverId).append("createdAt", createdAt).append("updatedAt", updatedAt).append("owner", owner).append("vehicle", vehicle).toString();
    }


    public class Owner {

        @SerializedName("realm")
        @Expose
        private String realm;
        @SerializedName("username")
        @Expose
        private String username;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("emailVerified")
        @Expose
        private Boolean emailVerified;
        @SerializedName("firebaseTokens")
        @Expose
        private List<Object> firebaseTokens = null;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("updatedAt")
        @Expose
        private String updatedAt;
        @SerializedName("rating")
        @Expose
        private Rating rating;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("provider")
        @Expose
        private String provider;
        @SerializedName("mobileVerified")
        @Expose
        private Boolean mobileVerified;
        @SerializedName("userMobile")
        @Expose
        private String userMobile;
        @SerializedName("userEmail")
        @Expose
        private String userEmail;

        public String getRealm() {
            return realm;
        }

        public void setRealm(String realm) {
            this.realm = realm;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public Boolean getEmailVerified() {
            return emailVerified;
        }

        public void setEmailVerified(Boolean emailVerified) {
            this.emailVerified = emailVerified;
        }

        public List<Object> getFirebaseTokens() {
            return firebaseTokens;
        }

        public void setFirebaseTokens(List<Object> firebaseTokens) {
            this.firebaseTokens = firebaseTokens;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public Rating getRating() {
            return rating;
        }

        public void setRating(Rating rating) {
            this.rating = rating;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getProvider() {
            return provider;
        }

        public void setProvider(String provider) {
            this.provider = provider;
        }

        public Boolean getMobileVerified() {
            return mobileVerified;
        }

        public void setMobileVerified(Boolean mobileVerified) {
            this.mobileVerified = mobileVerified;
        }

        public String getUserMobile() {
            return userMobile;
        }

        public void setUserMobile(String userMobile) {
            this.userMobile = userMobile;
        }

        public String getUserEmail() {
            return userEmail;
        }

        public void setUserEmail(String userEmail) {
            this.userEmail = userEmail;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("realm", realm).append("username", username).append("email", email).append("companyName", companyName).append("emailVerified", emailVerified).append("firebaseTokens", firebaseTokens).append("createdAt", createdAt).append("updatedAt", updatedAt).append("rating", rating).append("id", id).append("name", name).append("provider", provider).append("mobileVerified", mobileVerified).append("userMobile", userMobile).append("userEmail", userEmail).toString();
        }


        public class Rating {

            @SerializedName("totalUsers")
            @Expose
            private Integer totalUsers;
            @SerializedName("totalRating")
            @Expose
            private Integer totalRating;
            @SerializedName("avgRating")
            @Expose
            private Integer avgRating;

            public Integer getTotalUsers() {
                return totalUsers;
            }

            public void setTotalUsers(Integer totalUsers) {
                this.totalUsers = totalUsers;
            }

            public Integer getTotalRating() {
                return totalRating;
            }

            public void setTotalRating(Integer totalRating) {
                this.totalRating = totalRating;
            }

            public Integer getAvgRating() {
                return avgRating;
            }

            public void setAvgRating(Integer avgRating) {
                this.avgRating = avgRating;
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this).append("totalUsers", totalUsers).append("totalRating", totalRating).append("avgRating", avgRating).toString();
            }

        }
    }
}