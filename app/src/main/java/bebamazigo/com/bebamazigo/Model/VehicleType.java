package bebamazigo.com.bebamazigo.Model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Advosoft2 on 1/31/2018.
 */

public class VehicleType {


    private String id;

    private String[] bodyType;

    private boolean model;

    private String name;

    private String[] ton;

    private String type;

    private boolean make;
    private boolean isMachinery;

    public VehicleType(JSONObject object) {

        id = object.optString("id");
        model = object.optBoolean("model");
        make = object.optBoolean("make");
        name = object.optString("name");
        type = object.optString("type");

        isMachinery = type.equalsIgnoreCase("Machinery");

        JSONArray bodyTypeArray = object.optJSONArray("bodyType");
        JSONArray tonArray = object.optJSONArray("ton");

        if (bodyTypeArray != null && bodyTypeArray.length() > 0) {
            bodyType = new String[bodyTypeArray.length() + 1];
            bodyType[0] = "Body Type";
            for (int i = 0; i < bodyTypeArray.length(); i++) {
                bodyType[i + 1] = bodyTypeArray.optJSONObject(i).optString("name");
            }
        }

        if (tonArray != null && tonArray.length() > 0) {
            ton = new String[tonArray.length() + 1];
            ton[0] = "Tonnage";
            for (int i = 0; i < tonArray.length(); i++) {
                ton[i + 1] = tonArray.optJSONObject(i).optString("value");
            }
        }


    }

    public boolean isModel() {
        return model;
    }

    public boolean isMake() {
        return make;
    }

    public boolean isMachinery() {
        return isMachinery;
    }

    public String getId() {
        return id;
    }

    public String[] getBodyType() {
        return bodyType;
    }

    public boolean getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public String[] getTon() {
        return ton;
    }

    public String getType() {
        return type;
    }

    public boolean getMake() {
        return make;
    }
}