package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by Advosoft2 on 2/15/2018.
 */

public class DriverModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("realm")
    @Expose
    private String realm;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("mobile")
    @Expose
    private String mobile;
    @SerializedName("location")
    @Expose
    private Location location;
    @SerializedName("isVehicleAlloted")
    @Expose
    private Boolean isVehicleAlloted;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("rating")
    @Expose
    private Rating rating;
    @SerializedName("requestStatus")
    @Expose
    private Boolean requestStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Boolean getIsVehicleAlloted() {
        return isVehicleAlloted;
    }

    public void setIsVehicleAlloted(Boolean isVehicleAlloted) {
        this.isVehicleAlloted = isVehicleAlloted;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Rating getRating() {
        return rating;
    }

    public void setRating(Rating rating) {
        this.rating = rating;
    }

    public Boolean getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(Boolean requestStatus) {
        this.requestStatus = requestStatus;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("realm", realm).append("email", email).append("name", name).append("mobile", mobile).append("location", location).append("isVehicleAlloted", isVehicleAlloted).append("profileImage", profileImage).append("rating", rating).append("requestStatus", requestStatus).toString();
    }


    public class Location {

        @SerializedName("lng")
        @Expose
        private Double lng;
        @SerializedName("lat")
        @Expose
        private Double lat;

        public Double getLng() {
            return lng;
        }

        public void setLng(Double lng) {
            this.lng = lng;
        }

        public Double getLat() {
            return lat;
        }

        public void setLat(Double lat) {
            this.lat = lat;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this).append("lng", lng).append("lat", lat).toString();
        }

    }
}