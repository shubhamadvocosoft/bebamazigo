package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.SerializedName;

public class Rating {

    @SerializedName("totalUsers")
    private int totalUsers;

    @SerializedName("totalRating")
    private int totalRating;

    @SerializedName("avgRating")
    private float avgRating;

    public int getTotalUsers() {
        return totalUsers;
    }

    public void setTotalUsers(int totalUsers) {
        this.totalUsers = totalUsers;
    }

    public int getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(int totalRating) {
        this.totalRating = totalRating;
    }

    public float getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(float avgRating) {
        this.avgRating = avgRating;
    }

    @Override
    public String toString() {
        return
                "Rating{" +
                        "totalUsers = '" + totalUsers + '\'' +
                        ",totalRating = '" + totalRating + '\'' +
                        ",avgRating = '" + avgRating + '\'' +
                        "}";
    }
}