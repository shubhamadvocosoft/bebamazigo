package bebamazigo.com.bebamazigo.Model;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class User{

	@SerializedName("mobOtp")
	private MobOtp mobOtp;

	@SerializedName("governmentIds")
	private List<String> governmentIds;

	@SerializedName("companyName")
	private String companyName;

	@SerializedName("rating")
	private Rating rating;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("licenceIds")
	private List<String> licenceIds;

	@SerializedName("mobileVerified")
	private boolean mobileVerified;

	@SerializedName("profileImage")
	private String profileImage;

	@SerializedName("isVehicleAlloted")
	private boolean isVehicleAlloted;

	@SerializedName("emailVerified")
	private boolean emailVerified;

	@SerializedName("createdAt")
	private String createdAt;

	@SerializedName("jobId")
	private String jobId;

	@SerializedName("name")
	private String name;

	@SerializedName("realm")
	private String realm;

	@SerializedName("location")
	private Location location;

	@SerializedName("id")
	private String id;

	@SerializedName("vehicleId")
	private String vehicleId;

	@SerializedName("email")
	private String email;

	@SerializedName("firebaseTokens")
	private List<String> firebaseTokens;

	@SerializedName("updatedAt")
	private String updatedAt;

	public void setMobOtp(MobOtp mobOtp){
		this.mobOtp = mobOtp;
	}

	public MobOtp getMobOtp(){
		return mobOtp;
	}

	public void setGovernmentIds(List<String> governmentIds){
		this.governmentIds = governmentIds;
	}

	public List<String> getGovernmentIds(){
		return governmentIds;
	}

	public void setCompanyName(String companyName){
		this.companyName = companyName;
	}

	public String getCompanyName(){
		return companyName;
	}

	public void setRating(Rating rating){
		this.rating = rating;
	}

	public Rating getRating(){
		return rating;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setLicenceIds(List<String> licenceIds){
		this.licenceIds = licenceIds;
	}

	public List<String> getLicenceIds(){
		return licenceIds;
	}

	public void setMobileVerified(boolean mobileVerified){
		this.mobileVerified = mobileVerified;
	}

	public boolean isMobileVerified(){
		return mobileVerified;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setIsVehicleAlloted(boolean isVehicleAlloted){
		this.isVehicleAlloted = isVehicleAlloted;
	}

	public boolean isIsVehicleAlloted(){
		return isVehicleAlloted;
	}

	public void setEmailVerified(boolean emailVerified){
		this.emailVerified = emailVerified;
	}

	public boolean isEmailVerified(){
		return emailVerified;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setJobId(String jobId){
		this.jobId = jobId;
	}

	public String getJobId(){
		return jobId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setRealm(String realm){
		this.realm = realm;
	}

	public String getRealm(){
		return realm;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public Location getLocation(){
		return location;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setVehicleId(String vehicleId){
		this.vehicleId = vehicleId;
	}

	public String getVehicleId(){
		return vehicleId;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setFirebaseTokens(List<String> firebaseTokens){
		this.firebaseTokens = firebaseTokens;
	}

	public List<String> getFirebaseTokens(){
		return firebaseTokens;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	@Override
 	public String toString(){
		return 
			"User{" + 
			"mobOtp = '" + mobOtp + '\'' + 
			",governmentIds = '" + governmentIds + '\'' + 
			",companyName = '" + companyName + '\'' + 
			",rating = '" + rating + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",licenceIds = '" + licenceIds + '\'' + 
			",mobileVerified = '" + mobileVerified + '\'' + 
			",profileImage = '" + profileImage + '\'' + 
			",isVehicleAlloted = '" + isVehicleAlloted + '\'' + 
			",emailVerified = '" + emailVerified + '\'' + 
			",createdAt = '" + createdAt + '\'' + 
			",jobId = '" + jobId + '\'' + 
			",name = '" + name + '\'' + 
			",realm = '" + realm + '\'' + 
			",location = '" + location + '\'' + 
			",id = '" + id + '\'' + 
			",vehicleId = '" + vehicleId + '\'' + 
			",email = '" + email + '\'' + 
			",firebaseTokens = '" + firebaseTokens + '\'' + 
			",updatedAt = '" + updatedAt + '\'' + 
			"}";
		}
}