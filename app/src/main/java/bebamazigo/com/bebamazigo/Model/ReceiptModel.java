package bebamazigo.com.bebamazigo.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReceiptModel implements Serializable {
    @SerializedName("title")
    @Expose
    String title;
    @SerializedName("date")
    @Expose
    String date;
    @SerializedName("netPrice")
    @Expose
    int netPrice;
    @SerializedName("vehicleType")
    @Expose
    VehicalType vehicalType;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(int netPrice) {
        this.netPrice = netPrice;
    }

    public VehicalType getVehicalType() {
        return vehicalType;
    }

    public void setVehicalType(VehicalType vehicalType) {
        this.vehicalType = vehicalType;
    }

    public class VehicalType implements Serializable{
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("imgUrl")
        @Expose
        private String imgUrl;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @SerializedName("type")
        @Expose
        private String type;

        public List<BodyType> getBodyTypes() {
            return bodyTypes;
        }

        public void setBodyTypes(List<BodyType> bodyTypes) {
            this.bodyTypes = bodyTypes;
        }

        @SerializedName("bodyType")
        @Expose
        private List<BodyType> bodyTypes;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }
    }

    public class BodyType implements Serializable{
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @SerializedName("name")
        @Expose
        private String name;

    }
}
