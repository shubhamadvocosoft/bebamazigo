package bebamazigo.com.bebamazigo.util;

import bebamazigo.com.bebamazigo.Model.RouteModel;
import bebamazigo.com.bebamazigo.Model.TripModel;

/**
 * Created by Advosoft2 on 1/22/2018.
 */

public class StaticData {
    public static String NO_INTERNET = "Not connected to internet";
    public static String BLANK = "Field can't remain blank";
    public static String API_KEY = "AIzaSyDj5ZcywjBLiKbYaEZelmnh1ErRzxDaRx0";

    public static String SP_ACCESS_TOKEN = "token";
    public static String SP_IS_LOGIN = "isLoggedIn";
    public static String SP_USER_NAME = "name";
    public static String SP_USER_IMAGE = "imageProfile";
    public static String SP_USER_ID = "userId";
    public static String SP_ID = "id";
    public static String SP_EMAIL = "email";
    public static String SP_MOBILE = "mobile";
    public static String SP_REALM = "realm";
    public static String SP_COMPANY_NAME = "company_name";
    public static String SP_IS_FACEBOOK = "isFacebook";
    public static String SP_CURRENT_JOB = "curJob";
    public static String SP_FB_USER_ID = "fbUser";
    public static String SP_FB_DP = "fbProfile";

    public static String SP_CURRENT_LAT = "lat";
    public static String SP_CURRENT_LONG = "lng";
    public static String SP_NEW_LAT = "newLat";
    public static String SP_NEW_LONG = "newLng";

    public static String FCM_TOKEN = "fcm";

    public static RouteModel CURRENT_ROUTE;
    public static TripModel CURRENT_TRIP;
}
