package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.RequestModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    int selectedPosition = 0;
    private ArrayList<RequestModel> list;
    private String vehicleId;
    private int rowLayout;
    private Activity activity;
    private boolean isOpen = false;


    public RequestAdapter(ArrayList<RequestModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_request;
        this.activity = activity;
    }


    public void setData(ArrayList<RequestModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null)
            list.clear();
    }


    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new RequestAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RequestAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_name.setText(list.get(position).getOwner().getName());
        viewHolder.txt_vehicle.setText(list.get(position).getVehicle().getType());

        viewHolder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmRequest(position);
            }
        });
    }

    private void confirmRequest(final int pos) {
        JSONObject param = new JSONObject();
        try {
            param.put("requestId", list.get(pos).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.CONFIRM_REQUEST, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                activity.finish();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_name, txt_vehicle, txt_accept;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_vehicle = itemView.findViewById(R.id.txt_vehicle);
            txt_accept = itemView.findViewById(R.id.txt_accept);


        }
    }


}
