package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Model.TransactionModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolder> {
    int selectedPosition = 0;
    private List<TransactionModel> list;
    private int rowLayout;
    private Activity activity;


    public WalletAdapter(ArrayList<TransactionModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_wallet;
        this.activity = activity;

    }


    public void setData(List<TransactionModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public WalletAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new WalletAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(WalletAdapter.ViewHolder viewHolder, final int position) {
        if (list.get(position).getType().compareTo("received") == 0) {
            viewHolder.txt_received.setText("Received");
            viewHolder.img.setImageResource(R.drawable.kesrecevie_3x);
        } else if (list.get(position).getType().compareTo("pending") == 0) {
            viewHolder.txt_received.setText("Pending");
        } else if (list.get(position).getType().compareTo("send") == 0) {
            viewHolder.txt_received.setText("Paid");
            viewHolder.img.setImageResource(R.drawable.kespaid_3x);
        }

        if (list.get(position).getPaymentStatus().compareTo("pending") == 0) {
            viewHolder.txt_status.setText("Pending");
            viewHolder.img.setImageResource(R.drawable.keserror_3x);
        } else if (list.get(position).getPaymentStatus().compareTo("completed") == 0) {
            viewHolder.txt_status.setText("Completed");

            if (list.get(position).getType().compareTo("received") == 0) {
                viewHolder.img.setImageResource(R.drawable.kesrecevie_3x);
            } else if (list.get(position).getType().compareTo("send") == 0) {
                viewHolder.img.setImageResource(R.drawable.kespaid_3x);
            }
        }

        viewHolder.txt_add_payment.setText(list.get(position).getReason());

        viewHolder.txt_amount.setText(list.get(position).getAmount() + "");
        viewHolder.txt_date.setText(MyApplication.utcToDate(list.get(position).getCreatedAt()) + " " + MyApplication.utcToTime(list.get(position).getCreatedAt()));

       /* viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.CURRENT_TRIP = list.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("jobId", list.get(position).getId());
                MyApplication.activityStart(activity, ShowTripActivity.class, false, bundle);
            }
        });*/
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_received, txt_add_payment, txt_date, txt_status, txt_amount;
        ImageView img;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_received = (TextView) itemView.findViewById(R.id.txt_received);
            txt_add_payment = (TextView) itemView.findViewById(R.id.txt_add_payment);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_status = (TextView) itemView.findViewById(R.id.txt_status);
            txt_amount = (TextView) itemView.findViewById(R.id.txt_amount);
            img = (ImageView) itemView.findViewById(R.id.img);

        }
    }
}
