package bebamazigo.com.bebamazigo.Adapter;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Activity.VehicalDetailActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.DriverModel;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ViewHolder> {

    int selectedPosition = 0;
    private ArrayList<Vehicle> list;
    private ArrayList<DriverModel> driverList;
    private int rowLayout;
    private FragmentActivity activity;
    private boolean isOpen = false;
    private Gson gson = new Gson();

    public VehicleAdapter(ArrayList<Vehicle> list, FragmentActivity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_vehicle;
        this.activity = activity;

    }


    public void setData(ArrayList<Vehicle> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public VehicleAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new VehicleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final VehicleAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_type.setText(list.get(position).getType());

        Picasso.get()
                .load(ApiService.BASE_URL_IMAGE + list.get(position).getVehicleType().getImgUrl())
                .fit()
                .into(viewHolder.img_vehicle);

        if (TextUtils.isEmpty(list.get(position).getVehicleNumber()))
            viewHolder.txt_vehicle_no.setVisibility(View.GONE);
        else
            viewHolder.txt_vehicle_no.setText(list.get(position).getVehicleNumber());


        if (TextUtils.isEmpty(list.get(position).getRegistrationNumber()))
            viewHolder.txt_reg_no.setVisibility(View.GONE);
        else
            viewHolder.txt_reg_no.setText(list.get(position).getRegistrationNumber());


        viewHolder.txt_approve.setText(list.get(position).getApproveStatus());

        if ((list.get(position).getIsDriverAlloted())) {
            viewHolder.txt_driver.setText("Yes");
        } else {
            viewHolder.txt_driver.setText("No");
        }

       /* if ((list.get(position).getIsDriverAlloted())) {
//            viewHolder.txt_driver.setText(list.get(position).getDriver() + "");
        } else {
            viewHolder.txt_driver.setText("Find Driver");
            viewHolder.txt_driver.setTextColor(Color.RED);
            viewHolder.txt_driver.setTypeface(null, Typeface.BOLD);
            viewHolder.txt_driver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        }*/
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("jobId", list.get(position).getId());
                bundle.putString("activity", "owner");
                MyApplication.activityStart(activity, VehicalDetailActivity.class, false, bundle);
            }
        });
    }


    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_vehicle_no, txt_type, txt_reg_no, txt_driver, txt_approve;
        CircleImageView img_vehicle;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_vehicle_no = itemView.findViewById(R.id.txt_vehicle_no);
            txt_type = itemView.findViewById(R.id.txt_type);

            txt_reg_no = itemView.findViewById(R.id.txt_reg_no);
            txt_driver = itemView.findViewById(R.id.txt_driver);
            txt_approve = itemView.findViewById(R.id.txt_approve);
            img_vehicle = itemView.findViewById(R.id.img_vehicle);


        }
    }


}
