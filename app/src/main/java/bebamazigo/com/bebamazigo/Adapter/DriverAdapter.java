package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bebamazigo.com.bebamazigo.Activity.DriverDetailActivity;
import bebamazigo.com.bebamazigo.Activity.DriverDetailsNew;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.CircularImageView;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.DriverModel;
import bebamazigo.com.bebamazigo.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class DriverAdapter extends RecyclerView.Adapter<DriverAdapter.ViewHolder> {

    int selectedPosition = 0;
    private ArrayList<DriverModel> list;
    private String vehicleId;
    private int rowLayout;
    private Activity activity;
    private boolean isOpen = false;

    public DriverAdapter(ArrayList<DriverModel> list, String vehicleId, Activity activity) {

        this.list = list;
        this.rowLayout = R.layout.item_driver;
        this.activity = activity;
        this.vehicleId = vehicleId;

    }


    public void setData(ArrayList<DriverModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public void clearData() {
        if (list != null)
            list.clear();
    }


    @Override
    public DriverAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new DriverAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final DriverAdapter.ViewHolder viewHolder, final int position) {

        Picasso.get().load(ApiService.BASE_URL_IMAGE + list.get(position).getProfileImage())
                .fit()
                .into(viewHolder.img_profile);
        viewHolder.txt_name.setText(list.get(position).getName());

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(activity, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(list.get(position).getLocation().getLat(), list.get(position).getLocation().getLng(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = "";
            if (!TextUtils.isEmpty(addresses.get(0).getAddressLine(0)))
                address = addresses.get(0).getAddressLine(0) + ","; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

//            if (!TextUtils.isEmpty(addresses.get(0).getLocality()))
//                address = address + addresses.get(0).getLocality() + ",";
//
//            if (!TextUtils.isEmpty(addresses.get(0).getAdminArea()))
//                address = address + addresses.get(0).getAdminArea() + ",";
//
//            if (!TextUtils.isEmpty(addresses.get(0).getCountryName()))
//                address = address + addresses.get(0).getCountryName() + ",";
//
//            if (!TextUtils.isEmpty(addresses.get(0).getPostalCode()))
//                address = address + addresses.get(0).getPostalCode() + ",";
//
//            if (!TextUtils.isEmpty(addresses.get(0).getFeatureName()))
//                address = address + addresses.get(0).getFeatureName();
            viewHolder.txt_email.setText(address);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        if (list.get(position).getRequestStatus()){
            viewHolder.txt_send.setText("Request sent");
        }


        viewHolder.txt_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getRequestStatus()){
                    Toast.makeText(activity,"already sent request!!!",Toast.LENGTH_SHORT).show();
                }else {
                    sendRequest(position, viewHolder);
                }

            }
        });

        viewHolder.txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("id", list.get(position).getId());
                bundle.putString("vehicleId", vehicleId);
                MyApplication.activityStart(activity, DriverDetailsNew.class, false, bundle);
            }
        });
    }

    private void sendRequest(final int pos, final ViewHolder viewHolder) {
        JSONObject param = new JSONObject();
        try {
            param.put("vehicleId", vehicleId);
            param.put("driverId", list.get(pos).getId());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.REQUEST_DRIVER, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(activity, "Request Sent");
                viewHolder.txt_send.setText("Sent");
                /*list.remove(pos);
                notifyItemRemoved(pos);*/
                //notifyItemChanged(pos);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_name, txt_email, txt_send, txt_view;
        CircleImageView img_profile;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_name = itemView.findViewById(R.id.txt_name);
            txt_email = itemView.findViewById(R.id.txt_email);
            txt_send = itemView.findViewById(R.id.txt_send);
            txt_view = itemView.findViewById(R.id.txt_view);
            img_profile = itemView.findViewById(R.id.img_profile);


        }
    }


}
