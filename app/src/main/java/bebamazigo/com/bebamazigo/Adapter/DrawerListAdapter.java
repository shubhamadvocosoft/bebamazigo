package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.R;

/**
 * Created by ramees on 8/16/2016.
 */
public class DrawerListAdapter extends BaseAdapter {
    Activity activity;
    private static LayoutInflater inflater = null;
    ArrayList<DrawerItem> list = new ArrayList<>();

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    private int selectedPosition = 0;

    public DrawerListAdapter(Activity activity) {
// TODO Auto-generated constructor stub
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addMenu(String menu, int icon, int clickedIcon) {
        list.add(new DrawerItem(menu, icon, clickedIcon));
        notifyDataSetChanged();
    }

    public String getMenu(int position) {
        return list.get(position).getMenu();
    }

    @Override
    public int getCount() {
// TODO Auto-generated method stub
        return list.size();
    }

    @Override
    public Object getItem(int position) {
// TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
// TODO Auto-generated method stub
        return position;
    }

    public class Holder {
        TextView tv_title;
        ImageView im_icon;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
// TODO Auto-generated method stub
        Holder holder = new Holder();
        View view;
        view = inflater.inflate(R.layout.layout_drawer_item, null);
        holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
        holder.im_icon = (ImageView) view.findViewById(R.id.im_icon);
        holder.tv_title.setText(list.get(position).getMenu());
        holder.im_icon.setImageDrawable(activity.getResources().getDrawable(list.get(position).getIcon()));
        if (position == selectedPosition) {
            view.setBackgroundColor(activity.getResources().getColor(R.color.orange_dark));
            holder.im_icon.setImageDrawable(activity.getResources().getDrawable(list.get(position).getClickedIcon()));
        } else {
            view.setBackgroundColor(Color.TRANSPARENT);
            holder.im_icon.setImageDrawable(activity.getResources().getDrawable(list.get(position).getIcon()));
        }
        return view;
    }

    private class DrawerItem {
        private String menu;
        private int icon, clickedIcon;

        public DrawerItem(String menu, int icon, int clickedIcon) {
            this.menu = menu;
            this.icon = icon;
            this.clickedIcon = clickedIcon;
        }

        public int getClickedIcon() {
            return clickedIcon;
        }

        public void setClickedIcon(int clickedIcon) {
            this.clickedIcon = clickedIcon;
        }

        public String getMenu() {
            return menu;
        }

        public int getIcon() {
            return icon;
        }
    }
}