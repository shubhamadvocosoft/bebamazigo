package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.Activity.ReceiptActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.ReceiptModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class ReceiptAdapter extends RecyclerView.Adapter<ReceiptAdapter.ViewHolder> {

    int selectedPosition = 0;
    private List<ReceiptModel> list;
    private int rowLayout;
    private Activity activity;


    public ReceiptAdapter(ArrayList<ReceiptModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_receipt;
        this.activity = activity;
    }


    public void setData(List<ReceiptModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public ReceiptAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new ReceiptAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ReceiptAdapter.ViewHolder viewHolder, final int position) {

        viewHolder.txt_title.setText(list.get(position).getTitle());
        viewHolder.txt_amount.setText("KSH " + list.get(position).getNetPrice() + "");
        viewHolder.txt_date.setText(MyApplication.utcToDate(list.get(position).getDate()) + " " + MyApplication.utcToTime(list.get(position).getDate()));
        Picasso.get()
                .load(ApiService.BASE_URL_IMAGE + list.get(position).getVehicalType().getImgUrl())
                .into(viewHolder.img);


        viewHolder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ReceiptActivity.class);
                /*intent.putExtra("url", "https://www.google.com");*/
                activity.startActivity(intent);

                /*StaticData.CURRENT_TRIP = list.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("jobId", list.get(position).getId());
                MyApplication.activityStart(activity, ShowTripActivity.class, false, bundle);*/

            }
        });
    }

    @Override
    public int getItemCount() {

        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title, txt_receipt_no, txt_date, txt_amount;
        public ImageView img;
        public LinearLayout llMain;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            txt_receipt_no = (TextView) itemView.findViewById(R.id.txt_receipt_no);
            txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            txt_amount = (TextView) itemView.findViewById(R.id.txt_amount);
            img = (ImageView) itemView.findViewById(R.id.img);
            llMain = (LinearLayout) itemView.findViewById(R.id.llMain);

        }
    }
}
