package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.NotificationModel;
import bebamazigo.com.bebamazigo.Model.RequestModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    int selectedPosition = 0;
    private ArrayList<NotificationModel> list;
    private String vehicleId;
    private int rowLayout;
    private Activity activity;
    private boolean isOpen = false;


    public NotificationAdapter(ArrayList<NotificationModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_notification;
        this.activity = activity;
    }


    public void setData(ArrayList<NotificationModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null)
            list.clear();
    }


    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new NotificationAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final NotificationAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.date.setText(MyApplication.utcToDate(list.get(position).getDate())+","+MyApplication.utcToTime(list.get(position).getDate()));
        viewHolder.title.setText(list.get(position).getTitle());
        viewHolder.body.setText(list.get(position).getBody());


    }



    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView date, title, body;

        public ViewHolder(View itemView) {

            super(itemView);
            date = itemView.findViewById(R.id.date);
            title = itemView.findViewById(R.id.title);
            body = itemView.findViewById(R.id.body);


        }
    }


}
