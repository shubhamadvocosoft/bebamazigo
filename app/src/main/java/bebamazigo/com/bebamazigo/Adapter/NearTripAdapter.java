package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.Activity.DriverHomeActivity;
import bebamazigo.com.bebamazigo.Activity.NotificationJobActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.CircularImageView;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class NearTripAdapter extends RecyclerView.Adapter<NearTripAdapter.ViewHolder> {

    int selectedPosition = 0;
    private List<TripModel> list;
    private int rowLayout;
    private Activity activity;

    public NearTripAdapter(ArrayList<TripModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_near_trip;
        this.activity = activity;
    }

    public void setData(List<TripModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null)
            list.clear();
    }

    @Override
    public NearTripAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new NearTripAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(NearTripAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_title.setText("Job Title - " + list.get(position).getTitle());
        viewHolder.txt_type_of_vehicle.setText("Vehicle Type - " + list.get(position).getTypeOfVehicle());
        viewHolder.txt_job_amount.setText("Job Amount - " + list.get(position).getNetPrice() + " KSH");
        viewHolder.txt_source.setText("From - " + list.get(position).getSource().getAddress() + "");
        viewHolder.txt_start_date.setText("Start Date : " + MyApplication.utcToDate(list.get(position).getDate()) + "");
        Picasso.get().load(ApiService.BASE_URL_IMAGE + list.get(position).getVehicalType().getImgUrl())
                .fit()
                .into(viewHolder.imgVehicle);

        viewHolder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptJob(list.get(position).getId());
            }
        });
        viewHolder.txt_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.startActivity(new Intent(activity, NotificationJobActivity.class).putExtra("jobId", list.get(position).getId()).putExtra("isCameAfterAccept",false));
            }
        });
        viewHolder.txt_view_customer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomerDialog(activity, list.get(position).getCreator().getName(),
                        list.get(position).getCreator().getMobile(),
                        list.get(position).getCreator().getEmail(),
                        list.get(position).getCreator().getProfileImage());
            }
        });

        if (list.get(position).getRequiredType().equalsIgnoreCase("vehicle")) {
            viewHolder.txt_end_date.setVisibility(View.GONE);
            viewHolder.txt_destination.setText("To - " + list.get(position).getDestination().getAddress());

        } else {
            viewHolder.ll_vehicle.setVisibility(View.GONE);
            viewHolder.txt_end_date.setText("End Date : " + MyApplication.utcToDate(list.get(position).getEndDate()) + "");
        }
    }

    public void showCustomerDialog(Activity activity, String name, String mobile, String email, String image) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.customer_detail_dialog);

        CircleImageView custImage = (CircleImageView) dialog.findViewById(R.id.custImage);
        Picasso.get().load(ApiService.BASE_URL_IMAGE + image)
                .fit()
                .into(custImage);

        TextView textName = (TextView) dialog.findViewById(R.id.txtCustName);
        textName.setText(name);
        TextView textMobile = (TextView) dialog.findViewById(R.id.txtCustMobile);
        textMobile.setText(mobile);
        TextView textEmail = (TextView) dialog.findViewById(R.id.txtCustEmail);
        textEmail.setText(email);

        TextView dialogOkButton = (TextView) dialog.findViewById(R.id.txt_ok);
        dialogOkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void acceptJob(String jobId) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.JOB_ACCEPT, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.activityStart(activity, DriverHomeActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title, txt_type_of_vehicle, txt_job_amount, txt_source, txt_destination, txt_accept, txt_view, txt_view_customer, txt_end_date, txt_start_date;
        ImageView img_dp;
        ImageView imgVehicle;
        LinearLayout ll_vehicle;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_type_of_vehicle = itemView.findViewById(R.id.txt_type_of_vehicle);
            txt_job_amount = itemView.findViewById(R.id.txt_job_amount);
            txt_source = itemView.findViewById(R.id.txt_source);
            txt_destination = itemView.findViewById(R.id.txt_destination);
            txt_accept = itemView.findViewById(R.id.txt_accept);
            txt_view = itemView.findViewById(R.id.txt_view);
            txt_view_customer = itemView.findViewById(R.id.txt_view_customer);
            txt_end_date = itemView.findViewById(R.id.txt_end_date);
            txt_start_date = itemView.findViewById(R.id.txt_start_date);
            txt_view = itemView.findViewById(R.id.txt_view);
            img_dp = itemView.findViewById(R.id.img_dp);
            imgVehicle = itemView.findViewById(R.id.imgVehicle);
            ll_vehicle = itemView.findViewById(R.id.ll_vehicle);

        }
    }
}
