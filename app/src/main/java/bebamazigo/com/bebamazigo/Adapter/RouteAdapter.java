package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Model.RouteModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 1/30/2018.
 */

public class RouteAdapter extends RecyclerView.Adapter<RouteAdapter.ViewHolder> {
    int selectedPosition = 0;
    private ArrayList<RouteModel> list;
    private int rowLayout;
    private Activity activity;
    OnRouteSelect callback;

    public RouteAdapter(ArrayList<RouteModel> list, Activity activity, OnRouteSelect callback) {

        this.list = list;
        this.rowLayout = R.layout.item_route;
        this.activity = activity;
        this.callback = callback;
    }


    public void setData(ArrayList<RouteModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {

        viewHolder.distance.setText(list.get(position).getDistance().toUpperCase());
        viewHolder.duration.setText(list.get(position).getDuration().toUpperCase());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                selectedPosition = position;
                callback.itemSelected(position, list.get(position));
                notifyDataSetChanged();
            }
        });

        if (position == selectedPosition) {
            viewHolder.tick.setVisibility(View.VISIBLE);
        } else {
            viewHolder.tick.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setBackgroundColor(Color.GRAY);
                } else if (event.getAction() == MotionEvent.ACTION_UP
                        || event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v.setBackgroundColor(Color.WHITE);
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView duration, distance;
        ImageView tick;

        public ViewHolder(View itemView) {

            super(itemView);
            duration = (TextView) itemView.findViewById(R.id.txt_duration);
            distance = (TextView) itemView.findViewById(R.id.txt_distance);
            tick = itemView.findViewById(R.id.tick);
        }
    }

    public interface OnRouteSelect {
        void itemSelected(int position, RouteModel routeModel);
    }
}
