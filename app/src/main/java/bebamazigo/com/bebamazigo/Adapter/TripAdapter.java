package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.Activity.NotificationJobActivity;
import bebamazigo.com.bebamazigo.Activity.ShowTripActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class TripAdapter extends RecyclerView.Adapter<TripAdapter.ViewHolder> {
    int selectedPosition = 0;
    private List<TripModel> list;
    private int rowLayout;
    private Activity activity;


    public TripAdapter(ArrayList<TripModel> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_trip;
        this.activity = activity;

    }


    public void setData(List<TripModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public TripAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new TripAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(TripAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_title.setText(list.get(position).getTitle());
        viewHolder.txt_price.setText(list.get(position).getNetPrice() + "");
        viewHolder.txt_vehicle.setText(list.get(position).getTypeOfVehicle());
        viewHolder.txt_time.setText(MyApplication.utcToTime(list.get(position).getDate()));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StaticData.CURRENT_TRIP = list.get(position);
                Bundle bundle = new Bundle();
                bundle.putString("jobId", list.get(position).getId());
                MyApplication.activityStart(activity, ShowTripActivity.class, false, bundle);
            }
        });
    }

    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_title, txt_time, txt_vehicle, txt_price;
        CircleImageView img_dp;

        public ViewHolder(View itemView) {

            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_title);
            txt_vehicle = itemView.findViewById(R.id.txt_vehicle);
            txt_time = itemView.findViewById(R.id.txt_time);
            txt_price = itemView.findViewById(R.id.txt_price);
            img_dp = itemView.findViewById(R.id.img_dp);

        }
    }
}
