package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import bebamazigo.com.bebamazigo.Activity.JobDetailMachinary;
import bebamazigo.com.bebamazigo.Activity.ShowTripActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;

/**
 * Created by anupamchugh on 09/02/16.
 */
public class MultiViewTypeAdapter extends RecyclerView.Adapter {

    private List<TripModel> dataSet;
    private Activity activity;


    public MultiViewTypeAdapter(List<TripModel> data, Activity activity) {
        this.dataSet = data;
        this.activity = activity;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view;
        switch (viewType) {
            case 0:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip_vehical, parent, false);
                return new VehicalViewHolder(view);
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trip_mechinary, parent, false);
                return new MachineViewHolder(view);


        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {

        switch (dataSet.get(position).getRequiredType()) {
            case "Vehicle":
                return 0;
            case "Machinery":
                return 1;
            default:
                return -1;
        }

    }

    public void clearData() {

        if (dataSet != null)
            dataSet.clear();
    }

    public void setData(List<TripModel> list) {
        this.dataSet = list;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int listPosition) {

        TripModel object = dataSet.get(listPosition);
        if (object != null) {
            switch (object.getRequiredType()) {
                case "Vehicle":
                    VehicalViewHolder view = ((VehicalViewHolder) holder);
                    view.title.setText(object.getTitle());
                    Picasso.get().load(ApiService.BASE_URL_IMAGE + object.getVehicalType().getImgUrl()).fit().into(view.image);
                    view.txt_vehicle.setText(object.getTypeOfVehicle());
                    view.txt_status.setText(object.getStatus());
                    view.txt_price.setText("KSH " + object.getNetPrice());
                    view.txt_date.setText(MyApplication.utcToDate(object.getDate()));
                    view.txt_time.setText(MyApplication.utcToTime(object.getDate()));
                    String sourceString = "<b>" + "From: " + "</b> " + object.getSource().getAddress();
                    view.txt_from.setText(Html.fromHtml(sourceString));
                    String destString = "<b>" + "To: " + "</b> " + object.getDestination().getAddress();
                    view.txt_to.setText(Html.fromHtml(destString));


                    view.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StaticData.CURRENT_TRIP = dataSet.get(listPosition);
                            Bundle bundle = new Bundle();
                            bundle.putString("jobId", dataSet.get(listPosition).getId());
                            MyApplication.activityStart(activity, ShowTripActivity.class, false, bundle);
                        }
                    });

                    break;
                case "Machinery":
                    MachineViewHolder view2 = ((MachineViewHolder) holder);
                    view2.title.setText(object.getTitle());
                    Picasso.get().load(ApiService.BASE_URL_IMAGE + object.getVehicalType().getImgUrl()).into(view2.image);
                    view2.txt_vehicle.setText(object.getTypeOfVehicle());
                    view2.txt_status.setText(object.getStatus());
                    view2.txt_price.setText("KSH " + object.getNetPrice());
                    String s_date = "<b>" + "Start Date : " + "</b> " + MyApplication.utcToDate(object.getDate() + "," + MyApplication.utcToTime(object.getDate()));
                    view2.txt_date.setText(Html.fromHtml(s_date));
                    String e_date = "<b>" + "End Date : " + "</b> " + MyApplication.utcToDate(object.getEndDate() + "," + MyApplication.utcToTime(object.getEndDate()));
                    view2.txt_end_date.setText(Html.fromHtml(e_date));
                    String sourceString2 = "<b>" + "From: " + "</b> " + object.getSource().getAddress();
                    view2.txt_from.setText(Html.fromHtml(sourceString2));

                    view2.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StaticData.CURRENT_TRIP = dataSet.get(listPosition);
                            Bundle bundle = new Bundle();
                            bundle.putString("jobId", dataSet.get(listPosition).getId());
                            MyApplication.activityStart(activity, JobDetailMachinary.class, false, bundle);
                        }
                    });


                    break;


            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public static class VehicalViewHolder extends RecyclerView.ViewHolder {

        TextView title, txt_vehicle, txt_status, txt_date, txt_time, txt_price, txt_from, txt_to;
        ImageView image;

        public VehicalViewHolder(View itemView) {
            super(itemView);
            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.txt_vehicle = (TextView) itemView.findViewById(R.id.txt_vehicle);
            this.txt_status = (TextView) itemView.findViewById(R.id.txt_status);
            this.txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            this.txt_time = (TextView) itemView.findViewById(R.id.txt_time);
            this.txt_price = (TextView) itemView.findViewById(R.id.txt_price);
            this.txt_from = (TextView) itemView.findViewById(R.id.txt_from);
            this.txt_to = (TextView) itemView.findViewById(R.id.txt_to);
            this.image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public static class MachineViewHolder extends RecyclerView.ViewHolder {

        TextView title, txt_vehicle, txt_status, txt_date, txt_end_date, txt_price, txt_from;
        ImageView image;

        public MachineViewHolder(View itemView) {
            super(itemView);

            this.title = (TextView) itemView.findViewById(R.id.txt_title);
            this.txt_vehicle = (TextView) itemView.findViewById(R.id.txt_vehicle);
            this.txt_status = (TextView) itemView.findViewById(R.id.txt_status);
            this.txt_date = (TextView) itemView.findViewById(R.id.txt_date);
            this.txt_end_date = (TextView) itemView.findViewById(R.id.txt_end_date);
            this.txt_price = (TextView) itemView.findViewById(R.id.txt_price);
            this.txt_from = (TextView) itemView.findViewById(R.id.txt_from);
            this.image = (ImageView) itemView.findViewById(R.id.image);
        }
    }
}
