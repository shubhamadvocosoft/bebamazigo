package bebamazigo.com.bebamazigo.Adapter;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Activity.VehicalDetailActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.DriverModel;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class MyVehicleAdapter extends RecyclerView.Adapter<MyVehicleAdapter.ViewHolder> {

    int selectedPosition = 0;
    private ArrayList<Vehicle> list;
    private ArrayList<DriverModel> driverList;
    private int rowLayout;
    private FragmentActivity activity;
    private boolean isOpen = false;
    private Gson gson = new Gson();

    public MyVehicleAdapter(ArrayList<Vehicle> list, FragmentActivity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_request_vehicle;
        this.activity = activity;

    }


    public void setData(ArrayList<Vehicle> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public MyVehicleAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new MyVehicleAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final MyVehicleAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_type.setText(list.get(position).getVehicaldata().getType());

        Picasso.get()
                .load(ApiService.BASE_URL_IMAGE + list.get(position).getVehicleType().getImgUrl()).fit()
                .into(viewHolder.img_vehicle);

        if (TextUtils.isEmpty(list.get(position).getVehicaldata().getVehicalNumber()))
            viewHolder.txt_vehicle_no.setVisibility(View.GONE);
        else
            viewHolder.txt_vehicle_no.setText(list.get(position).getVehicaldata().getVehicalNumber());

        viewHolder.txt_owner_name.setText(list.get(position).getOwner().getName());
        viewHolder.txt_owner_phone.setText(list.get(position).getOwner().getMobile());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("jobId", list.get(position).getVehicaldata().getId());
                bundle.putString("activity", "driver");
                MyApplication.activityStart(activity, VehicalDetailActivity.class, false, bundle);
            }
        });

        viewHolder.txt_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptJob(list.get(position).getId(), position);

            }
        });
        viewHolder.txt_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rejectJob(list.get(position).getId(), position);

            }
        });
    }


    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    private void acceptJob(String jobId, final int pos) {
        JSONObject param = new JSONObject();
        try {
            param.put("requestId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.Vehicle_ACCEPT, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(activity, "Request Accepted!!!");
                list.remove(pos);
                notifyItemRemoved(pos);
                activity.getFragmentManager().popBackStack();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void rejectJob(String jobId, final int pos) {
        JSONObject param = new JSONObject();
        try {
            param.put("requestId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.Vehicle_Reject, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(activity, "Request Canceled!!!");
                list.remove(pos);
                notifyItemRemoved(pos);
                activity.getFragmentManager().popBackStack();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_vehicle_no, txt_owner_phone, txt_type, txt_owner_name, txt_accept, txt_decline;
        ImageView img_vehicle;

        public ViewHolder(View itemView) {

            super(itemView);

            txt_vehicle_no = itemView.findViewById(R.id.txt_vehicle_nmbr);
            txt_owner_phone = itemView.findViewById(R.id.txt_owner_phone);
            txt_type = itemView.findViewById(R.id.txt_type);
            txt_owner_name = itemView.findViewById(R.id.txt_owner_name);
            txt_accept = itemView.findViewById(R.id.txt_accept);
            txt_decline = itemView.findViewById(R.id.txt_decline);
            img_vehicle = itemView.findViewById(R.id.img_vehicle);

        }
    }

}
