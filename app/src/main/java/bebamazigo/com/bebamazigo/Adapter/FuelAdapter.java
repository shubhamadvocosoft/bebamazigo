package bebamazigo.com.bebamazigo.Adapter;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Activity.VehicalDetailActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.DriverModel;
import bebamazigo.com.bebamazigo.Model.FuelModel;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class FuelAdapter extends RecyclerView.Adapter<FuelAdapter.ViewHolder> {
    int selectedPosition = 0;
    private ArrayList<FuelModel> list;
    private ArrayList<FuelModel> driverList;
    private int rowLayout;
    private FragmentActivity activity;
    private boolean isOpen = false;
    private Gson gson = new Gson();

    public FuelAdapter(ArrayList<FuelModel> list, FragmentActivity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_fuel_request;
        this.activity = activity;

    }


    public void setData(ArrayList<FuelModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {

        if (list != null)
            list.clear();
    }

    @Override
    public FuelAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new FuelAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final FuelAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.txt_amount.setText(list.get(position).getAmount()+"");
        viewHolder.txt_date.setText(MyApplication.utcToDate(list.get(position).getUpdatedAt()));
        viewHolder.txt_status.setText(list.get(position).getStatus());
        //viewHolder.txt_trans_id.setText(list.get(position).getStatus());



    }


    @Override
    public int getItemCount() {

        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txt_amount, txt_date,  txt_status, txt_trans_id,txt_mpesa;


        public ViewHolder(View itemView) {

            super(itemView);
            txt_amount = itemView.findViewById(R.id.txt_amount);
            txt_date = itemView.findViewById(R.id.txt_date);

            txt_status = itemView.findViewById(R.id.txt_status);
            txt_trans_id = itemView.findViewById(R.id.txt_trans_id);
            txt_mpesa = itemView.findViewById(R.id.txt_mpesa);



        }
    }


}
