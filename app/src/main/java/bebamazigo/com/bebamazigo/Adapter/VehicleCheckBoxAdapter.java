package bebamazigo.com.bebamazigo.Adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/1/2018.
 */

public class VehicleCheckBoxAdapter extends RecyclerView.Adapter<VehicleCheckBoxAdapter.ViewHolder> {
    int selectedPosition = 0;
    private ArrayList<String> list;
    private List<String> selList = new ArrayList<>();
    private String vehicleId;
    private int rowLayout;
    private Activity activity;
    private boolean isOpen = false;


    public VehicleCheckBoxAdapter(ArrayList<String> list, Activity activity) {
        this.list = list;
        this.rowLayout = R.layout.item_check;
        this.activity = activity;
    }

    public String[] getSelVehicles() {
        if (selList.size() > 0) {
            String[] array = selList.toArray(new String[selList.size()]);
            return array;
        } else
            return null;
    }

    public void setData(ArrayList<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public void clearData() {
        if (list != null)
            list.clear();
    }


    @Override
    public VehicleCheckBoxAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(rowLayout, viewGroup, false);
        return new VehicleCheckBoxAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final VehicleCheckBoxAdapter.ViewHolder viewHolder, final int position) {
        viewHolder.vehicle.setText(list.get(position));

        viewHolder.vehicle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String[] array = getSelVehicles();
                    selList.add(buttonView.getText().toString());
                } else {
                    selList.remove(buttonView.getText().toString());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public CheckBox vehicle;

        public ViewHolder(View itemView) {
            super(itemView);
            vehicle = itemView.findViewById(R.id.chk_vehicle);
        }
    }


}
