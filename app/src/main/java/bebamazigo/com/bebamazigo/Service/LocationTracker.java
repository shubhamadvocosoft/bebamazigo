package bebamazigo.com.bebamazigo.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import bebamazigo.com.bebamazigo.Activity.DriverHomeActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;

public class LocationTracker extends Service {

    private static final String TAG = "BOOMBOOMLOCATIONSERVICE";
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 5f;

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };
    private LocationManager mLocationManager = null;
    private Context context;

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    private void createNotificationChannelAndBuilder() {

        int NOTIFICATION_ID = 1001;
        String CHANNEL_ID = "beba_mazigo_01";
        CharSequence name = "My Channel";
        String Description = "Beba Mazigo Channel";

        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            mChannel.setDescription(Description);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.RED);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 600});
            mChannel.setShowBadge(false);
            notificationManager.createNotificationChannel(mChannel);
        }

        /*NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name) + " is Running")
                .setContentText(getResources().getString(R.string.app_name) + " is Running")
                .setSmallIcon(R.drawable.ic_notification);

        Intent resultIntent = new Intent(getApplicationContext(), DriverHomeActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(DriverHomeActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setContentIntent(resultPendingIntent);

        notificationManager.notify(NOTIFICATION_ID, builder.build());*/

        Intent playIntent = new Intent(getApplicationContext(), DriverHomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name) + " is Running")
                .setContentText(getResources().getString(R.string.app_name) + " is Running")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, notification);
        } else {
            startService(playIntent);
        }

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            boolean finish = intent.getBooleanExtra("finish", false);
            if (finish) {
                stopForeground(true);
                stopSelf();
            }
        } catch (Exception e) {
            e.printStackTrace();

            stopForeground(true);
            stopSelf();
        }
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        context = getApplicationContext();

        createNotificationChannelAndBuilder();

        /*Intent playIntent = new Intent(getApplicationContext(), DriverHomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                playIntent, 0);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setTicker(getResources().getString(R.string.app_name) + " is Running")
                .setContentText(getResources().getString(R.string.app_name) + " is Running")
                .setSmallIcon(R.drawable.ic_notification)
                .setContentIntent(pendingIntent)
                .setOngoing(true)
                .build();
        startForeground(1001,
                notification);*/

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);

            MyApplication.setDouble(StaticData.SP_NEW_LAT, (mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));
            MyApplication.setDouble(StaticData.SP_NEW_LONG, (mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));

            Location location = new Location(LocationManager.NETWORK_PROVIDER); //provider name is unnecessary
            location.setLatitude((mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));//your coords of course
            location.setLongitude((mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));

            Intent in = new Intent("locationUpdated");
            in.putExtra("location", location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
        try {

            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);

        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }

    private class LocationListener implements android.location.LocationListener {

        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);

            MyApplication.setDouble(StaticData.SP_NEW_LAT, (location.getLatitude()));
            MyApplication.setDouble(StaticData.SP_NEW_LONG, (location.getLongitude()));
            Log.e("location", location.toString());

            if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_JOB))) {
                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Locations");
                mDatabase.child(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_JOB)).child("currntLatt").setValue(location.getLatitude());
                mDatabase.child(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_JOB)).child("currntLong").setValue(location.getLongitude());
            }

            Intent in = new Intent("locationUpdated");
            in.putExtra("location", location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(in);

        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }
}