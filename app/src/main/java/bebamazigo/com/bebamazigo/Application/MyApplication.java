package bebamazigo.com.bebamazigo.Application;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.multidex.MultiDex;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bebamazigo.com.bebamazigo.Activity.CustomerHomeActivity;
import bebamazigo.com.bebamazigo.Activity.DriverHomeActivity;
import bebamazigo.com.bebamazigo.Activity.LogInWithFBActivity;
import bebamazigo.com.bebamazigo.Activity.OwnerHomeActivity;
import bebamazigo.com.bebamazigo.Activity.SplashActivity;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.Service.LocationTracker;
import bebamazigo.com.bebamazigo.util.StaticData;

/**
 * Created by Advosoft2 on 1/22/2018.
 */

public class MyApplication extends Application {

    public static String SHARED_PREF_NAME = "BebaMazigoRS_PREF";
    static Dialog mDialog = null;
    private static MyApplication application;
    private static ProgressDialog dialog;
    private RequestQueue mRequestQueue;
    private Context context;

    public static synchronized MyApplication getInstance() {
        return application;
    }

    public static void showProgressDialog(String message) {
        if (dialog != null && dialog.isShowing()) {
            dialog.setMessage(message);
        } else
            dialog = ProgressDialog.show(MyApplication.getInstance().getApplicationContext(), "", message, true);

        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
    }

    public static void hideProgressDialog(Activity activity) {
        if (dialog != null && activity != null) {
            if (dialog.isShowing()) {
                try {
                    dialog.dismiss();
                } catch (IllegalStateException e) {
                }
            }
        }
    }

    public static byte[] getBytesFromBitmap(Bitmap bmp) {
        Bitmap bmph = Bitmap.createScaledBitmap(bmp, 50, 50, false);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmph.compress(Bitmap.CompressFormat.PNG, 50, stream);

        return stream.toByteArray();
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }

    public static void showToast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public static void activityStart(Activity activity, Class<?> aClass, boolean finish) {
        if (finish)
            activity.finish();
        activity.startActivity(new Intent(activity, aClass));
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);

    }

    public static void activityStart(Activity activity, Class<?> aClass, boolean finish, Bundle bundle) {
        Intent intent = new Intent(activity, aClass);
        if (finish) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }
        activity.startActivity(intent.putExtra("bundle", bundle));
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);
        if (finish)
            activity.finish();
    }

    public static void activityFinish(Activity activity) {
        activity.finish();
        activity.overridePendingTransition(R.anim.exit, R.anim.enter);
    }

    public static String getCurrentUTC(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            date = format.parse(time);
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        return format.format(date);
    }

    public static void hideSoftKeyboard(Activity activity, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void saveLogInInfo(Activity activity, JSONObject data) {
        JSONObject user = new JSONObject();
        if (data.optJSONObject("user") != null)
            user = data.optJSONObject("user");
        else
            user = data;

        if (TextUtils.isEmpty(data.optString("access_token"))) {
            if (!user.optBoolean("emailVerified")) {
                showToast(activity, "Verify your email address then login again");
                //MyApplication.activityStart(activity, LogInWithFBActivity.class, true);
                return;
            }

            if (!user.optBoolean("mobileVerified")) {
                int otp = Integer.parseInt(user.optJSONObject("mobOtp").optString("otp"));
                MyApplication.getInstance().showOTPDialog(otp, activity, user.optString("id"));
                return;
            }
        }

        MyApplication.setSharedPrefString(StaticData.SP_ACCESS_TOKEN, data.optString("access_token"));
        MyApplication.setSharedPrefString(StaticData.SP_ID, user.optString("id"));
        MyApplication.setSharedPrefString(StaticData.SP_USER_NAME, user.optString("name"));

        if (!user.optString("profileImage").contains("facebook")) {

            MyApplication.setStatus(StaticData.SP_IS_FACEBOOK, false);
            MyApplication.setSharedPrefString(StaticData.SP_USER_IMAGE, ApiService.BASE_URL_IMAGE + user.optString("profileImage"));
        } else {
            MyApplication.setSharedPrefString(StaticData.SP_USER_IMAGE, user.optString("profileImage"));
            MyApplication.setStatus(StaticData.SP_IS_FACEBOOK, true);
        }
        MyApplication.setSharedPrefString(StaticData.SP_EMAIL, user.optString("email"));
        MyApplication.setSharedPrefString(StaticData.SP_MOBILE, user.optString("mobile"));
        MyApplication.setSharedPrefString(StaticData.SP_REALM, user.optString("realm"));
        MyApplication.setSharedPrefString(StaticData.SP_COMPANY_NAME, user.optString("companyName"));
        MyApplication.setStatus(StaticData.SP_IS_LOGIN, true);
        MyApplication.getInstance().sendToken(activity);
    }

    public static void goToHome(Activity activity) {
        if (TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN))) {
            LoginManager.getInstance().logOut();
            MyApplication.activityStart(activity, LogInWithFBActivity.class, true);
        } else {
            if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equals("owner"))
                MyApplication.activityStart(activity, OwnerHomeActivity.class, true);
            if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equals("customer"))
                MyApplication.activityStart(activity, CustomerHomeActivity.class, true);
            if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equals("driver"))
                MyApplication.activityStart(activity, DriverHomeActivity.class, true);
        }
    }

    public static void initialize_progressbar(Context context) {
        if (mDialog != null) {
            if (mDialog.isShowing()) {
                mDialog.dismiss();
            }
        }
        if (context != null) {
            mDialog = new Dialog(context);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setContentView(R.layout.dialog_progress);
            mDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            mDialog.setCancelable(false);

            LinearLayout linearLayout = (LinearLayout) mDialog.findViewById(R.id.linearLayoutLoader);

            ImageView imageView = new ImageView(context);

            Float width = context.getResources().getDimension(R.dimen._55sdp);

            imageView.setLayoutParams(new LinearLayout.LayoutParams(
                    width.intValue(),
                    width.intValue()
            ));

            imageView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
            GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imageView);
            Glide.with(context.getApplicationContext())
                    .load(R.drawable.loader_animation)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageViewTarget);

            linearLayout.addView(imageView);

            try {
                mDialog.show();
            } catch (Exception e) {
                Log.e("Error", e.toString());
            }
        }
    }

    public static String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String alternative = "alternatives=true";
        String key = "key=AIzaSyDll9PQUPlUyzG5oknwZ1YnZv83sGY51FQ";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + alternative + "&" + key;
        ;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        Log.e("url", url);


        return url;
    }

    public static void stop_progressbar() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }

    public static String utcToDate(String date) {
        try {
            // 2017-03-29 14:40:26
            //  String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
            //  String strCurrentDate = "2018-01-16T12:53:19.200Z";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            //SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss Z");
            Date newDate = null;

            newDate = format.parse(date);
            //  format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
//            format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            format = new SimpleDateFormat("dd-MM-yyyy");
            return format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static String utcToTime(String date) {
        try {
            // 2017-03-29 14:40:26
            //  String strCurrentDate = "Wed, 18 Apr 2012 07:55:29 +0000";
            //  String strCurrentDate = "2018-01-16T12:53:19.200Z";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
            //SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss Z");
            Date newDate = null;

            newDate = format.parse(date);
            //  format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
//            format = new SimpleDateFormat("dd MMM yyyy hh:mm a");
            format = new SimpleDateFormat("hh:mm");
            return format.format(newDate);
        } catch (ParseException e) {
            e.printStackTrace();
            return date;
        }
    }

    public static boolean isEmailValid(String email) {

        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if (matcher.matches())
            return true;
        else
            return false;
    }

    public static void logout(final Activity activity) {
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.LOG_OUT, null, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(getInstance().context, response.optJSONObject("success").optJSONObject("msg").optString("replyCode"));
                if (MyApplication.getStatus(StaticData.SP_IS_FACEBOOK))
                    LoginManager.getInstance().logOut();
                if (getSharedPrefString(StaticData.SP_REALM).equalsIgnoreCase("driver")) {
                    getInstance().startService(new Intent(getInstance(), LocationTracker.class).putExtra("finish", true));
                }

                SharedPreferences preferences = application.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();

                MyApplication.activityStart(activity, SplashActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });


    }

    public static long getSharedPrefLong(String preffConstant) {
        long longValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        longValue = sp.getLong(preffConstant, 0);
        return longValue;
    }

    public static void setSharedPrefLong(String preffConstant, long longValue) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putLong(preffConstant, longValue);
        editor.commit();
    }

    public static String getSharedPrefString(String preffConstant) {
        String stringValue = "";
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        stringValue = sp.getString(preffConstant, "");
        return stringValue;
    }

    public static void setSharedPrefString(String preffConstant,
                                           String stringValue) {
        if (!TextUtils.isEmpty(stringValue)) {
            SharedPreferences sp = application.getSharedPreferences(
                    SHARED_PREF_NAME, 0);
            SharedPreferences.Editor editor = sp.edit();
            editor.putString(preffConstant, stringValue);
            editor.commit();
        }
    }

    public static int getSharedPrefInteger(String preffConstant) {
        int intValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        intValue = sp.getInt(preffConstant, 0);
        return intValue;
    }

    public static void printSharedPref() {
        int intValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Log.e("SP", sp.toString());

    }

    public static void setSharedPrefInteger(String preffConstant, int value) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt(preffConstant, value);
        editor.commit();
    }

    public static float getSharedPrefFloat(String preffConstant) {
        float floatValue = 0;
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        floatValue = sp.getFloat(preffConstant, 40);
        return floatValue;
    }

    public static void setSharedPrefFloat(String preffConstant, float floatValue) {
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(preffConstant, floatValue);
        editor.commit();
    }

    public static void setSharedPrefArray(String preffConstant, float floatValue) {
        SharedPreferences sp = application.getSharedPreferences(
                preffConstant, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putFloat(preffConstant, floatValue);
        editor.commit();
    }

    public static boolean getStatus(String name) {
        boolean status;
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        status = sp.getBoolean(name, false);
        return status;
    }

    public static void setStatus(String name, boolean istrue) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(name, istrue);
        editor.commit();
    }

    public static void setDouble(String name, Double value) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(name, value.toString());
        editor.commit();
    }

    public static Double getDouble(String name) {
        SharedPreferences sp = application.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        Log.d("data_value", sp.getString(name, ""));

        Double value = Double.parseDouble(sp.getString(name, ""));

        return value;
    }

    public void showOTPDialog(final int otp, final Activity activity, final String peopleId) {

        LayoutInflater inflater = LayoutInflater.from(activity);
        final View customView = inflater.inflate(R.layout.dialog_otp_application, null);
        final AlertDialog dialog = new AlertDialog.Builder(activity)
                .setView(customView)
                .create();
        final EditText edtOTP = customView.findViewById(R.id.edt_otp);
        edtOTP.setText(otp + "");
        final TextView txtSubmit = customView.findViewById(R.id.txt_submit);
        final TextView txtResend = customView.findViewById(R.id.txt_resend);
        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtOTP.getText().toString().isEmpty()) {
                    edtOTP.setError("Enter OTP");
                } else {
                    verifyOTP(Integer.parseInt(edtOTP.getText().toString()), activity, dialog, peopleId);
                }
            }
        });
        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resendOTP(activity, peopleId);
                dialog.dismiss();
            }
        });
        if(!((Activity) activity).isFinishing()) {
            //show dialog
            dialog.show();
        }



    }

    public void verifyOTP(int otp, final Activity activity, final AlertDialog dialog, String peopleId) {
        JSONObject param = new JSONObject();
        try {
            param.put("peopleId", peopleId);
            param.put("otp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiService.getInstance(activity).makePostCall(activity, ApiService.VERIFY_MOBILE, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                showToast(activity, "Please! Login again");
                MyApplication.activityStart(activity, LogInWithFBActivity.class, true);
                dialog.dismiss();
            }

            @Override
            public void onError(VolleyError volleyError) {
                showToast(activity, "Otp is not correct.");
            }
        });

    }

    public void resendOTP(final Activity activity, String peopleId) {
        JSONObject param = new JSONObject();
        try {
            param.put("peopleId", peopleId);
            param.put("type", "signup");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiService.getInstance(activity).makePostCall(activity, ApiService.RESEND_OTP, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                showOTPDialog(Integer.parseInt(response.optJSONObject("success").optJSONObject("data").optJSONObject("mobOtp").optString("otp")), activity, response.optJSONObject("success").optJSONObject("data").optString("id"));
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });

    }

    public void sendToken(final Activity activity) {
        JSONObject param = new JSONObject();
        try {
            param.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(activity).makePostCallAuth(activity, ApiService.UPDATE_FCM, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                goToHome(activity);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        context = getApplicationContext();
        dialog = new ProgressDialog(context);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });

    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        Intent intent = new Intent();
        intent.setAction("com.mydomain.SEND_LOG"); // see step 5.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity(intent);
        // kill off the crashed app
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setRetryPolicy(new DefaultRetryPolicy(
                50000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        getRequestQueue().add(req);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void stopService() {
        ActivityManager mActivityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        mActivityManager.restartPackage(getPackageName());
    }

}
