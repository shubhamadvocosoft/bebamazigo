package bebamazigo.com.bebamazigo.Dialog;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.RequestAdapter;
import bebamazigo.com.bebamazigo.Model.RequestModel;
import bebamazigo.com.bebamazigo.R;

/**
 * Created by Advosoft2 on 2/15/2018.
 */

public class DialogRequests extends DialogFragment {
    RecyclerView recyclerView;
    RequestAdapter requestAdapter;
    ArrayList<RequestModel> list;


    public static DialogRequests newInstance(ArrayList<RequestModel> list) {
        DialogRequests dialog = new DialogRequests();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable("list", list);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        list = (ArrayList<RequestModel>) getArguments().getSerializable("list");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_select_driver, container,
                false);

        recyclerView = rootView.findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
        requestAdapter = new RequestAdapter(list, getActivity());
        recyclerView.setAdapter(requestAdapter);

        getDialog().setTitle("Requests");

        // Do something else
        return rootView;
    }
}