package bebamazigo.com.bebamazigo.Activity;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;

public class ReceiptActivity extends AppCompatActivity implements AdvancedWebView.Listener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webView)
    AdvancedWebView webView;
    private ProgressDialog pd;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt);
        ButterKnife.bind(this, this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Receipt");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {

            url = bundle.getString("url");

            Log.d("URL", url);

        } else {
            finish();
            MyApplication.showToast(this, "Something Went Wrong");
        }

        pd = new ProgressDialog(ReceiptActivity.this);
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(true);
        pd.setMessage("Loading");
        pd.show();

        webView.setListener(this, this);
        webView.loadUrl(url);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(ReceiptActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {

    }

    @Override
    public void onPageFinished(String url) {
        pd.dismiss();
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        MyApplication.showToast(ReceiptActivity.this, description);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {

    }

    @Override
    public void onExternalPageRequest(String url) {

    }
}
