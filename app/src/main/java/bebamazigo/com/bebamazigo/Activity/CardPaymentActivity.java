package bebamazigo.com.bebamazigo.Activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.victor.loading.rotate.RotateLoading;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import im.delight.android.webview.AdvancedWebView;

public class CardPaymentActivity extends AppCompatActivity implements AdvancedWebView.Listener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webView)
    AdvancedWebView webView;
    @BindView(R.id.rotateloading)
    RotateLoading rotateloading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_payment);
        ButterKnife.bind(this, this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Add Money Card");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        String url = bundle.getString("url");

        if (url.isEmpty()) {
            finish();
            MyApplication.showToast(this, "Something Went Wrong");
        }

        Log.d("URL", url);

        rotateloading.start();
        rotateloading.setVisibility(View.VISIBLE);

        webView.setListener(this, this);
        webView.loadUrl(url);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(CardPaymentActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        Log.d("onPageStarted", url);
        rotateloading.start();
        rotateloading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        rotateloading.stop();
        rotateloading.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        MyApplication.showToast(CardPaymentActivity.this, description);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
        Log.d("onDownloadRequested", url);
    }

    @Override
    public void onExternalPageRequest(String url) {
        Log.d("onExternalPageRequest", url);
    }
}
