package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.CircularImageView;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import hyogeun.github.com.colorratingbarlib.ColorRatingBar;

public class DriverDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_profile)
    CircularImageView imgProfile;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.rating)
    ColorRatingBar rating;
    @BindView(R.id.txt_vehicle_type)
    TextView txtVehicleType;
    @BindView(R.id.txt_vehicle_no)
    TextView txtVehicleNo;
    @BindView(R.id.txt_reg_no)
    TextView txtRegNo;
    @BindView(R.id.txt_make_no)
    TextView txtMakeNo;
    @BindView(R.id.txt_model_no)
    TextView txtModelNo;
    @BindView(R.id.txt_tonnage)
    TextView txtTonnage;
    @BindView(R.id.txt_body_type)
    TextView txtBodyType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        setTitle("Driver Detail");

        getDetail();
    }

    private void getDetail() {
//        ApiService
    }
}
