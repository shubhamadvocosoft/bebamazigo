package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapterAssesment;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicalAssismentActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.txt_vehicle_no)
    TextView txtVehicleNo;
    @BindView(R.id.txt_vehicle_type)
    TextView txtVehicleType;
    @BindView(R.id.spn_register_type)
    Spinner spnRegisterType;
    @BindView(R.id.txt_assesment)
    TextView txtAssesment;
    @BindView(R.id.txt_add_money)
    TextView txtAddMoney;
    @BindView(R.id.txt_submit)
    TextView txtSubmit;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    @BindView(R.id.txt_wallet_amount)
    TextView txtWalletAmount;

    Vehicle model = null;
    int assesPrice = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehical_assesment);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Vehicle Assesment");

        model = (Vehicle) getIntent().getBundleExtra("bundle").getSerializable("data");

        txtVehicleNo.setText(model.getVehicleNumber());
        txtVehicleType.setText(model.getType());

        spnRegisterType.setAdapter(new HintAdapterAssesment(VehicalAssismentActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.assesment_duration)));

        spnRegisterType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                txtAssesment.setText((Integer.parseInt(spnRegisterType.getSelectedItem().toString()) * assesPrice) + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtAddMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MyApplication.activityStart(VehicalAssismentActivity.this, AddMoneyActivity.class, false);

            }
        });

        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(txtAssesment.getText().toString()) <= Integer.parseInt(txtWalletAmount.getText().toString())) {

                    makeAssesmentApi();

                } else {
                    Toast.makeText(VehicalAssismentActivity.this, "Your Account Balance is Low!!!!", Toast.LENGTH_SHORT).show();
                }

            }
        });

        getAssestPrice();
        getWallet();


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(VehicalAssismentActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    private void getAssestPrice() {
        String url = ApiService.Get_VehicalAssesPrice + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN);
        ApiService.getInstance(VehicalAssismentActivity.this).makeGetCall(VehicalAssismentActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                try {
                    assesPrice = response.getJSONObject("success").getJSONObject("data").optInt("price");

                    txtAssesment.setText(assesPrice + "");

                } catch (Exception e) {

                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void getWallet() {
        ApiService.getInstance(VehicalAssismentActivity.this).makeGetCall(VehicalAssismentActivity.this, ApiService.GET_WALLET +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response + "");
                if (response.optJSONObject("success") != null) {
                    JSONObject data = response.optJSONObject("success").optJSONObject("data");
                    txtWalletAmount.setText(data.optString("walletAmount"));
                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void makeAssesmentApi() {
        JSONObject param = new JSONObject();
        try {
            param.put("vehicleId", model.getId());
            param.put("amount", Integer.parseInt(txtAssesment.getText().toString()));
            param.put("paymentType", "wallet");
            param.put("numberOfYear", Integer.parseInt(spnRegisterType.getSelectedItem().toString()));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(VehicalAssismentActivity.this).makePostCallAuth(VehicalAssismentActivity.this, ApiService.VehicleAssesment,
                param, new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        MyApplication.showToast(VehicalAssismentActivity.this, "assesment successful!!!");
                        MyApplication.activityFinish(VehicalAssismentActivity.this);
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        MyApplication.showToast(VehicalAssismentActivity.this, "Something went wrong.");
                    }
                });
    }

}
