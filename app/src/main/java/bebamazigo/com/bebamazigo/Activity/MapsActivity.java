package bebamazigo.com.bebamazigo.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bebamazigo.com.bebamazigo.Adapter.RouteAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.RouteModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.DirectionsJSONParser;
import bebamazigo.com.bebamazigo.util.ParserTask;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, RouteAdapter.OnRouteSelect, ParserTask.ParserCallback {

    LatLng source = new LatLng(0, 0);
    LatLng destination = new LatLng(0, 0);
    @BindView(R.id.recycler_route)
    RecyclerView recyclerRoute;
    @BindView(R.id.txt_accept)
    TextView txtAccept;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    ArrayList markerPoints = new ArrayList();
    RouteAdapter routeAdapter;
    List<List<HashMap<String, String>>> routeList = new ArrayList<>();
    ArrayList<RouteModel> data = new ArrayList<>();
    RouteAdapter.OnRouteSelect callback;
    Polyline polyline;
    Bundle bundle;
    String sourceAddress, destinationAddress;
    RouteModel routeModel = new RouteModel();
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        ButterKnife.bind(this);

        callback = this;
        setUpUi();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(MapsActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpUi() {

        setSupportActionBar(toolbar);
        toolbar.setTitle("Select Route");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerRoute = findViewById(R.id.recycler_route);
        routeAdapter = new RouteAdapter(data, MapsActivity.this, callback);
        recyclerRoute.setLayoutManager(new GridLayoutManager(MapsActivity.this, 1, GridLayoutManager.VERTICAL, false));
        recyclerRoute.setAdapter(routeAdapter);
        bundle = getIntent().getBundleExtra("bundle");

        source = new LatLng(bundle.getDouble("latS"), bundle.getDouble("lngS"));
        destination = new LatLng(bundle.getDouble("latD"), bundle.getDouble("lngD"));
        sourceAddress = bundle.getString("addressD");
        destinationAddress = bundle.getString("dest");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 16));

        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }

        // Adding new item to the ArrayList
        markerPoints.add(source);
        markerPoints.add(destination);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(source);
        options.position(destination);

        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        // Add new marker to the Google Map Android API V2

        mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));
        mMap.addMarker(new MarkerOptions().position(destination)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(destinationAddress));
        // Checks, whether start and end locations are captured
        if (markerPoints.size() >= 2) {
            // Getting URL to the Google Directions API
            String url = MyApplication.getDirectionsUrl(source, destination);

            ApiService.getInstance(MapsActivity.this).makeGetCall(MapsActivity.this, url, new ApiService.OnResponse() {
                @Override
                public void onResponseSuccess(JSONObject response) {
                    ParserTask parserTask = new ParserTask(MapsActivity.this);
                    parserTask.execute(response.toString());
                }

                @Override
                public void onError(VolleyError volleyError) {

                }
            });

        }
    }

    @OnClick({R.id.txt_accept, R.id.txt_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_accept:
                StaticData.CURRENT_ROUTE = routeModel;
                setResult(Activity.RESULT_OK, null);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                break;
            case R.id.txt_cancel:
                setResult(Activity.RESULT_CANCELED, null);
                finish();
                overridePendingTransition(R.anim.exit, R.anim.enter);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(Activity.RESULT_CANCELED, null);
        finish();
        overridePendingTransition(R.anim.exit, R.anim.enter);
    }

    @Override
    public void itemSelected(int position, RouteModel model) {
        routeModel = model;
        changeRoute(position);
    }


    /**
     * A class to parse the Google Places in JSON format
     */


    private void changeRoute(int routeNo) {
        if (routeList.size() > routeNo) {

            if (polyline != null) {
                polyline.remove();
            }

            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            points = new ArrayList();
            lineOptions = new PolylineOptions();

            List<HashMap<String, String>> path = routeList.get(routeNo);

            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
                builder.include(position);
            }

            lineOptions.addAll(points);
            lineOptions.width(12);
            lineOptions.color(getResources().getColor(R.color.orange_dark));
            lineOptions.geodesic(true);
            // Drawing polyline in the Google Map for the i-th route
            polyline = mMap.addPolyline(lineOptions);
            LatLngBounds bounds = builder.build();

            int padding = 50; // offset from edges of the map in pixels
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
            mMap.animateCamera(cu);
        }
    }


    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser) {
        routeAdapter = new RouteAdapter(parser.getRouteList(), MapsActivity.this, callback);
        recyclerRoute.setAdapter(routeAdapter);

        routeList = result;
        routeModel = parser.getRouteList().get(0);
        changeRoute(0);
    }
}
