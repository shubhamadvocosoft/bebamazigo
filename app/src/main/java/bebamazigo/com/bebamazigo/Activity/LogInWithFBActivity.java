package bebamazigo.com.bebamazigo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogInWithFBActivity extends AppCompatActivity {

    @BindView(R.id.login_button)
    LoginButton loginButton;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.txt_signup)
    TextView txtSignup;
    @BindView(R.id.ll_button)
    LinearLayout llButton;
    CallbackManager callbackManager;
    @BindView(R.id.img_fb)
    ImageView imgFb;
    @BindView(R.id.spn_user_type)
    Spinner spnUserType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in_with_fb);
        ButterKnife.bind(this);

        spnUserType.setAdapter(new HintAdapter(LogInWithFBActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.user_type)));

        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email"));

        callbackManager = CallbackManager.Factory.create();

        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                final String url = ApiService.FB_TOKEN + "?access_token=" + loginResult.getAccessToken().getToken() + "&realm=" + spnUserType.getSelectedItem().toString().toLowerCase();
                MyApplication.setSharedPrefString(StaticData.SP_FB_USER_ID, loginResult.getAccessToken().getUserId());

                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.v("LoginActivity", response.toString());
                                MyApplication.setSharedPrefString(StaticData.SP_EMAIL, object.optString("email"));
                                String imageUrl = object.optJSONObject("picture").optJSONObject("data").optString("url");
                                MyApplication.setSharedPrefString(StaticData.SP_USER_IMAGE, imageUrl);
                                ApiService.getInstance(LogInWithFBActivity.this).makeGetCall(LogInWithFBActivity.this, url, new ApiService.OnResponse() {
                                    @Override
                                    public void onResponseSuccess(JSONObject response) {
                                        JSONObject user = response.optJSONObject("success").optJSONObject("data").optJSONObject("user");
                                        if (TextUtils.isEmpty(user.optString("email")) || TextUtils.isEmpty(user.optString("mobile"))) {
                                            Bundle bundle = new Bundle();

                                            bundle.putString("realm", user.optString("realm"));
                                            bundle.putString("name", user.optString("name"));
                                            bundle.putString("email", user.optString("email"));
                                            bundle.putString("peopleId", user.optString("id"));
                                            MyApplication.activityStart(LogInWithFBActivity.this, RegisterActivity.class, false, bundle);
                                        } else
                                            MyApplication.saveLogInInfo(LogInWithFBActivity.this, response.optJSONObject("success").optJSONObject("data"));
                                    }

                                    @Override
                                    public void onError(VolleyError volleyError) {
                                        Log.e("error", volleyError.toString());
                                    }
                                });
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                // App code
                Log.v("LoginActivity", "cancel");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.v("LoginActivity", exception.getCause().toString());
            }
        });
    }

    private void sendToken() {
        JSONObject param = new JSONObject();
        try {
            param.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(LogInWithFBActivity.this).makePostCallAuth(LogInWithFBActivity.this, ApiService.UPDATE_FCM, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                goToNextActivity();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void goToNextActivity() {
        if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equalsIgnoreCase("owner")) {
            MyApplication.activityStart(LogInWithFBActivity.this, OwnerHomeActivity.class, true);
        } else if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equalsIgnoreCase("customer")) {
            MyApplication.activityStart(LogInWithFBActivity.this, CustomerHomeActivity.class, true);
        } else
            MyApplication.activityStart(LogInWithFBActivity.this, DriverHomeActivity.class, true);
    }

    @OnClick({R.id.login_button, R.id.txt_login, R.id.txt_signup, R.id.img_fb})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login_button:
                break;
            case R.id.txt_login:
                Bundle bundle = new Bundle();
                bundle.putInt("selectedRealm", spnUserType.getSelectedItemPosition());
                MyApplication.activityStart(LogInWithFBActivity.this, LogInActivity.class, true, bundle);
                break;
            case R.id.txt_signup:
                MyApplication.activityStart(LogInWithFBActivity.this, SignUpActivity.class, true);
                break;
            case R.id.img_fb:
                if (spnUserType.getSelectedItemPosition() != 0)
                    loginButton.performClick();
                else
                    MyApplication.showToast(LogInWithFBActivity.this, "Select a user type");
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

}
