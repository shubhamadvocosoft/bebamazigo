package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TripEndActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.txt_fare)
    TextView txtFare;

    @BindView(R.id.txt_submit)
    TextView txtSubmit;

    String fare, source, destination, jobId;
    TripModel model;
    @BindView(R.id.txt_source)
    TextView txtSource;
    @BindView(R.id.txt_destination)
    TextView txtDestination;
    @BindView(R.id.ll_loc)
    LinearLayout llLoc;
    @BindView(R.id.txt_start_date)
    TextView txtStartDate;
    @BindView(R.id.txt_end_date)
    TextView txtEndDate;
    @BindView(R.id.ll_date)
    LinearLayout llDate;
    @BindView(R.id.rating)
    RatingBar rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trip_end);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Rating");


        Bundle bundle = getIntent().getBundleExtra("bundle");
        fare = bundle.getString("fare");
        source = bundle.getString("source");
        destination = bundle.getString("destination");
        jobId = bundle.getString("jobId");
        model = (TripModel) bundle.getSerializable("data");
        txtFare.setText(fare);
        if (model.getVehicalType().getType().compareTo("Vehicle") == 0) {
            llLoc.setVisibility(View.VISIBLE);
            llDate.setVisibility(View.GONE);
            txtSource.setText(model.getSource().getAddress());
            txtDestination.setText(model.getDestination().getAddress());
        } else {
            llLoc.setVisibility(View.GONE);
            llDate.setVisibility(View.VISIBLE);
            txtStartDate.setText(MyApplication.utcToDate(model.getDate()) + " " + MyApplication.utcToTime(model.getDate()));
            txtEndDate.setText(MyApplication.utcToDate(model.getEndDate()) + " " + MyApplication.utcToTime(model.getEndDate()));
        }


    }

    @OnClick(R.id.txt_submit)
    public void onClick() {
        if (rating.getRating() == 0.0) {
            Toast.makeText(this, "Please Give Rating", Toast.LENGTH_SHORT).show();
        } else {
            submitRating();
        }
    }

    private void submitRating() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", getIntent().getBundleExtra("bundle").getString("jobId"));
            param.put("userRating", Double.parseDouble(rating.getRating() + ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(TripEndActivity.this).makePostCallAuth(TripEndActivity.this, ApiService.GIVE_RATING, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {

                if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareTo("driver") == 0) {
                    MyApplication.activityStart(TripEndActivity.this, DriverHomeActivity.class, true);
                } else if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareTo("owner") == 0) {
                    MyApplication.activityStart(TripEndActivity.this, OwnerHomeActivity.class, true);
                } else {
                    MyApplication.activityStart(TripEndActivity.this, CustomerHomeActivity.class, true);
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public void onBackPressed() {

        if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareTo("driver") == 0) {
            MyApplication.activityStart(TripEndActivity.this, DriverHomeActivity.class, true);
        } else if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareTo("owner") == 0) {
            MyApplication.activityStart(TripEndActivity.this, OwnerHomeActivity.class, true);
        } else {
            MyApplication.activityStart(TripEndActivity.this, CustomerHomeActivity.class, true);
        }

        super.onBackPressed();

    }
}
