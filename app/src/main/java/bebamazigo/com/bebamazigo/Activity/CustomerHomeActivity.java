package bebamazigo.com.bebamazigo.Activity;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import bebamazigo.com.bebamazigo.Adapter.DrawerListAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.Fragments.FragmentEditProfile;
import bebamazigo.com.bebamazigo.Fragments.FragmentReceipt;
import bebamazigo.com.bebamazigo.Fragments.FragmentTrips;
import bebamazigo.com.bebamazigo.util.CustomActivity;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CustomerHomeActivity extends CustomActivity {

    private static CustomerHomeActivity mInstance;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageView)
    CircleImageView imageView;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.lv_drawer)
    ListView lvDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    DrawerListAdapter drawerListAdapter;
    FrameLayout contentContainer;
    FragmentTransaction transaction;
    private float lastTranslate = 0.0f;
    private Menu menu;

    public static CustomerHomeActivity getmInstance() {
        return mInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_home);
        ButterKnife.bind(this);
        mInstance = this;
        contentContainer = findViewById(R.id.contentContainer);
        changeFragment(FragmentTrips.getInstance(), "Home");
        init();
        setDrawer();
    }

    public void changeFragment(Fragment fragment, String title) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.contentContainer, fragment).addToBackStack("fragback").commit();
        /*transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentContainer, fragment);
        transaction.commit();*/
        /*hideMenu(fragment, menu);*/
        toolbar.setTitle(title);
//        changeToolbarTitle(title);
    }

    private void hideMenu(Fragment fragment, Menu menu) {
        if (menu != null) {
            MenuItem notification = menu.findItem(R.id.notification);
            if (fragment instanceof FragmentTrips) {
                notification.setVisible(true);
            } else {
                notification.setVisible(false);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.contentContainer);
            if (f instanceof FragmentTrips) {
                showExitDialog();
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                changeFragment(FragmentTrips.getInstance(), "Home");
                /*getSupportFragmentManager().popBackStack();*/
            } else {
                showExitDialog();
            }
        }
    }

    private void showExitDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(CustomerHomeActivity.this)
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.activityFinish(CustomerHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        }
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.notification) {
            MyApplication.activityStart(CustomerHomeActivity.this, NotificationActivity.class, false);
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        setSupportActionBar(toolbar);
    }

    private void setDrawer() {
        textName.setText(MyApplication.getSharedPrefString(StaticData.SP_USER_NAME));
        if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE))) {
            Log.d("profile", MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE));
            Glide.with(CustomerHomeActivity.this)
                    .load((MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)))
                    .centerCrop()
                    .into(imageView);
        }
        Log.e("picUrl", (MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)));


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (lvDrawer.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    contentContainer.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    contentContainer.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };


        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(CustomerHomeActivity.this);
        drawerListAdapter.addMenu("Home", R.drawable.home_white, R.drawable.home_white);
        drawerListAdapter.addMenu("Receipt", R.drawable.receipt_white, R.drawable.receipt_white);
        drawerListAdapter.addMenu("Update Profile", R.drawable.update_profile_white, R.drawable.update_profile_white);
        drawerListAdapter.addMenu("Wallet", R.drawable.wallet_white, R.drawable.wallet_white);
        drawerListAdapter.addMenu("Logout", R.drawable.logout_white, R.drawable.logout_white);
        lvDrawer.setAdapter(drawerListAdapter);

        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                drawerListAdapter.setSelectedPosition(position);
                if (position == 0) {
                    drawerLayout.closeDrawers();
                    changeFragment(FragmentTrips.getInstance(), "Home");
                } else if (position == 1) {
                    drawerLayout.closeDrawers();
                    changeFragment(FragmentReceipt.getInstance(), "Receipt");
                } else if (position == 2) {
                    drawerLayout.closeDrawers();
                    changeFragment(FragmentEditProfile.getInstance(), "Update Profile");
                } else if (position == 3) {
                    drawerLayout.closeDrawers();
                    MyApplication.activityStart(CustomerHomeActivity.this, WalletActivity.class, false);
                } else if (position == 4) {
                    if (drawerLayout != null) {
                        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                            drawerLayout.closeDrawer(GravityCompat.START);
                        }
                    }
                    showLogOutDialog();
                }
            }
        });

    }

    private void showLogOutDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(CustomerHomeActivity.this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.logout(CustomerHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

}
