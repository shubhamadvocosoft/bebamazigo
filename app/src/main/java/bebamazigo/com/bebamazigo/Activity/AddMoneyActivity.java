package bebamazigo.com.bebamazigo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMoneyActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    final int GET_NEW_CARD = 2;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.txt)
    TextView txt;
    @BindView(R.id.radio_mpesa)
    RadioButton radioMpesa;
    @BindView(R.id.radio_card)
    RadioButton radioCard;
    @BindView(R.id.radio_group)
    RadioGroup radioGroup;
    @BindView(R.id.txt_card_detail)
    TextView txtCardDetail;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_number)
    TextView txtNumber;
    @BindView(R.id.txt_expiration)
    TextView txtExpiration;
    @BindView(R.id.ll_expiration)
    LinearLayout llExpiration;
    @BindView(R.id.txt_cvv)
    TextView txtCvv;
    @BindView(R.id.ll_detail)
    LinearLayout llDetail;
    @BindView(R.id.txt_add)
    TextView txtAdd;
    @BindView(R.id.txt_amount)
    EditText txtAmount;
    @BindView(R.id.txt_mobile)
    EditText txtMobile;
    @BindView(R.id.ll_wallet_mobile)
    LinearLayout ll_wallet_mobile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_money);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Add Money");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        radioGroup.setOnCheckedChangeListener(AddMoneyActivity.this);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(AddMoneyActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        if (checkedId == radioMpesa.getId()) {
            txtCardDetail.setVisibility(View.GONE);
            llDetail.setVisibility(View.GONE);
            ll_wallet_mobile.setVisibility(View.VISIBLE);
        } else {
            txtCardDetail.setVisibility(View.GONE);
            ll_wallet_mobile.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            /*String cardHolderName = data.getStringExtra(CreditCardUtils.EXTRA_CARD_HOLDER_NAME);
            String cardNumber = data.getStringExtra(CreditCardUtils.EXTRA_CARD_NUMBER);
            String expiry = data.getStringExtra(CreditCardUtils.EXTRA_CARD_EXPIRY);
            String cvv = data.getStringExtra(CreditCardUtils.EXTRA_CARD_CVV);

            txtName.setText(cardHolderName);
            txtNumber.setText(cardNumber);
            txtExpiration.setText(expiry);
            txtCvv.setText(cvv);
            llDetail.setVisibility(View.VISIBLE);*/
        }

    }

    @OnClick({R.id.txt_card_detail, R.id.txt_add})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_card_detail:
                /*Intent intent = new Intent(AddMoneyActivity.this, CardEditActivity.class);
                startActivityForResult(intent, GET_NEW_CARD);*/
                break;
            case R.id.txt_add:
                if (radioGroup.getCheckedRadioButtonId() == radioMpesa.getId()) {
                    if (txtAmount.getText().toString().isEmpty()) {
                        MyApplication.showToast(this, "Please Enter Amount");
                    } else if (txtMobile.getText().toString().isEmpty()) {
                        MyApplication.showToast(this, "Please Enter Mobile");
                    } else {
                        addMpesa();
                    }
                } else if (radioGroup.getCheckedRadioButtonId() == radioCard.getId()) {
                    if (txtAmount.getText().toString().isEmpty()) {
                        MyApplication.showToast(this, "Please Enter Amount");
                    } else {
                        addCard();
                    }
                }
                break;
        }
    }

    private void addMpesa() {
        JSONObject param = new JSONObject();
        try {
            param.put("mobileNumber", txtMobile.getText());
            param.put("amount", Integer.parseInt(txtAmount.getText().toString()));

            Log.d("Param's : ", "mobileNumber" + " : " + txtMobile.getText().toString() + " " + "amount" + " : " + Integer.parseInt(txtAmount.getText().toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(AddMoneyActivity.this).makePostCallAuth(AddMoneyActivity.this, ApiService.Add_MONEY_MPESA,
                param, new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("Response", response.toString());

                        if (response.optJSONObject("success") != null) {

                            JSONObject data = response.optJSONObject("success").optJSONObject("data");

                            if (data.optString("paymentStatus").equalsIgnoreCase("pending")) {
                                if (data.optString("id") != null) {
                                    Intent intent = new Intent(AddMoneyActivity.this, MPesaPaymentActivity.class);
                                    intent.putExtra("trans_id", data.optString("id"));
                                    startActivity(intent);
                                    finish();
                                } else {
                                    MyApplication.showToast(AddMoneyActivity.this, "Something Went Wrong");
                                }
                            } else {
                                MyApplication.showToast(AddMoneyActivity.this, "Something Went Wrong");
                            }
                        } else {
                            MyApplication.showToast(AddMoneyActivity.this, "Something Went Wrong");
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        MyApplication.showToast(AddMoneyActivity.this, "Something went wrong.");
                    }
                });
    }

    private void addCard() {
        JSONObject param = new JSONObject();
        try {
            param.put("amount", Integer.parseInt(txtAmount.getText().toString()));
            /*param.put("fName", txtName.getText().toString().split(" ")[0]);
            param.put("lName", txtName.getText().toString().split(" ")[1]);
            param.put("cardNumber", Long.parseLong(txtNumber.getText().toString()));
            param.put("month", Integer.parseInt(txtExpiration.getText().toString().split("/")[0]));
            param.put("year", Integer.parseInt(txtExpiration.getText().toString().split("/")[1]));
            param.put("cvv", Integer.parseInt(txtCvv.getText().toString()));*/
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(AddMoneyActivity.this).makePostCallAuth(AddMoneyActivity.this, ApiService.Add_MONEY_CARD,
                param, new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("Response", response.toString());

                        if (response.optJSONObject("success") != null) {

                            JSONObject data = response.optJSONObject("success").optJSONObject("data");

                            String url = data.optString("url");
                            if (url != null) {
                                Intent intent = new Intent(AddMoneyActivity.this, CardPaymentActivity.class);
                                intent.putExtra("url", url);
                                startActivity(intent);
                                finish();
                            } else {
                                MyApplication.showToast(AddMoneyActivity.this, "Something Went Wrong");
                            }

                        } else {
                            MyApplication.showToast(AddMoneyActivity.this, "Something Went Wrong");
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        MyApplication.showToast(AddMoneyActivity.this, "Something went wrong.");
                    }
                });
    }
}
