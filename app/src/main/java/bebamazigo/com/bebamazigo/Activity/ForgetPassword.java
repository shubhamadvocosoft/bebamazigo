package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.TouchEffect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPassword extends AppCompatActivity {

    @BindView(R.id.ll_mobile_input)
    LinearLayout llMobileInput;

    @BindView(R.id.edt_user_mobile)
    EditText edtUserMobile;
    @BindView(R.id.txt_submit_mobile)
    TextView txtSubmitMobile;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;

    @BindView(R.id.ll_button_mobile)
    LinearLayout llButtonMobile;

    @BindView(R.id.ll_password_input)
    LinearLayout llPasswordInput;

    @BindView(R.id.edt_user_otp)
    EditText edtUserOtp;
    @BindView(R.id.edt_user_newpassword)
    EditText edtUserNewpassword;
    @BindView(R.id.edt_user_confirmpassword)
    EditText edtUserConfirmpassword;
    @BindView(R.id.txt_submit_newpassword)
    TextView txtSubmitNewpassword;
    @BindView(R.id.txt_cancel_password)
    TextView txtCancelPassword;
    @BindView(R.id.txt_resent_otp)
    TextView txtResendOtp;

    @BindView(R.id.ll_button_password)
    LinearLayout llButtonPassword;

    private String selectedRealm = "";
    private String otp = "";
    private String peopleId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);

        try {
            Bundle bundle = getIntent().getBundleExtra("bundle");
            selectedRealm = bundle.getString("selectedRealm");
        } catch (Exception e) {
            e.printStackTrace();
        }

        llMobileInput.setVisibility(View.VISIBLE);
        llPasswordInput.setVisibility(View.GONE);

        txtSubmitMobile.setOnTouchListener(new TouchEffect());
        txtSubmitNewpassword.setOnTouchListener(new TouchEffect());
        txtCancel.setOnTouchListener(new TouchEffect());
        txtCancelPassword.setOnTouchListener(new TouchEffect());
        txtResendOtp.setOnTouchListener(new TouchEffect());

    }

    @OnClick({R.id.txt_submit_mobile, R.id.txt_cancel, R.id.txt_submit_newpassword, R.id.txt_cancel_password, R.id.txt_resent_otp})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_submit_mobile:
                if(mobileValidation()) {
                    //RunApiHere
                    forgetPasswordRequest();
                }
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(ForgetPassword.this);
                break;
            case R.id.txt_submit_newpassword:
                if(passwordValidation()) {
                    //RunApiHere
                    resetPasswordRequest();
                } else {
                    break;
                }
                break;
            case R.id.txt_cancel_password:
                MyApplication.activityFinish(ForgetPassword.this);
                break;
            case R.id.txt_resent_otp:
                forgetPasswordRequest();
                break;
        }
    }

    private boolean mobileValidation() {
        if(edtUserMobile.getText().toString().isEmpty()) {
            edtUserMobile.setError("Enter Mobile Number");
            edtUserMobile.requestFocus();
            return false;
        } else {
            return true;
        }
    }

    private boolean passwordValidation() {
        if(edtUserOtp.getText().toString().isEmpty()) {
            edtUserOtp.setError("Enter OTP");
            edtUserOtp.requestFocus();
            return false;
        } else if(edtUserOtp.getText().toString().length() < 4) {
            edtUserOtp.setError("Enter Correct OTP");
            edtUserOtp.requestFocus();
            return false;
        } else if(edtUserNewpassword.getText().toString().isEmpty()) {
            edtUserNewpassword.setError("Enter New Password");
            edtUserNewpassword.requestFocus();
            return false;
        } else if(edtUserConfirmpassword.getText().toString().isEmpty()) {
            edtUserConfirmpassword.setError("Enter Confirm Password");
            edtUserConfirmpassword.requestFocus();
            return false;
        } else if(edtUserNewpassword.getText().toString().length() < 6) {
            edtUserNewpassword.setError("Password Length Should Be 6");
            edtUserNewpassword.requestFocus();
            return false;
        } else if(edtUserConfirmpassword.getText().toString().length() < 6) {
            edtUserConfirmpassword.setError("Password Length Should Be 6");
            edtUserConfirmpassword.requestFocus();
            return false;
        } else if(!edtUserNewpassword.getText().toString().equalsIgnoreCase(edtUserConfirmpassword.getText().toString())) {
            edtUserNewpassword.setError("New Password And Confirm Password Should Be Same");
            edtUserConfirmpassword.setError("New Password And Confirm Password Should Be Same");
            return false;
        } else {
            return true;
        }
    }

    //ForgetPasswordApi
    private void forgetPasswordRequest() {
        ApiService.getInstance(ForgetPassword.this).makePostCall(ForgetPassword.this, ApiService.FORGET_PASSWORD, getForgetPasswordParam(), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("ForgetPassResponse", response.toString());
                JSONObject responseSuccess = response.optJSONObject("success");
                if (responseSuccess != null) {

                    JSONObject msg = responseSuccess.optJSONObject("msg");
                    if(msg.optString("replyCode").equalsIgnoreCase("Success")) {
                        JSONObject data = responseSuccess.optJSONObject("data");
                        otp = data.optJSONObject("resetOtp").optString("otp");
                        peopleId = data.optString("id");

                        MyApplication.showToast(ForgetPassword.this,"OTP Sent Successfully");

                        llMobileInput.setVisibility(View.GONE);
                        llPasswordInput.setVisibility(View.VISIBLE);
                    }
                }


            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
    //ForgetPasswordParams
    private JSONObject getForgetPasswordParam() {
        JSONObject param = new JSONObject();
        try {
            param.put("mobile", edtUserMobile.getText().toString());
            param.put("type", selectedRealm.toLowerCase());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Params", param.toString());
        return param;
    }
    //ResetPasswordApi
    private void resetPasswordRequest() {
        ApiService.getInstance(ForgetPassword.this).makePostCall(ForgetPassword.this, ApiService.SUBMIT_NEW_PASSWORD, getResetPasswordParam(), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("SubmitPassResponse", response.toString());
                JSONObject responseSuccess = response.optJSONObject("success");
                if (responseSuccess != null) {
                    JSONObject msg = responseSuccess.optJSONObject("msg");
                    if(msg.optString("replyCode").equalsIgnoreCase("Success")) {
                        MyApplication.showToast(ForgetPassword.this,"Password Reset Successfully Please Login!!");
                        MyApplication.activityFinish(ForgetPassword.this);
                    }
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
    //ResetPasswordParams
    private JSONObject getResetPasswordParam() {
        JSONObject param = new JSONObject();
        try {
            param.put("peopleId", peopleId);
            param.put("otp", Integer.parseInt(edtUserOtp.getText().toString()));
            param.put("newPassword", edtUserNewpassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("Params", param.toString());
        return param;
    }

}
