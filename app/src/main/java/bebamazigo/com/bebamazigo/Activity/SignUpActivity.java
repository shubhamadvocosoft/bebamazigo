package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.CustomActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends CustomActivity {

    @BindView(R.id.spn_user_type)
    Spinner spnUserType;
    @BindView(R.id.txt_next)
    TextView txtNext;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        spnUserType.setAdapter(new HintAdapter(SignUpActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.user_type_signup)));
    }

    @OnClick({R.id.txt_next, R.id.txt_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_next:
                if (spnUserType.getSelectedItemPosition() == 0) {
                    MyApplication.showToast(SignUpActivity.this, "Select a user type");
                } else {
                    goToRegisterPage(spnUserType.getSelectedItemPosition());
                }
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(SignUpActivity.this);
        }
    }

    private void goToRegisterPage(int selectedItemPosition) {
        Bundle bundle = new Bundle();
        bundle.putInt("user_type", selectedItemPosition);
        MyApplication.activityStart(SignUpActivity.this, RegisterActivity.class, false, bundle);
    }

}
