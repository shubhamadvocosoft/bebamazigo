package bebamazigo.com.bebamazigo.Activity;

import android.animation.ValueAnimator;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.CustomActivity;
import bebamazigo.com.bebamazigo.util.DirectionsJSONParser;
import bebamazigo.com.bebamazigo.util.ParserTask;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShowTripActivity extends CustomActivity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback, ParserTask.ParserCallback {

    TripModel model;
    ArrayList markerPoints = new ArrayList();
    LatLng source = new LatLng(0, 0);
    LatLng destination = new LatLng(0, 0);
    String sourceAddress, destinationAddress;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.bottomSheetHeading)
    TextView bottomSheetHeading;

    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_description)
    TextView txtDescription;
    @BindView(R.id.txt_source)
    TextView txtSource;
    @BindView(R.id.txt_destination)
    TextView txtDestination;
    @BindView(R.id.txt_vehicle)
    TextView txtVehicle;
    @BindView(R.id.txt_weight)
    TextView txtWeight;
    @BindView(R.id.txt_budget)
    TextView txtBudget;
    @BindView(R.id.txt_price)
    TextView txtPrice;
    @BindView(R.id.txt_distance)
    TextView txtDistance;
    @BindView(R.id.budget_ll)
    LinearLayout budgetLl;

    Polyline polyline;
    int counter = 2;
    Marker sourceMarker, carMarker;
    Gson gson = new Gson();
    String jobId;
    Timer timer;
    List<HashMap<String, String>> routePoints;
    int test = 0;
    private GoogleMap mMap;

    Runnable runnableGetLocation = new Runnable() {
        public void run() {
            try {
                DatabaseReference mFirebaseDatabase = FirebaseDatabase.getInstance().getReference("Locations").child(model.getId());

                mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String, String> value = (Map<String, String>) dataSnapshot.getValue();
                        Log.i("lat lng", "lat lng json" + new JSONObject(value));

                        JSONObject obj = new JSONObject(value);
                        String lat = obj.optString("currntLatt");
                        String lng = obj.optString("currntLong");
                        if (!MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
                                !MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

                            Location location = new Location("");
                            location.setLatitude(Double.parseDouble(lat));
                            location.setLongitude(Double.parseDouble(lng));

                            if (TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT))) {
                                location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                            } else {
                                location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG))),
                                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                            }
                            animateMarker(location, carMarker);
                            MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LAT, lat);
                            MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LONG, lng);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                /*ApiService.getInstance().makeGetCall(null, ApiService.GET_DRIVER_LOCATION + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&driverId=" + model.getDriverId(), new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("RESPONSE",response.toString());
                        JSONObject data = response.optJSONObject("success").optJSONObject("data");
                        String lat = data.optString("lat");
                        String lng = data.optString("lng");
                        if (!MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
                                !MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

                            Location location = new Location("");
                            location.setLatitude(Double.parseDouble(lat));
                            location.setLongitude(Double.parseDouble(lng));

                            if (TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT))) {
                                location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                            } else {
                                location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG))),
                                        new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                            }
                            animateMarker(location, carMarker);
                            MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LAT, lat);
                            MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LONG, lng);
                        }

                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                            //MyApplication.showToast(ShowTripActivity.this, "ERROR");
                    }
                });*/

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private int mInterval = 1000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private BottomSheetBehavior bottomSheetBehavior;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(ShowTripActivity.this);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_trip);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Trip");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Trip");

        setBottomSheet();
        timer = new Timer();
        mHandler = new Handler();
        jobId = getIntent().getBundleExtra("bundle").getString("jobId");
        if (!TextUtils.isEmpty(jobId))
            getJob();

    }

    @Override
    protected void onStart() {
        super.onStart();
        //Register this fragment to listen to event.
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void getJob() {
        String url = ApiService.GET_JOB_ID + "&jobId=" + jobId;
        ApiService.getInstance(ShowTripActivity.this).makeGetCall(ShowTripActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), TripModel.class);

                source = new LatLng(model.getSource().getLocation().getLat(), model.getSource().getLocation().getLng());
                if (model.getDestination() != null) {
                    destination = new LatLng(model.getDestination().getLocation().getLat(), model.getDestination().getLocation().getLng());
                    destinationAddress = model.getDestination().getAddress();
                }
                sourceAddress = model.getSource().getAddress();

                if (model.getStatus().compareTo("end") == 0) {
                    if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareToIgnoreCase("customer") == 0) {
                        showCompleteDialog();
                    } else {

                    }

                }

                setMap();

                setDetail();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setDetail() {
        if (model.getDestination() == null) {
            txtBudget.setText(model.getBudget().toString() + " KSH");
            txtDate.setText(MyApplication.utcToDate(model.getDate()));
            txtDescription.setText(model.getDescription());
            txtSource.setText(model.getSource().getAddress());
            txtPrice.setText(model.getNetPrice().toString() + " KSH");
            txtTitle.setText(model.getTitle());
            txtVehicle.setText(model.getTypeOfVehicle());
        } else {
            if (model.getBudget() != null) {
                txtBudget.setText(model.getBudget().toString() + " KSH");
            } else {
                txtBudget.setText("");
                budgetLl.setVisibility(View.GONE);
            }

            txtDate.setText(MyApplication.utcToDate(model.getDate()));
            txtDescription.setText(model.getDescription());
            txtDestination.setText(model.getDestination().getAddress());
            txtSource.setText(model.getSource().getAddress());
            txtPrice.setText(model.getNetPrice().toString() + " KSH");
            txtTitle.setText(model.getTitle());
            txtWeight.setText(model.getWeight() + " KG");

            try {
                if (Long.parseLong(model.getDistance().getValue()) < 1000)
                    txtDistance.setText(model.getDistance().getValue() + " Meters");
                else
                    txtDistance.setText(Long.parseLong(model.getDistance().getValue()) / 1000 + " KM");
            } catch (NumberFormatException e) {

            }


            txtVehicle.setText(model.getTypeOfVehicle());

            getRoute();
        }
    }

    private void getRoute() {
        String url = MyApplication.getDirectionsUrl(source, destination);

        ApiService.getInstance(ShowTripActivity.this).makeGetCall(ShowTripActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ParserTask parserTask = new ParserTask(ShowTripActivity.this);
                parserTask.execute(response.toString());
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(View bottomSheet, int newState) {

                // Check Logs to see how bottom sheets behaves
                switch (newState) {
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        Log.e("Bottom Sheet Behaviour", "STATE_COLLAPSED");
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        Log.e("Bottom Sheet Behaviour", "STATE_DRAGGING");
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        Log.e("Bottom Sheet Behaviour", "STATE_EXPANDED");
                        break;
                    case BottomSheetBehavior.STATE_HIDDEN:
                        Log.e("Bottom Sheet Behaviour", "STATE_HIDDEN");
                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        Log.e("Bottom Sheet Behaviour", "STATE_SETTLING");
                        break;
                }
            }


            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }
        MarkerOptions options = new MarkerOptions();
        //Adding new item to the ArrayList

        markerPoints.add(source);
        options.position(source);
        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));

        if (model.getDestination() != null) {
            markerPoints.add(destination);
            options.position(destination);
            if (markerPoints.size() == 1) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
            } else if (markerPoints.size() == 2) {
                options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
            }
            carMarker = mMap.addMarker(new MarkerOptions().position(source)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_truck))
                    .title(sourceAddress));


            mMap.addMarker(new MarkerOptions().position(destination)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    .title(destinationAddress));
        }
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        //Creating MarkerOptions


        //Setting the position of the marker


        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }


        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));
        carMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_truck))
                .title(sourceAddress));


        mMap.addMarker(new MarkerOptions().position(destination)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(destinationAddress));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));


        //Checks, whether start and end locations are captured
    }

    @Override
    public void onBackPressed() {
        MyApplication.activityFinish(ShowTripActivity.this);
    }

    private void showRoute(List<HashMap<String, String>> data) {

        if (polyline != null) {
            polyline.remove();
        }

        ArrayList points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();
        //LatLngBounds.Builder builder = new LatLngBounds.Builder();
        points = new ArrayList();
        lineOptions = new PolylineOptions();

        for (int j = 0; j < data.size(); j++) {
            HashMap<String, String> point = data.get(j);

            double lat = Double.parseDouble(point.get("lat").toString());
            double lng = Double.parseDouble(point.get("lng").toString());

            LatLng position = new LatLng(lat, lng);

            points.add(position);
            //builder.include(position);
        }

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(getResources().getColor(R.color.orange_dark));
        lineOptions.geodesic(true);
        //Drawing polyline in the Google Map for the i-th route
        polyline = mMap.addPolyline(lineOptions);
        //LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);


        //CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        //mMap.animateCamera(cu);
    }

    @Override
    public void onMapLoaded() {

    }

    private float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        Double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng.floatValue();
    }

    void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {

            /*new code*/
            final LatLng startPosition = carMarker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {

                    float v = valueAnimator.getAnimatedFraction();
                    double lng = v * endPosition.longitude + (1 - v) * startPosition.longitude;
                    double lat = v * endPosition.latitude + (1 - v) * startPosition.latitude;

                    LatLng newPos = new LatLng(lat, lng);
                    Location location1 = new Location("");
                    location1.setLatitude(startPosition.latitude);
                    location1.setLongitude(startPosition.longitude);

                    Location location2 = new Location("");
                    location2.setLatitude(newPos.latitude);
                    location2.setLongitude(newPos.longitude);

                    float bear = location1.bearingTo(location2);
                    Log.d("distance", location1.distanceTo(location2) + "");
                    Log.d("accuracy", location2.getAccuracy() + "");
                    Log.d("total", location1.distanceTo(location2) - location2.getAccuracy() + "");
                    Log.d("update_bearing", bear + "");

                    if (location1.distanceTo(location2) - location2.getAccuracy() > 8) {
                        carMarker.setPosition(newPos);
                        carMarker.setAnchor(0.5f, 0.5f);

                        carMarker.setRotation(bear);

                        if (mMap != null) {
                            mMap.moveCamera(CameraUpdateFactory
                                    .newCameraPosition
                                            (new CameraPosition.Builder()
                                                    .target(newPos)
                                                    .zoom(18.5f)
                                                    .build()));
                        }
                    }
                }
            });
            valueAnimator.start();
        }
    }

    /*void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                    }
                }

            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            valueAnimator.start();
            counter++;
        }
    }*/

    float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }

    /*Runnable latLngThrower = new Runnable() {
        @Override
        public void run() {
            try {
                if (counter < routePoints.size()) {
                    Location location = new Location("");

                    final LatLng current = new LatLng(Double.parseDouble(routePoints.get(counter).get("lat")),
                            Double.parseDouble(routePoints.get(counter).get("lng")));
                    LatLng pre = new LatLng(Double.parseDouble(routePoints.get(counter - 1).get("lat")),
                            Double.parseDouble(routePoints.get(counter - 1).get("lng")));


                    location.setLatitude(current.latitude);
                    location.setLongitude(current.longitude);


                    location.setBearing(bearingBetweenLocations(pre, current));

                    animateMarker(location, carMarker);


                    Handler handler1 = new Handler();
                    handler1.post(new Runnable() {
                        public void run() {
                            JSONObject param = new JSONObject();
                            JSONObject locationObject = new JSONObject();

                            try {
                                locationObject.put("lat", current.latitude);
                                locationObject.put("lng", current.longitude);

                                param.put("location", locationObject);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            ApiService.getInstance().makePostCallAuth(null, ApiService.UPDATE_DRIVER_LOCATION, param, new ApiService.OnResponse() {
                                @Override
                                public void onResponseSuccess(JSONObject response) {
//                            MyApplication.showToast(null, "Location Updated");
                                }

                                @Override
                                public void onError(VolleyError volleyError) {
//                            MyApplication.showToast(ShowTripActivity.this, "Update Location Failed");
                                }
                            });
                        }
                    });


                }
            } finally {
                mHandler.postDelayed(latLngThrower, mInterval);
            }
        }
    };*/

    @Override
    protected void onDestroy() {
        mHandler.removeCallbacks(runnableGetLocation);
        super.onDestroy();

    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser) {
        for (int i = 0; i < parser.getRouteList().size(); i++) {
            Log.d("model_route", model.getDistance().getText());
            Log.d("loop_root", parser.getRouteList().get(i).getDistance() + "");
            //  parser.getRouteList().get(i).getSelectedRoute();

            if (parser.getRouteList().get(i).getDistance().equals(model.getDistance().getText())) {
                routePoints = result.get(i);
                showRoute(routePoints);

                if (!TextUtils.isEmpty(model.getDriverId()) && MyApplication.getSharedPrefString(StaticData.SP_REALM).equals("customer"))
                    callAsynchronousTask();
                break;
            }

        }
    }

    public void callAsynchronousTask() {
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                mHandler.post(runnableGetLocation);
            }
        };
        timer.schedule(doAsynchronousTask, 0, 2000); //execute in every 50000 ms
    }

    private void showCompleteDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View yourCustomView = inflater.inflate(R.layout.dialog_complete_job, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(yourCustomView)
                .create();

        TextView txtComplete = (TextView) yourCustomView.findViewById(R.id.txt_complete);
        TextView txtDispute = (TextView) yourCustomView.findViewById(R.id.txt_dispute);

        txtComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                jobComplete();
            }
        });
        txtDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showDisputeDialog();

            }
        });


        dialog.show();
    }

    private void showDisputeDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View yourCustomView = inflater.inflate(R.layout.dialog_dispute, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(yourCustomView)
                .create();
        final EditText editComment = (EditText) yourCustomView.findViewById(R.id.edit_comment);
        TextView txtSubmit = (TextView) yourCustomView.findViewById(R.id.txt_submit);

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editComment.getText().toString().isEmpty()) {
                    MyApplication.showToast(ShowTripActivity.this, "comment something!!!");
                } else {
                    dialog.dismiss();
                    jobDispute(editComment.getText().toString());
                }
            }
        });
        dialog.show();
    }

    private void jobComplete() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("status", "completed");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("auth..", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
        ApiService.getInstance(ShowTripActivity.this).makePostCallAuth(ShowTripActivity.this, ApiService.COMPLETE_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(ShowTripActivity.this, "Job completed!!!");

                Bundle bundle = new Bundle();
                bundle.putString("fare", model.getNetPrice().toString());
                bundle.putString("source", model.getSource().getAddress());
                bundle.putString("destination", model.getDestination().getAddress());
                bundle.putString("jobId", jobId);
                bundle.putSerializable("data", (Serializable) model);
                MyApplication.activityStart(ShowTripActivity.this, TripEndActivity.class, false, bundle);
                finish();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void jobDispute(String comment) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("status", "dispute");
            param.put("comment", comment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(ShowTripActivity.this).makePostCallAuth(ShowTripActivity.this, ApiService.COMPLETE_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(ShowTripActivity.this, "Job Disputed Successfully!!!");

                finish();

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }
}