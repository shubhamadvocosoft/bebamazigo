package bebamazigo.com.bebamazigo.Activity;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import bebamazigo.com.bebamazigo.util.TouchEffect;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LogInActivity extends AppCompatActivity {

    @BindView(R.id.spn_user_type)
    Spinner spnUserType;
    @BindView(R.id.edt_user)
    EditText edtUser;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.txt_forgot)
    TextView txtForgot;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.txt_reset)
    TextView txtReset;
    @BindView(R.id.txt_new_registration)
    TextView txtNewRegistration;
    Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        /*printKey();*/
        txtLogin.setOnTouchListener(new TouchEffect());
        txtReset.setOnTouchListener(new TouchEffect());
        spnUserType.setAdapter(new HintAdapter(LogInActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.user_type)));

        edtUser.setText("@mailinator.com");
        edtPassword.setText("123456");

        try {
            Bundle bundle = getIntent().getBundleExtra("bundle");
            int selectedRealm = bundle.getInt("selectedRealm");
            spnUserType.setSelection(selectedRealm);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.txt_forgot, R.id.txt_login, R.id.txt_reset, R.id.txt_new_registration})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_forgot:
                //ForgetPasswordWorkHere
                if (spnUserType.getSelectedItemPosition() == 0) {
                    MyApplication.showToast(LogInActivity.this, "Select a User Type");
                } else {
                    Bundle bundle = new Bundle();
                    bundle.putString("selectedRealm", spnUserType.getSelectedItem().toString());
                    MyApplication.activityStart(LogInActivity.this, ForgetPassword.class, false, bundle);
                }
                break;
            case R.id.txt_login:
                if (MyApplication.isConnectingToInternet(LogInActivity.this)) {
                    if (isValid()) {
                        logIn();
                    }
                } else {
                    MyApplication.showToast(LogInActivity.this, StaticData.NO_INTERNET);
                }
                break;
            case R.id.txt_reset:
                spnUserType.setSelection(0);
                edtUser.setText("");
                edtPassword.setText("");
                break;
            case R.id.txt_new_registration:
                MyApplication.activityStart(LogInActivity.this, SignUpActivity.class, false);
                break;
        }
    }

    private void logIn() {
        ApiService.getInstance(LogInActivity.this).makePostCall(LogInActivity.this, ApiService.LOG_IN, getParam(), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("Response", response.toString());
                MyApplication.saveLogInInfo(LogInActivity.this, response.optJSONObject("success").optJSONObject("data"));
            }

            @Override
            public void onError(VolleyError volleyError) {
                try {
                    Log.d("error_res", volleyError.getMessage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private JSONObject getParam() {
        JSONObject param = new JSONObject();
        try {
            param.put("realm", spnUserType.getSelectedItem().toString().toLowerCase().trim());
            param.put("email", edtUser.getText().toString().trim());
            param.put("password", edtPassword.getText().toString().trim());
            param.put("firebaseToken", FirebaseInstanceId.getInstance().getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private boolean isValid() {
        if (spnUserType.getSelectedItemPosition() == 0) {
            MyApplication.showToast(LogInActivity.this, "Select a User Type");
            return false;
        }
        if (TextUtils.isEmpty(edtUser.getText())) {
            edtUser.setError(StaticData.BLANK);
            return false;
        }
        if (!MyApplication.isEmailValid(edtUser.getText().toString())) {
            edtUser.setError("Invalid Email address");
            return false;
        }
        if (TextUtils.isEmpty(edtPassword.getText())) {
            edtPassword.setError(StaticData.BLANK);
            return false;
        }
        return true;
    }

    private void printKey() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String string = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.e("MY KEY HASH:", string);
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

}
