package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.DirectionsJSONParser;
import bebamazigo.com.bebamazigo.util.ParserTask;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.shadowfax.proswipebutton.ProSwipeButton;

public class NotificationJobActivity extends AppCompatActivity implements OnMapReadyCallback, ProSwipeButton.OnSwipeListener, ParserTask.ParserCallback {

    ArrayList markerPoints = new ArrayList();
    LatLng source = new LatLng(0, 0);
    LatLng destination = new LatLng(0, 0);
    String sourceAddress, destinationAddress;

    @BindView(R.id.bottomSheetHeading)
    TextView bottomSheetHeading;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_description)
    TextView txtDescription;
    @BindView(R.id.txt_source)
    TextView txtSource;
    @BindView(R.id.txt_destination)
    TextView txtDestination;
    @BindView(R.id.txt_vehicle)
    TextView txtVehicle;
    @BindView(R.id.txt_weight)
    TextView txtWeight;
    @BindView(R.id.txt_budget)
    TextView txtBudget;
    @BindView(R.id.txt_price)
    TextView txtPrice;
    TripModel model = new TripModel();
    Gson gson = new Gson();
    @BindView(R.id.txt_distance)
    TextView txtDistance;
    @BindView(R.id.swipe_accept)
    ProSwipeButton swipeAccept;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.bottomSheetLayout)
    LinearLayout bottomSheetLayout;
    @BindView(R.id.scrollView)
    ScrollView scrollView;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.ll_vehicle)
    LinearLayout llVehicle;

    @BindView(R.id.ll_mach)
    LinearLayout llMach;
    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.txt_title_mac)
    TextView txtTitleMac;
    @BindView(R.id.txt_machinary_type)
    TextView txtMachinaryType;
    @BindView(R.id.txt_job_loc)
    TextView txtJobLoc;
    @BindView(R.id.txt_start_date)
    TextView txtStartDate;
    @BindView(R.id.txt_end_date)
    TextView txtEndDate;
    @BindView(R.id.txt_budget_mac)
    TextView txtBudgetMac;
    @BindView(R.id.txt_discription)
    TextView txtDiscription;
    @BindView(R.id.btn_accept)
    Button btnAccept;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    Polyline polyline;
    int counter = 0;
    Marker sourceMarker;
    String jobId = "";
    boolean isCameAfterAccept = false;
    String preLat = "", preLng = "";
    private GoogleMap mMap;
    private int mInterval = 500; // 5 seconds by default, can be changed later
    private Handler mHandler;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(NotificationJobActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_job);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Job");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        swipeAccept.setOnSwipeListener(NotificationJobActivity.this);

        Bundle b = getIntent().getExtras();
        jobId = b.getString("jobId");
        isCameAfterAccept = b.getBoolean("isCameAfterAccept", false);

        if (!TextUtils.isEmpty(jobId))
            getJob();

        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptJob(jobId);
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jobCancel();
            }
        });
    }

    private void getJob() {
        String url = ApiService.GET_JOB_ID + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&jobId=" + jobId;
        ApiService.getInstance(NotificationJobActivity.this).makeGetCall(NotificationJobActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("Response", response.toString());
                model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), TripModel.class);

                source = new LatLng(model.getSource().getLocation().getLat(), model.getSource().getLocation().getLng());
                sourceAddress = model.getSource().getAddress();

                if (model.getVehicalType().getType().compareTo("Vehicle") == 0) {
                    llVehicle.setVisibility(View.VISIBLE);
                    llMach.setVisibility(View.GONE);
                    destination = new LatLng(model.getDestination().getLocation().getLat(), model.getDestination().getLocation().getLng());
                    destinationAddress = model.getDestination().getAddress();

                    setMap();
                    setDetail();
                } else {
                    llVehicle.setVisibility(View.GONE);
                    llMach.setVisibility(View.VISIBLE);

                    if (model.getStatus().compareTo("requested") == 0) {
                        btnCancel.setVisibility(View.GONE);
                    } else {
                        btnCancel.setVisibility(View.VISIBLE);
                    }
                    txtStatus.setText(model.getStatus());
                    txtTitleMac.setText(model.getTitle());
                    txtMachinaryType.setText(model.getTypeOfVehicle());
                    txtBudgetMac.setText("KSH " + model.getNetPrice());
                    txtJobLoc.setText(model.getSource().getAddress());
                    txtStartDate.setText(MyApplication.utcToDate(model.getDate()) + "," + MyApplication.utcToTime(model.getDate()));
                    txtEndDate.setText(MyApplication.utcToDate(model.getEndDate()) + "," + MyApplication.utcToTime(model.getEndDate()));
                    txtDiscription.setText(model.getDescription());
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        //mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 16));

        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }
        MarkerOptions options = new MarkerOptions();
        // Adding new item to the ArrayList
        markerPoints.add(source);
        options.position(source);

        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));


        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
        if (model.getRequiredType().equalsIgnoreCase("vehicle")) {
            markerPoints.add(destination);
            options.position(destination);

            mMap.addMarker(new MarkerOptions().position(destination)
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                    .title(destinationAddress));
        }
        // Checks, whether start and end locations are captured
    }

    private void setMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    private void setDetail() {
        if (model.getDestination() == null) {
            txtBudget.setText(model.getBudget().toString() + " KSH");
            txtDate.setText(MyApplication.utcToDate(model.getDate()));
            txtDescription.setText(model.getDescription());
            txtSource.setText(model.getSource().getAddress());
            txtPrice.setText(model.getNetPrice().toString() + " KSH");
            txtTitle.setText(model.getTitle());
            txtVehicle.setText(model.getTypeOfVehicle());
        } else {
            if (model.getBudget() != null) {
                txtBudget.setText(model.getBudget().toString() + " KSH");
            }
            txtDate.setText(MyApplication.utcToDate(model.getDate()));
            txtDescription.setText(model.getDescription());
            txtDestination.setText(model.getDestination().getAddress());
            txtSource.setText(model.getSource().getAddress());
            txtPrice.setText(model.getNetPrice().toString() + " KSH");
            txtTitle.setText(model.getTitle());
            txtWeight.setText(model.getWeight() + " KG");

            if (Long.parseLong(model.getDistance().getValue()) < 1000) {
                txtDistance.setText(model.getDistance().getValue() + " Meters");
            } else {
                txtDistance.setText(Long.parseLong(model.getDistance().getValue()) / 1000 + " KM");
            }

            txtVehicle.setText(model.getTypeOfVehicle());

            getRoute();
        }
    }

    private void getRoute() {
        String url = MyApplication.getDirectionsUrl(source, destination);

        ApiService.getInstance(NotificationJobActivity.this).makeGetCall(NotificationJobActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ParserTask parserTask = new ParserTask(NotificationJobActivity.this);
                parserTask.execute(response.toString());
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> poly = new ArrayList();
        int index = 0, len = encoded.length();
        int lat = 0, lng = 0;

        while (index < len) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            poly.add(p);
        }

        return poly;
    }


    private void showRoute(List<HashMap<String, String>> data) {

        if (polyline != null) {
            polyline.remove();
        }

        ArrayList points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        points = new ArrayList();
        lineOptions = new PolylineOptions();

        for (int j = 0; j < data.size(); j++) {
            HashMap<String, String> point = data.get(j);

            double lat = Double.parseDouble(point.get("lat").toString());
            double lng = Double.parseDouble(point.get("lng").toString());

            LatLng position = new LatLng(lat, lng);

            points.add(position);
//            builder.include(position);
        }

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(getResources().getColor(R.color.orange_dark));
        lineOptions.geodesic(true);
// Drawing polyline in the Google Map for the i-th route
        polyline = mMap.addPolyline(lineOptions);
//        LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);

        if (!TextUtils.isEmpty(model.getDriverId())) {
//            callAsynchronousTask();
        }

//        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
//        mMap.animateCamera(cu);
    }

    private void acceptJob() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(NotificationJobActivity.this).makePostCallAuth(NotificationJobActivity.this, ApiService.JOB_ACCEPT, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {

                swipeAccept.showResultIcon(true);
                MyApplication.activityStart(NotificationJobActivity.this, DriverHomeActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

                swipeAccept.showResultIcon(false);
            }
        });
    }


    @Override
    public void onSwipeConfirm() {
        acceptJob();
    }

    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser) {
        for (int i = 0; i < parser.getRouteList().size(); i++) {
            if (parser.getRouteList().get(i).getSelectedRoute().equalsIgnoreCase(model.getSelectedRoute())) {
                showRoute(result.get(i));
                break;
            }
        }

    }

    private void acceptJob(String jobId) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(NotificationJobActivity.this).makePostCallAuth(NotificationJobActivity.this, ApiService.JOB_ACCEPT, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.activityStart(NotificationJobActivity.this, DriverHomeActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void jobCancel() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(NotificationJobActivity.this).makePostCallAuth(NotificationJobActivity.this, ApiService.CANCEL_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(NotificationJobActivity.this, "Job Cancelled!!!");
                MyApplication.activityStart(NotificationJobActivity.this, DriverHomeActivity.class, true);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
