package bebamazigo.com.bebamazigo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WithdrawWalletMoneyActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.et_mobile_number)
    EditText etMobileNumber;
    @BindView(R.id.et_amount)
    EditText etAmount;
    @BindView(R.id.tv_procees)
    TextView tvProcees;
    @BindView(R.id.tv_cancel)
    TextView tvCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_wallet_money);
        ButterKnife.bind(this, this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Withdraw Money");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private boolean valid() {
        if (etMobileNumber.getText().toString().isEmpty()) {
            etMobileNumber.setError("Enter Number");
            return false;
        } else if (etAmount.getText().toString().isEmpty()) {
            etAmount.setError("Enter Amount");
            return false;
        } else {
            return true;
        }
    }

    private void requestWithdrawMoney() {
        JSONObject param = new JSONObject();
        try {
            param.put("mobileNumber", etMobileNumber.getText());
            param.put("amount", Integer.parseInt(etAmount.getText().toString()));

            Log.d("Param's : ", "mobileNumber" + " : " + etMobileNumber.getText().toString() + " " + "amount" + " : " + Integer.parseInt(etAmount.getText().toString()));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(WithdrawWalletMoneyActivity.this).makePostCallAuth(WithdrawWalletMoneyActivity.this, ApiService.Withdraw_Money,
                param, new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("Response", response.toString());

                        if (response.optJSONObject("success") != null) {

                            JSONObject data = response.optJSONObject("success").optJSONObject("data");

                            if (data.optString("paymentStatus").equalsIgnoreCase("pending")) {
                                if (data.optString("id") != null) {
                                    Intent intent = new Intent(WithdrawWalletMoneyActivity.this, WithdrawWalletMoneyPayment.class);
                                    intent.putExtra("trans_id", data.optString("id"));
                                    startActivity(intent);
                                    finish();
                                } else {
                                    MyApplication.showToast(WithdrawWalletMoneyActivity.this, "Something Went Wrong");
                                }
                            } else {
                                MyApplication.showToast(WithdrawWalletMoneyActivity.this, "Something Went Wrong");
                            }
                        } else {
                            MyApplication.showToast(WithdrawWalletMoneyActivity.this, "Something Went Wrong");
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        MyApplication.showToast(WithdrawWalletMoneyActivity.this, "Something went wrong.");
                    }

                });
    }

    @OnClick({R.id.tv_procees, R.id.tv_cancel})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_procees:
                if (valid())
                    requestWithdrawMoney();
                break;
            case R.id.tv_cancel:
                finish();
                break;
        }
    }
}
