package bebamazigo.com.bebamazigo.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Data.DataPart;
import bebamazigo.com.bebamazigo.Model.VehicleType;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.MarshMallowPermission;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddVehicleMachineryActivity extends AppCompatActivity {

    public static final int PICK_LOG = 1;
    public static final int PICK_INSURANCE = 2;
    public static final int PICK_RC = 3;
    @BindView(R.id.llMain)
    LinearLayout llMain;
    @BindView(R.id.scrollViewMain)
    ScrollView scrollViewMain;
    @BindView(R.id.llChildMain)
    LinearLayout llChildMain;
    @BindView(R.id.spn_register_type)
    Spinner spnRegisterType;
    @BindView(R.id.edt_vehicle_no)
    EditText edtVehicleNo;
    @BindView(R.id.edt_reg_no)
    EditText edtRegNo;
    @BindView(R.id.spn_vehicle_type)
    Spinner spnVehicleType;
    @BindView(R.id.spn_weight)
    Spinner spnWeight;
    @BindView(R.id.edt_make_no)
    TextView edtMakeNo;
    @BindView(R.id.edt_model_no)
    EditText edtModelNo;
    @BindView(R.id.img_log)
    ImageView imgLog;
    @BindView(R.id.ll_log)
    LinearLayout llLog;
    @BindView(R.id.img_insurance)
    ImageView imgInsurance;
    @BindView(R.id.ll_insurance)
    LinearLayout llInsurance;
    @BindView(R.id.ll_image)
    LinearLayout llImage;
    @BindView(R.id.txt_submit)
    TextView txtSubmit;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    @BindView(R.id.ll_button)
    LinearLayout llButton;
    @BindView(R.id.spn_trailer_type)
    Spinner spnTrailerType;
    Gson gson;
    @BindView(R.id.ll_vehicle)
    LinearLayout llVehicle;
    @BindView(R.id.ll_machinery)
    LinearLayout llMachinery;

    Bitmap bmpLog, bmpInsurance, bmpRc;
    @BindView(R.id.img_rc)
    ImageView imgRc;
    @BindView(R.id.ll_rc)
    LinearLayout llRc;
    String type, vehicleNumber, registrationNumber, tonnage, bodyType, vehicleType, machinery, modelNumber, makeNumber;
    ArrayList<VehicleType> vehiclesList = new ArrayList<>();
    ArrayList<VehicleType> machineryList = new ArrayList<>();
    @BindView(R.id.spn_machinery_type)
    Spinner spnMachineryType;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    boolean isAdd = true;

    String date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle_machinery);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Add Vehicles/Machinery");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        gson = new Gson();

        spnRegisterType.setAdapter(new HintAdapter(AddVehicleMachineryActivity.this, R.layout.item_spinner, getResources().getStringArray(R.array.register_truck)));
        getVehicle();

        spnRegisterType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnVehicleType.setVisibility(View.GONE);
                    edtModelNo.setVisibility(View.GONE);
                    edtMakeNo.setVisibility(View.GONE);
                    spnMachineryType.setVisibility(View.GONE);
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);

                    findViewById(android.R.id.content).invalidate();

                } else if (position == 1) {
                    spnMachineryType.setVisibility(View.GONE);
                    spnVehicleType.setVisibility(View.VISIBLE);
                    spnVehicleType.setAdapter(new HintAdapter(AddVehicleMachineryActivity.this, R.layout.item_spinner, getVehicleList()));

                    findViewById(android.R.id.content).invalidate();

                } else {
                    spnMachineryType.setVisibility(View.VISIBLE);
                    spnVehicleType.setVisibility(View.GONE);
                    spnMachineryType.setAdapter(new HintAdapter(AddVehicleMachineryActivity.this, R.layout.item_spinner, getMachineryList()));

                    findViewById(android.R.id.content).invalidate();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnVehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    changeUI(position - 1, 1);
                else {
                    edtModelNo.setVisibility(View.GONE);
                    edtMakeNo.setVisibility(View.GONE);
                    spnMachineryType.setVisibility(View.GONE);
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnMachineryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    changeUI(position - 1, 2);
                else {
                    spnVehicleType.setVisibility(View.GONE);
                    edtModelNo.setVisibility(View.GONE);
                    edtMakeNo.setVisibility(View.GONE);
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        edtMakeNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDateTime();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(AddVehicleMachineryActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectDateTime() {
        SwitchDateTimeDialogFragment dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                "Title example",
                "OK",
                "Cancel"
        );

        // Assign values

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        dateTimeFragment.startAtCalendarView();
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setMaximumDateTime(cal.getTime());
        dateTimeFragment.setDefaultDateTime(Calendar.getInstance().getTime());
        // Or assign each element, default element is the current moment
        // dateTimeFragment.setDefaultHourOfDay(15);
        // dateTimeFragment.setDefaultMinute(20);
        // dateTimeFragment.setDefaultDay(4);
        // dateTimeFragment.setDefaultMonth(Calendar.MARCH);
        // dateTimeFragment.setDefaultYear(2017);

        // Define new day and month format

        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateTimeFragment.setSimpleDateMonthAndDayFormat(format);
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {

        }

        // Set listener
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date selectedDate) {
                long millis = selectedDate.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                date = sdf.format(new Date(millis));
                edtMakeNo.setTextColor(Color.BLACK);
                edtMakeNo.setText(MyApplication.utcToDate(date));

            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

        // Show
        dateTimeFragment.show(getSupportFragmentManager(), "dialog_time");
    }

    private void changeUI(int position, int type) {
        VehicleType vehicleType;
        if (type == 1)
            vehicleType = vehiclesList.get(position);
        else
            vehicleType = machineryList.get(position);

        if (vehicleType.getBodyType() != null) {
            spnTrailerType.setVisibility(View.VISIBLE);
            spnTrailerType.setAdapter(new HintAdapter(AddVehicleMachineryActivity.this, R.layout.item_spinner, vehiclesList.get(position).getBodyType()));
        } else {
            spnTrailerType.setVisibility(View.GONE);
        }

        if (vehicleType.getTon() != null) {
            spnWeight.setVisibility(View.VISIBLE);
            spnWeight.setAdapter(new HintAdapter(AddVehicleMachineryActivity.this, R.layout.item_spinner, (vehicleType.getTon())));
        } else {
            spnWeight.setVisibility(View.GONE);
        }

        if (vehicleType.isMake()) {
            edtMakeNo.setVisibility(View.VISIBLE);
        } else {
            edtMakeNo.setVisibility(View.GONE);
        }
        if (vehicleType.isModel()) {
            edtModelNo.setVisibility(View.VISIBLE);
        } else {
            edtModelNo.setVisibility(View.GONE);
        }

    }


    private String[] getVehicleList() {
        List<String> vehicle = new ArrayList();
        vehicle.add("Vehicle");

        for (int i = 0; i < vehiclesList.size(); i++) {
            vehicle.add(vehiclesList.get(i).getName());
        }
        return vehicle.toArray(new String[vehicle.size()]);
    }

    private String[] getMachineryList() {
        List<String> vehicle = new ArrayList();
        vehicle.add("Machinery");
        for (int i = 0; i < machineryList.size(); i++) {
            vehicle.add(machineryList.get(i).getName());
        }

        return vehicle.toArray(new String[vehicle.size()]);
    }

    private void getVehicle() {
        ApiService.getInstance(AddVehicleMachineryActivity.this).makeGetCall(AddVehicleMachineryActivity.this, ApiService.GET_VEHICLE_TYPE + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN)
                , new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            JSONArray vehicle = response.optJSONObject("success").optJSONObject("data").optJSONArray("vehicle");
                            JSONArray machinery = response.optJSONObject("success").optJSONObject("data").optJSONArray("machinery");
                            for (int i = 0; i < vehicle.length(); i++) {
                                vehiclesList.add(new VehicleType(vehicle.optJSONObject(i)));
                            }
                            for (int i = 0; i < machinery.length(); i++) {
                                machineryList.add(new VehicleType(machinery.optJSONObject(i)));
                            }
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private void addTruck() {
        Map<String, DataPart> params = new HashMap<>();
        JSONObject data = new JSONObject();
        try {
            data.put("type", vehicleType);
            data.put("vehicleNumber", vehicleNumber);
            data.put("registrationNumber", registrationNumber);
            data.put("tonnage", tonnage);
            data.put("bodyType", bodyType);
            data.put("machinery", machinery);
            if (spnRegisterType.getSelectedItem().toString().trim().compareTo("Vehicle") == 0) {
                data.put("vehicleMake", new Date() + "");
            } else {
                data.put("vehicleMake", date);
            }

            data.put("vehicleModel", modelNumber);
            data.put("userId", MyApplication.getSharedPrefString(StaticData.SP_USER_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        params.put("data", new DataPart("", data.toString().getBytes(), "text"));

        if (bmpLog != null) {
            params.put("logBook", new DataPart(edtVehicleNo.getText() + "_Log", MyApplication.getBytesFromBitmap(bmpLog), "image/jpeg"));
        }
        if (bmpInsurance != null) {
            params.put("insuranceCopy", new DataPart(edtVehicleNo.getText() + "_Insurance", MyApplication.getBytesFromBitmap(bmpInsurance), "image/jpeg"));
        }
        if (bmpRc != null) {
            params.put("ntsaIns", new DataPart(edtVehicleNo.getText() + "_RC", MyApplication.getBytesFromBitmap(bmpRc), "image/jpeg"));
        }

        ApiService.getInstance(AddVehicleMachineryActivity.this).multipartCall(AddVehicleMachineryActivity.this, ApiService.ADD_VEHICLE,
                params, new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            String msg = response.optJSONObject("success").optJSONObject("msg").optString("replyMessage");
                            MyApplication.showToast(AddVehicleMachineryActivity.this, msg);
                            MyApplication.activityFinish(AddVehicleMachineryActivity.this);
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    @OnClick({R.id.ll_log, R.id.ll_insurance, R.id.txt_submit, R.id.txt_cancel, R.id.ll_rc})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_log:
                openGallery(PICK_LOG);
                break;
            case R.id.ll_insurance:
                openGallery(PICK_INSURANCE);
                break;
            case R.id.txt_submit:
                if (isValid()) {
                    addTruck();
                }
                break;
            case R.id.ll_rc:
                openGallery(PICK_RC);
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(AddVehicleMachineryActivity.this);
                break;
        }
    }

    private void openGallery(int PICK_IMAGE) {
        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(AddVehicleMachineryActivity.this);
        if (marshMallowPermission.checkPermissionForREAD_EXTERNAL_STORAGE()) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        } else {
            marshMallowPermission.requestPermissionForREAD_EXTERNAL_STORAGE();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data.getData() != null) {
            Uri selectedImage = data.getData();
            try {
                if (requestCode == PICK_LOG) {
                    bmpLog = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgLog.setImageBitmap(bmpLog);
                } else if (requestCode == PICK_INSURANCE) {
                    bmpInsurance = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgInsurance.setImageBitmap(bmpInsurance);
                } else if (requestCode == PICK_RC) {
                    bmpRc = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgRc.setImageBitmap(bmpRc);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(edtVehicleNo.getText())) {
            edtVehicleNo.setError(StaticData.BLANK);
            return false;
        } else {
            vehicleNumber = edtVehicleNo.getText().toString();
        }

        if (TextUtils.isEmpty(edtRegNo.getText())) {
            edtRegNo.setError(StaticData.BLANK);
            return false;
        } else {
            registrationNumber = edtRegNo.getText().toString();
        }


        if (spnRegisterType.getSelectedItemPosition() == 0) {
            MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Register Type");
            return false;
        } else if (spnRegisterType.getSelectedItemPosition() == 1) {
            if (spnVehicleType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Vehicle Type");
                return false;
            } else {
                vehicleType = spnVehicleType.getSelectedItem().toString();
            }
        } else if (spnRegisterType.getSelectedItemPosition() == 2) {
            if (spnMachineryType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Machinery Type");
                return false;
            } else {
                vehicleType = spnMachineryType.getSelectedItem().toString();
            }
        }
        if (!check()) {
            return false;
        }

        if (bmpLog == null) {
            MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Copy of Log");
            return false;
        }
        if (bmpInsurance == null) {
            MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Copy of Insurance");
            return false;
        }
        if (bmpRc == null) {
            MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Copy of RC");
            return false;
        }

        return true;
    }

    private boolean check() {
        if (spnTrailerType.getVisibility() == View.VISIBLE) {
            if (spnTrailerType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Body Type");
                return false;
            } else {
                bodyType = spnTrailerType.getSelectedItem().toString();
            }
        }

        if (spnWeight.getVisibility() == View.VISIBLE) {
            if (spnWeight.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddVehicleMachineryActivity.this, "Please Select Tonnage");
                return false;
            } else {
                tonnage = spnWeight.getSelectedItem().toString();
            }
        }

        if (edtMakeNo.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(edtMakeNo.getText())) {
                edtMakeNo.setError(StaticData.BLANK);
                return false;
            } else {
                makeNumber = edtMakeNo.getText().toString();
            }
        }
        if (edtModelNo.getVisibility() == View.VISIBLE) {
            if (TextUtils.isEmpty(edtModelNo.getText())) {
                edtModelNo.setError(StaticData.BLANK);
                return false;
            } else {
                modelNumber = edtModelNo.getText().toString();
            }
        }

        return true;
    }
}
