package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileActivity extends AppCompatActivity {

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_company)
    EditText edtCompany;
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.txt_submit)
    TextView txtSubmit;

    String name, email, mobile, companyName, userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setUpUi();


    }

    private void setUpUi() {
        MyApplication.hideSoftKeyboard(EditProfileActivity.this, edtName);
        userType = MyApplication.getSharedPrefString(StaticData.SP_REALM);
        name = MyApplication.getSharedPrefString(StaticData.SP_USER_NAME);
        email = MyApplication.getSharedPrefString(StaticData.SP_EMAIL);
        mobile = MyApplication.getSharedPrefString(StaticData.SP_MOBILE);


        if (!TextUtils.isEmpty(name)) {
            edtName.setText(name);
        }
        if (!TextUtils.isEmpty(email)) {
            edtEmail.setText(email);
        }
        if (!TextUtils.isEmpty(mobile)) {
            edtMobile.setText(mobile);
        }

        if (userType.equalsIgnoreCase("owner")) {
            edtCompany.setVisibility(View.VISIBLE);
            companyName = MyApplication.getSharedPrefString(StaticData.SP_COMPANY_NAME);
            if (!TextUtils.isEmpty(companyName)) {
                edtCompany.setText(companyName);
            }
        } else {
            edtCompany.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.txt_submit)
    public void onClick() {
        if (isValid()) {
            editProfile();
        }
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(edtName.getText())) {
            edtName.setError(StaticData.BLANK);
            return false;
        }
        if (userType.equalsIgnoreCase("owner")) {
            if (TextUtils.isEmpty(edtCompany.getText())) {
                edtCompany.setError(StaticData.BLANK);
                return false;
            }
        }
        if (TextUtils.isEmpty(edtMobile.getText())) {
            edtMobile.setError(StaticData.BLANK);
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText())) {
            edtEmail.setError(StaticData.BLANK);
            return false;
        }

        /*if (edtMobile.length() != 10) {
            edtMobile.setError("Invalid mobile no.");
            return false;
        }*/

        if (!MyApplication.isEmailValid(edtEmail.getText().toString())) {
            edtEmail.setError("Invalid Email address");
            return false;
        }
        return true;
    }

    private void editProfile() {
        JSONObject data = new JSONObject();
        try {
            data.put("realm", MyApplication.getSharedPrefString(StaticData.SP_REALM));
            data.put("name", edtName.getText().toString());
            data.put("email", edtEmail.getText().toString());
            data.put("mobile", edtMobile.getText().toString());
            if (userType.equalsIgnoreCase("owner")) {
                data.put("companyName", edtCompany.getText().toString());
            }
            data.put("mobile", edtMobile.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(EditProfileActivity.this).makePostCallAuth(EditProfileActivity.this, ApiService.EDIT_PROFILE,
                data,
                new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        JSONObject data = response.optJSONObject("success").optJSONObject("data");

                        String name, email, mobile, companyName, realm, id, userId, image;
                        userId = data.optString("userId");
                        id = data.optString("id");
                        realm = data.optString("realm");
                        name = data.optString("name");
                        email = data.optString("userEmail");
                        mobile = data.optString("userMobile");
                        companyName = data.optString("companyName");
                        image = data.optString("profileImage");


                        MyApplication.setSharedPrefString(StaticData.SP_USER_ID, userId);
                        MyApplication.setSharedPrefString(StaticData.SP_ID, id);
                        MyApplication.setSharedPrefString(StaticData.SP_USER_NAME, name);
                        MyApplication.setSharedPrefString(StaticData.SP_USER_IMAGE, ApiService.BASE_URL_IMAGE + image);
                        MyApplication.setSharedPrefString(StaticData.SP_EMAIL, email);
                        MyApplication.setSharedPrefString(StaticData.SP_MOBILE, mobile);
                        MyApplication.setSharedPrefString(StaticData.SP_REALM, realm);
                        MyApplication.setSharedPrefString(StaticData.SP_COMPANY_NAME, companyName);


                        if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equalsIgnoreCase("owner")) {
                            MyApplication.activityStart(EditProfileActivity.this, OwnerHomeActivity.class, true);
                        } else if (MyApplication.getSharedPrefString(StaticData.SP_REALM).equalsIgnoreCase("customer")) {
                            MyApplication.activityStart(EditProfileActivity.this, CustomerHomeActivity.class, true);
                        } else
                            MyApplication.activityStart(EditProfileActivity.this, DriverHomeActivity.class, true);

                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });

    }
}
