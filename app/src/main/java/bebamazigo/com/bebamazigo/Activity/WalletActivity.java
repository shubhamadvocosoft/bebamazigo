package bebamazigo.com.bebamazigo.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.WalletAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TransactionModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalletActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_add_money)
    TextView txtAddMoney;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    WalletAdapter walletAdapter;
    @BindView(R.id.txt_all)
    TextView txtAll;
    @BindView(R.id.txt_paid)
    TextView txtPaid;
    @BindView(R.id.txt_recieved)
    TextView txtRecieved;
    @BindView(R.id.txt_balance)
    TextView txtBalance;
    @BindView(R.id.tv_withdraw_wallet_balance)
    TextView tvWithdrawWalletBalance;
    Gson gson = new Gson();
    ArrayList<TransactionModel> jobList = new ArrayList<>();


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(WalletActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        getWallet();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getWallet();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Wallet");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        walletAdapter = new WalletAdapter(jobList, WalletActivity.this);
        recycler.setLayoutManager(new GridLayoutManager(WalletActivity.this, 1, GridLayoutManager.VERTICAL, false));
        recycler.setAdapter(walletAdapter);

        getTransaction("");


    }


    @OnClick({R.id.txt_all, R.id.txt_paid, R.id.txt_recieved, R.id.txt_add_money, R.id.tv_withdraw_wallet_balance})
    public void onClick(View view) {
        if (view.getId() == txtAddMoney.getId()) {
            MyApplication.activityStart(WalletActivity.this, AddMoneyActivity.class, false);
        } else if (view.getId() == R.id.txt_all) {
            filterList((TextView) view);
            getTransaction("");
        } else if (view.getId() == R.id.txt_paid) {
            filterList((TextView) view);
            getTransaction("send");
        } else if (view.getId() == R.id.txt_recieved) {
            filterList((TextView) view);
            getTransaction("received");
        } else if (view.getId() == R.id.tv_withdraw_wallet_balance) {
            startActivity(new Intent(WalletActivity.this, WithdrawWalletMoneyActivity.class));
        } else {
            filterList((TextView) view);
        }
    }

    private void filterList(TextView view) {
        txtAll.setBackgroundColor(getResources().getColor(R.color.white));
        txtAll.setTextColor(getResources().getColor(R.color.dark_grey));

        txtPaid.setBackgroundColor(getResources().getColor(R.color.white));
        txtPaid.setTextColor(getResources().getColor(R.color.dark_grey));

        txtRecieved.setBackgroundColor(getResources().getColor(R.color.white));
        txtRecieved.setTextColor(getResources().getColor(R.color.dark_grey));

        view.setBackgroundColor(getResources().getColor(R.color.orange));
        view.setTextColor(getResources().getColor(R.color.white));
    }

    private void getWallet() {
        ApiService.getInstance(WalletActivity.this).makeGetCall(WalletActivity.this, ApiService.GET_WALLET +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                if (response.optJSONObject("success") != null) {
                    JSONObject data = response.optJSONObject("success").optJSONObject("data");
                    txtBalance.setText(data.optString("walletAmount"));
                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void getTransaction(String type) {
        Log.d("auth", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
        ApiService.getInstance(WalletActivity.this).makeGetCall(WalletActivity.this, ApiService.GET_Transaction +
                "?type=" + type + "&access_token=" + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                if (response.optJSONObject("success") != null) {
                    JSONArray data = response.optJSONObject("success").optJSONArray("data");
                    if (data.length() == 0) {
                        jobList.clear();
                    }
                    jobList.clear();
                    for (int i = 0; i < data.length(); i++) {
                        TransactionModel model = null;
                        try {
                            model = gson.fromJson(data.getJSONObject(i).toString(), TransactionModel.class);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jobList.add(model);
                    }

                    walletAdapter.setData(jobList);

                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
