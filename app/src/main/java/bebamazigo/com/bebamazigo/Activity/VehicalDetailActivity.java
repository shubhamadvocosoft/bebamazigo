package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicalDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_vehicle_no)
    TextView txtVehicleNo;
    @BindView(R.id.txt_reg_no)
    TextView txtRegNo;
    @BindView(R.id.txt_category)
    TextView txtCategory;
    @BindView(R.id.txt_type)
    TextView txtType;
    @BindView(R.id.txt_model)
    TextView txtModel;
    @BindView(R.id.txt_manufacture)
    TextView txtManufacture;
    @BindView(R.id.txt_assment)
    TextView txtAssment;
    @BindView(R.id.img_log_book)
    ImageView imgLogBook;
    @BindView(R.id.img_inst)
    ImageView imgInst;
    @BindView(R.id.img_insurence)
    ImageView imgInsurence;
    @BindView(R.id.btn_assesment)
    Button btnAssesment;

    Gson gson;
    String jobId;
    Vehicle model = null;
    @BindView(R.id.ll_manufacture)
    LinearLayout llManufacture;
    @BindView(R.id.ll_assesment)
    LinearLayout llAssesment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehical_detail);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Vehicle");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Vehicle");
        gson = new Gson();
        jobId = getIntent().getBundleExtra("bundle").getString("jobId");
        if (!TextUtils.isEmpty(jobId))
            getVehicalDetail();

        if (getIntent().getBundleExtra("bundle").getString("activity").compareTo("owner")==0){
            btnAssesment.setVisibility(View.VISIBLE);
        }else {
            btnAssesment.setVisibility(View.GONE);
        }

        btnAssesment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model.getAssessmentEnd() == null) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("data", model);
                    MyApplication.activityStart(VehicalDetailActivity.this, VehicalAssismentActivity.class, false, bundle);
                } else {
                    try {
                        Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(MyApplication.utcToDate(model.getAssessmentEnd()));
                        if (new Date().before(date1)) {
                            Toast.makeText(VehicalDetailActivity.this, "vehical already have assesment", Toast.LENGTH_SHORT).show();
                        } else {
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("data", model);
                            MyApplication.activityStart(VehicalDetailActivity.this, VehicalAssismentActivity.class, false, bundle);
                        }
                    } catch (Exception e) {

                    }

                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (getIntent().getBundleExtra("bundle").getString("activity").compareTo("owner")==0){
            inflater.inflate(R.menu.hire_driver, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(VehicalDetailActivity.this);
        } else if (id == R.id.hire_driver) {
            if (model.getIsDriverAlloted()){
                Bundle bundle = new Bundle();
                bundle.putString("id", model.getDriverId());
                MyApplication.activityStart(VehicalDetailActivity.this, DriverDetailsNew.class, false, bundle);
            }else {
                Bundle bundle = new Bundle();
                bundle.putSerializable("data", model);
                MyApplication.activityStart(VehicalDetailActivity.this, HireDriverAssesmentActivity.class, false, bundle);
            }


        }
        return super.onOptionsItemSelected(item);
    }

    private void getVehicalDetail() {
        String url = ApiService.Get_VehicalById + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&vehicleId=" + jobId;
        ApiService.getInstance(VehicalDetailActivity.this).makeGetCall(VehicalDetailActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("responseVehicleDetail", response.toString());
                model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), Vehicle.class);

                setDetail();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setDetail() {
        try {
            txtVehicleNo.setText(model.getVehicleNumber());
            txtRegNo.setText(model.getRegistrationNumber());
            txtType.setText(model.getType());
            txtCategory.setText(model.getVehicleType().getType());
            txtModel.setText(model.getVehicleModel());
            txtManufacture.setText(MyApplication.utcToDate(model.getVehicleMake()));

            if (model.getVehicleType().getType().compareTo("Vehicle")==0){
                 llManufacture.setVisibility(View.GONE);
            }

            if (model.getAssessmentEnd()==null){
                llAssesment.setVisibility(View.GONE);
            }else {
                txtAssment.setText(MyApplication.utcToDate(model.getAssessmentEnd()));
            }


            if (model.getAssessmentEnd() == null) {
                btnAssesment.setText("Apply for Assesment");
                llAssesment.setVisibility(View.GONE);
            } else {
                Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse(MyApplication.utcToDate(model.getAssessmentEnd()));
                if (new Date().before(date1)) {
                    btnAssesment.setText("Vehicle Assesment");
                } else {
                    btnAssesment.setText("Apply for Assesment");
                }
            }

            if (model.getLogBook().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getLogBook().get(0)).into(imgLogBook);
            if (model.getNtsaIns().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getInsuranceCopy().get(0)).into(imgInst);
            if (model.getInsuranceCopy().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getNtsaIns().get(0)).into(imgInsurence);
        } catch (Exception e) {
            Log.d("exception...", e.toString());
        }


    }

    @Override
    protected void onRestart() {
        super.onRestart();
        getVehicalDetail();
    }
}
