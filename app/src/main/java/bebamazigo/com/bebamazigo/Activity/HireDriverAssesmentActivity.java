package bebamazigo.com.bebamazigo.Activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import bebamazigo.com.bebamazigo.Adapter.DriverAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.DriverModel;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.Service.LocationTracker;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HireDriverAssesmentActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll)
    LinearLayout ll;
    @BindView(R.id.recycler)
    RecyclerView recycler;
    @BindView(R.id.spinner_vehicle)
    Spinner spinnerVehicle;
    DriverAdapter driverAdapter;
    ArrayList<DriverModel> driverList = new ArrayList<>();
    ArrayList<Vehicle> vehicleList = new ArrayList<>();
    Gson gson = new Gson();

    String vehicleleId = "";
    String vehicleId = "";

    boolean isLocationAvailable = false;
    Vehicle model_v;

    BroadcastReceiver locationUpdated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Location newLocation = intent.getParcelableExtra("location");
            MyApplication.setDouble(StaticData.SP_NEW_LAT, (newLocation.getLatitude()));
            MyApplication.setDouble(StaticData.SP_NEW_LONG, (newLocation.getLongitude()));

            isLocationAvailable = true;

            startService(new Intent(HireDriverAssesmentActivity.this, LocationTracker.class).putExtra("finish", true));

            getNearByDriver();

            recycler.setLayoutManager(new GridLayoutManager(HireDriverAssesmentActivity.this, 1, GridLayoutManager.VERTICAL, false));
            driverAdapter = new DriverAdapter(driverList, model_v.getId(), HireDriverAssesmentActivity.this);
            recycler.setAdapter(driverAdapter);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hire_driver);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Hire Driver");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(new Intent(HireDriverAssesmentActivity.this, LocationTracker.class));
        } else {
            startService(new Intent(HireDriverAssesmentActivity.this, LocationTracker.class));
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        model_v = (Vehicle) getIntent().getBundleExtra("bundle").getSerializable("data");

        /* vehicleList = (ArrayList<Vehicle>) getIntent().getBundleExtra("bundle").getSerializable("list");

        spinnerVehicle.setAdapter(new HintAdapter(HireDriverAssesmentActivity.this, 0, getVehicleArray()));

        spinnerVehicle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position != 0) {

                    vehicleId = vehicleList.get(position - 1).getId();
                    driverAdapter.setVehicleId(vehicleId);
                    getNearByDriver();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

        spinnerVehicle.setVisibility(View.GONE);
        ll.setVisibility(View.GONE);

       /* getNearByDriver();

        recycler.setLayoutManager(new GridLayoutManager(HireDriverAssesmentActivity.this, 1, GridLayoutManager.VERTICAL, false));
        driverAdapter = new DriverAdapter(driverList, model_v.getId(), HireDriverAssesmentActivity.this);
        recycler.setAdapter(driverAdapter);*/


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(HireDriverAssesmentActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private String[] getVehicleArray() {
        List<String> vehicles = new ArrayList<>();
        vehicles.add("Select Vehicle");
        for (int i = 0; i < vehicleList.size(); i++) {
            vehicles.add(vehicleList.get(i).getType());
        }
        return vehicles.toArray(new String[vehicles.size()]);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(HireDriverAssesmentActivity.this).
                registerReceiver(locationUpdated, new IntentFilter("locationUpdated"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(HireDriverAssesmentActivity.this).unregisterReceiver(locationUpdated);
    }

    private void getNearByDriver() {
        if (isLocationAvailable) {
            JSONObject position = new JSONObject();
            try {
                position.put("lat", MyApplication.getDouble(StaticData.SP_NEW_LAT));
                position.put("lng", MyApplication.getDouble(StaticData.SP_NEW_LONG));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            String uri = Uri.parse(ApiService.GET_NEAR_DRIVER + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN))
                    .buildUpon()
                    .appendQueryParameter("position", position.toString())
                    .appendQueryParameter("vehicleId", model_v.getId())
                    .appendQueryParameter("vehicleType", model_v.getType())
                    .build().toString();

            ApiService.getInstance(this).makeGetCall(this, uri, new ApiService.OnResponse() {
                @Override
                public void onResponseSuccess(JSONObject response) {
                    if (response.optJSONObject("success") != null) {
                        Log.d("response", response.toString());
                        driverList = new ArrayList<DriverModel>();
                        JSONArray data = response.optJSONObject("success").optJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            DriverModel model = null;
                            try {
                                model = gson.fromJson(data.getJSONObject(i).toString(), DriverModel.class);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            driverList.add(model);
                        }
                        driverAdapter.setData(driverList);
                        driverAdapter.notifyDataSetChanged();
                    }
                }

                @Override
                public void onError(VolleyError volleyError) {

                }
            });
        } else {
            MyApplication.showToast(HireDriverAssesmentActivity.this, "Location not available. Getting Location");
        }
    }
}
