package bebamazigo.com.bebamazigo.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bebamazigo.com.bebamazigo.Adapter.VehicleCheckBoxAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Data.DataPart;
import bebamazigo.com.bebamazigo.Model.VehicleType;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.MarshMallowPermission;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class RegisterActivity extends AppCompatActivity {

    public static final int PICK_LICENSE = 1;
    public static final int PICK_GOVT_ID = 2;
    public static final int PICK_PROFILE = 3;
    public static final int PICK_LOG_BOOK = 4;
    public static final int PICK_PIN = 5;
    public static final int PICK_CERTIFICATE = 6;
    public static final int PICK_PROFILE_PIC = 7;

    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.edt_cnfrm_password)
    EditText edtCnfrmPassword;
    @BindView(R.id.edt_company)
    EditText edtCompany;
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.txt_submit)
    TextView txtSubmit;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    @BindView(R.id.ll_button)
    LinearLayout llButton;
    @BindView(R.id.ll_license)
    LinearLayout llLicense;
    @BindView(R.id.ll_govt_id)
    LinearLayout llGovtId;
    @BindView(R.id.txt_)
    TextView txt1;
    @BindView(R.id.ll_image)
    LinearLayout llImage;
    @BindView(R.id.img_license)
    ImageView imgLicense;
    @BindView(R.id.img_govt)
    ImageView imgGovt;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.ll_profile)
    LinearLayout llProfile;
    @BindView(R.id.img_log_book)
    ImageView imgLogBook;
    @BindView(R.id.ll_log_book)
    LinearLayout llLogBook;
    @BindView(R.id.img_pin)
    ImageView imgPin;
    @BindView(R.id.ll_pin)
    LinearLayout llPin;
    @BindView(R.id.img_certi)
    ImageView imgCerti;
    @BindView(R.id.ll_certi)
    LinearLayout llCerti;
    @BindView(R.id.ll_owner)
    LinearLayout llOwner;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_profile_pic)
    CircleImageView imgProfilePic;
    @BindView(R.id.rec_vehicles)
    RecyclerView recVehicles;

    private int userType;
    private String mode = "AA";
    private Bitmap bmpLicense, bmpGovt, bmpPro, bmpLog, bmpPin, bmpCerti, bmpProfilePic;
    private VehicleCheckBoxAdapter vehicleCheckBoxAdapter;
    private String peopleId;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(RegisterActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        userType = getIntent().getBundleExtra("bundle").getInt("user_type");
        Bundle bundle = getIntent().getBundleExtra("bundle");

        if (!TextUtils.isEmpty(bundle.getString("realm"))) {
            userType = getUserType(bundle.getString("realm"));
            mode = "fb";

            edtCnfrmPassword.setVisibility(View.GONE);
            edtPassword.setVisibility(View.GONE);
            String name = bundle.getString("name");
            String email = bundle.getString("email");
            peopleId = bundle.getString("peopleId");

            if (!TextUtils.isEmpty(name)) {
                edtName.setText(name);
                edtName.setEnabled(false);
            }
            if (!TextUtils.isEmpty(email)) {
                edtEmail.setText("i" + email);
                edtEmail.setEnabled(false);
            }
            if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE))) {
                new DownloadImagesTask(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE), imgProfilePic).execute();
            }

        }
        edtName.clearFocus();
        txtSubmit.requestFocus(View.FOCUS_UP);
        if (userType == 1) {
            txt1.setVisibility(View.GONE);
            llImage.setVisibility(View.GONE);
            llOwner.setVisibility(View.GONE);
            edtCompany.setVisibility(View.GONE);
            toolbar.setTitle("Register As Customer");
        }
        if (userType == 2) {
            txt1.setVisibility(View.VISIBLE);
            llImage.setVisibility(View.VISIBLE);
            llOwner.setVisibility(View.GONE);
            edtCompany.setVisibility(View.GONE);
            toolbar.setTitle("Register As Driver");

            vehicleCheckBoxAdapter = new VehicleCheckBoxAdapter(new ArrayList<String>(), RegisterActivity.this);
            recVehicles.setVisibility(View.VISIBLE);
            recVehicles.setLayoutManager(new GridLayoutManager(RegisterActivity.this, 2, GridLayoutManager.VERTICAL, false));
            recVehicles.setAdapter(vehicleCheckBoxAdapter);
            getVehicleList();
        }
        if (userType == 3) {
            txt1.setVisibility(View.VISIBLE);
            llImage.setVisibility(View.GONE);
            llOwner.setVisibility(View.VISIBLE);
            edtCompany.setVisibility(View.VISIBLE);
            toolbar.setTitle("Register As Owner");
        }
        if (mode.equalsIgnoreCase("fb")) {

            /*Glide.with(RegisterActivity.this)
                    .load((MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)))
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .placeholder(R.drawable.ic_dummy_profile)
                    .into(imgProfilePic);*/
        }

    }

    private int getUserType(String realm) {
        if (realm.equalsIgnoreCase("customer"))
            return 1;
        else if (realm.equalsIgnoreCase("driver"))
            return 2;
        else
            return 3;
    }

    private void getVehicleList() {

        ApiService.getInstance(RegisterActivity.this).makeGetCall(RegisterActivity.this, ApiService.GET_VEHICLE_TYPE + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN)
                , new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            ArrayList<String> vehiclesList = new ArrayList<String>();
                            JSONArray vehicle = response.optJSONObject("success").optJSONObject("data").optJSONArray("vehicle");
                            JSONArray machinery = response.optJSONObject("success").optJSONObject("data").optJSONArray("machinery");
                            for (int i = 0; i < vehicle.length(); i++) {
                                VehicleType vehicleType = new VehicleType(vehicle.optJSONObject(i));
                                vehiclesList.add(vehicleType.getName());
                            }
                            for (int i = 0; i < machinery.length(); i++) {
                                VehicleType vehicleType = new VehicleType(machinery.optJSONObject(i));
                                vehiclesList.add(vehicleType.getName());
                            }
                            vehicleCheckBoxAdapter.setData(vehiclesList);
                        }

                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private void signUp() {
        Map<String, DataPart> params = new HashMap<>();
        JSONObject data = new JSONObject();
        try {

            data.put("realm", getResources().getStringArray(R.array.user_type)[userType].toLowerCase());
            data.put("name", edtName.getText().toString());
            data.put("email", edtEmail.getText().toString());
            if (userType == 2) {
                if (vehicleCheckBoxAdapter.getSelVehicles() != null) {
                    data.put("canDrive", new JSONArray(vehicleCheckBoxAdapter.getSelVehicles()));
                } else {
                    Toast.makeText(this, "Please Select a Vehicle", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            if (userType == 3) {
                data.put("companyName", edtCompany.getText().toString());
            }
            data.put("mobile", edtMobile.getText().toString());
            if (mode.equalsIgnoreCase("fb")) {
                data.put("peopleId", peopleId);
            } else
                data.put("password", edtPassword.getText().toString());
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        if (userType == 2) {

            params.put("licenceIds", new DataPart(edtName.getText() + "_License" + ".jpg", MyApplication.getBytesFromBitmap(bmpLicense), "image/jpeg"));
            params.put("governmentIds", new DataPart(edtName.getText() + "_GovtId" + ".jpg", MyApplication.getBytesFromBitmap(bmpGovt), "image/jpeg"));
        }
        if (userType == 3) {

            params.put("logBook", new DataPart(edtName.getText() + "_LogBook" + ".jpg", MyApplication.getBytesFromBitmap(bmpLog), "image/jpeg"));
            params.put("copyOfPin", new DataPart(edtName.getText() + "_Pin" + ".jpg", MyApplication.getBytesFromBitmap(bmpPin), "image/jpeg"));
            params.put("companyCertificate", new DataPart(edtName.getText() + "_CompanyCertificate" + ".jpg", MyApplication.getBytesFromBitmap(bmpCerti), "image/jpeg"));

        }
        if (bmpProfilePic != null) {
            params.put("profileImage", new DataPart(edtName.getText() + "_ProfileImage" + ".jpg", MyApplication.getBytesFromBitmap(bmpProfilePic), "image/jpeg"));
        }

        params.put("data", new DataPart("", data.toString().getBytes(), "text"));

        ApiService.getInstance(RegisterActivity.this).multipartCall(RegisterActivity.this, ApiService.SIGN_UP, params, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                JSONObject responseSuccess = response.optJSONObject("success");
                if (responseSuccess != null) {
                    JSONObject msg = responseSuccess.optJSONObject("msg");
                    if (msg.optString("replyCode").equals("Success")) {
                        MyApplication.saveLogInInfo(RegisterActivity.this, responseSuccess.optJSONObject("data"));
                    }
                }
            }

            @Override
            public void onError(VolleyError volleyError) {
                try {
                    JSONObject object = new JSONObject(volleyError.getMessage());
                    Toast.makeText(RegisterActivity.this, object.getJSONObject("error").optString("message"), Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(edtName.getText())) {
            edtName.setError(StaticData.BLANK);
            return false;
        }
        if (TextUtils.isEmpty(edtPassword.getText()) && edtPassword.getVisibility() == View.VISIBLE) {
            edtPassword.setError(StaticData.BLANK);
            return false;
        }
        if (edtPassword.getText().toString().length() < 6 && edtPassword.getVisibility() == View.VISIBLE) {
            edtPassword.setError("Password Length Should Be 6");
            edtPassword.requestFocus();
            return false;
        }
        if (TextUtils.isEmpty(edtCnfrmPassword.getText()) && edtCnfrmPassword.getVisibility() == View.VISIBLE) {
            edtCnfrmPassword.setError(StaticData.BLANK);
            return false;
        }
        if (userType == 3) {
            if (TextUtils.isEmpty(edtCompany.getText())) {
                edtCompany.setError(StaticData.BLANK);
                return false;
            }
        }
        if (TextUtils.isEmpty(edtMobile.getText())) {
            edtMobile.setError(StaticData.BLANK);
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText())) {
            edtEmail.setError(StaticData.BLANK);
            return false;
        }
        if (edtPassword.getText().toString().trim().compareToIgnoreCase(edtCnfrmPassword.getText().toString().trim()) != 0) {
            edtCnfrmPassword.setError("Password does not match the confirm password");
            return false;
        }
        /*if (edtMobile.length() != 10) {
            edtMobile.setError("Invalid mobile no.");
            return false;
        }*/
        if (!MyApplication.isEmailValid(edtEmail.getText().toString())) {
            edtEmail.setError("Invalid Email address");
            return false;
        }
        if (userType == 2) {
            if (bmpProfilePic == null) {
                MyApplication.showToast(RegisterActivity.this, "Select Profile Image");
                return false;
            }
            if (bmpLicense == null) {
                MyApplication.showToast(RegisterActivity.this, "Select license image");
                return false;
            }
            if (bmpGovt == null) {
                MyApplication.showToast(RegisterActivity.this, "Select id image");
                return false;
            }
           /* if (bmpPro == null) {
                MyApplication.showToast(RegisterActivity.this, "Select passport image");
                return false;
            }*/
        }

        if (userType == 3) {
            if (bmpLog == null) {
                MyApplication.showToast(RegisterActivity.this, "Select log book image");
                return false;
            }
            if (bmpPin == null) {
                MyApplication.showToast(RegisterActivity.this, "Select pin image");
                return false;
            }
            if (bmpCerti == null) {
                MyApplication.showToast(RegisterActivity.this, "Select company certificate image");
                return false;
            }
        }

        if (bmpProfilePic == null) {
            MyApplication.showToast(RegisterActivity.this, "Select Profile Picture");
            return false;
        }

        return true;
    }

    @OnClick({R.id.img_profile_pic, R.id.ll_license, R.id.ll_govt_id, R.id.txt_submit, R.id.txt_cancel, R.id.ll_profile, R.id.ll_log_book, R.id.ll_pin, R.id.ll_certi})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_submit:
                if (MyApplication.isConnectingToInternet(RegisterActivity.this)) {
                    if (isValid()) {
                        signUp();
                    }
                } else {
                    MyApplication.showToast(RegisterActivity.this, StaticData.NO_INTERNET);
                }
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(RegisterActivity.this);
                break;
            case R.id.ll_license:
                openGallery(PICK_LICENSE);
                break;
            case R.id.ll_govt_id:
                openGallery(PICK_GOVT_ID);
                break;
            case R.id.ll_profile:
//                openGallery(PICK_PROFILE);
                break;
            case R.id.ll_log_book:
                openGallery(PICK_LOG_BOOK);
                break;
            case R.id.ll_pin:
                openGallery(PICK_PIN);
                break;
            case R.id.ll_certi:
                openGallery(PICK_CERTIFICATE);
                break;
            case R.id.img_profile_pic:
                openGallery(PICK_PROFILE_PIC);
                break;
        }
    }

    private void openGallery(int PICK_IMAGE) {
        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(RegisterActivity.this);
        if (marshMallowPermission.checkPermissionForREAD_EXTERNAL_STORAGE()) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        } else {
            marshMallowPermission.requestPermissionForREAD_EXTERNAL_STORAGE();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data.getData() != null) {
            Uri selectedImage = null;
            selectedImage = data.getData();
            Log.d("selectedImage", selectedImage.toString());
            try {
                if (requestCode == PICK_LICENSE) {
                    bmpLicense = null;
                    imgLicense.setImageBitmap(null);
                    bmpLicense = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgLicense.setImageBitmap(bmpLicense);

                } else if (requestCode == PICK_GOVT_ID) {
                    bmpGovt = null;
                    imgGovt.setImageBitmap(null);
                    bmpGovt = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                    imgGovt.setImageBitmap(bmpGovt);

                } else if (requestCode == PICK_LOG_BOOK) {
                    bmpLog = null;
                    imgLogBook.setImageBitmap(null);
                    bmpLog = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgLogBook.setImageBitmap(bmpLog);

                } else if (requestCode == PICK_PIN) {
                    bmpPin = null;
                    imgPin.setImageBitmap(null);
                    bmpPin = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgPin.setImageBitmap(bmpPin);

                } else if (requestCode == PICK_CERTIFICATE) {
                    bmpCerti = null;
                    imgCerti.setImageBitmap(null);
                    bmpCerti = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgCerti.setImageBitmap(bmpCerti);

                } else if (requestCode == PICK_PROFILE_PIC) {
                    bmpProfilePic = null;
                    imgProfilePic.setImageBitmap(null);
                    bmpProfilePic = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                    imgProfilePic.setImageBitmap(bmpProfilePic);

                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public class DownloadImagesTask extends AsyncTask<Void, Void, Bitmap> {
        String url;
        ImageView imageView;

        public DownloadImagesTask(String url, ImageView imageView) {
            this.url = url;
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(Void... imageViews) {
            return download_Image(url);
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            imageView.setImageBitmap(result);
        }

        private Bitmap download_Image(String url) {

            Bitmap bmp = null;
            try {
                URL ulrn = new URL(url);
                HttpURLConnection con = (HttpURLConnection) ulrn.openConnection();
                InputStream is = con.getInputStream();
                bmp = BitmapFactory.decodeStream(is);
                if (null != bmp)
                    return bmp;

            } catch (Exception e) {
                e.printStackTrace();
            }
            return bmp;
        }
    }
}
