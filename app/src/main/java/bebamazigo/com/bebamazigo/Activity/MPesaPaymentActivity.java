package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.victor.loading.rotate.RotateLoading;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.MessageEvent;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.SweetAlertDialog.SweetAlertDialog;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MPesaPaymentActivity extends AppCompatActivity {

    final Handler handler = new Handler();
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rotateloading)
    RotateLoading rotateloading;
    String trans_id;
    long startTime = System.currentTimeMillis();
    private boolean isFirstTime = true;

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {

            if (!trans_id.isEmpty()) {

                getMPesaAddMoneyResponse(trans_id);

                long currTime = System.currentTimeMillis();
                long elapsedtime = currTime - startTime;
                if (elapsedtime > 20000) {
                    if (isFirstTime) {

                        isFirstTime = true;

                        startTime = System.currentTimeMillis();
                        currTime = 0;
                        elapsedtime = 0;

                        handler.removeCallbacks(runnable);

                        showAlertDialog(3, "Transaction Pending!!", false);

                    }
                }
            }
            handler.postDelayed(runnable, 5000);
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpesa_payment);
        ButterKnife.bind(this, this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Add Money MPesa");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        EventBus.getDefault().register(this);

        rotateloading.start();

        Bundle bundle = getIntent().getExtras();
        trans_id = bundle.getString("trans_id");

        if (trans_id.isEmpty()) {
            finish();
            MyApplication.showToast(this, "Something Went Wrong");
        }

        Log.d("TRANS_ID", trans_id);

        runnable.run();

    }

    private void showAlertDialog(int type, String contentTitle, boolean cancelable) {

        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(MPesaPaymentActivity.this, type); //For Success Pass 2 For Warning Pass 3
        if (sweetAlertDialog.isShowing()) {
            sweetAlertDialog.dismissWithAnimation();
        }
        sweetAlertDialog.setTitleText(contentTitle); //Title Of Dialog
        sweetAlertDialog.setConfirmText("Okay");
        sweetAlertDialog.showCancelButton(false);
        sweetAlertDialog.setCancelable(cancelable);
        sweetAlertDialog.setCanceledOnTouchOutside(cancelable);
        sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                sDialog.cancel();
                finish();
            }
        })
                .show();
    }

    private void getMPesaAddMoneyResponse(String trans_id) {
        String url = ApiService.GET_MPesaAddMoneyResponse + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&transactionId=" + trans_id;
        Log.d("REQUEST URL", url);
        ApiService.getInstance(MPesaPaymentActivity.this).makeGetCall(getApplicationContext(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                if (response.optJSONObject("success") != null) {

                    JSONObject data = response.optJSONObject("success").optJSONObject("data");

                    if (data.optString("paymentStatus").equalsIgnoreCase("complete")) {
                        handler.removeCallbacks(runnable);
                        showAlertDialog(2, "Transaction Success", false);
                    } else if (data.optString("paymentStatus").equalsIgnoreCase("incomplete")) {
                        handler.removeCallbacks(runnable);
                        showAlertDialog(3, "Transaction Failed!!", false);
                    }

                } else {
                    MyApplication.showToast(MPesaPaymentActivity.this, "Something Went Wrong");
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(MPesaPaymentActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent messageEvent) {

        if (messageEvent != null) {
            //Do Work Here
            Toast.makeText(this, "onMessageEvent", Toast.LENGTH_SHORT).show();
            Log.d("onMessageEvent", messageEvent.getMessage());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
        if (handler != null)
            handler.removeCallbacks(runnable);
    }

}
