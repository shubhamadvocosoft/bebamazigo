package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class JobDetailMachinary extends AppCompatActivity {

    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_machinary_type)
    TextView txtMachinaryType;
    @BindView(R.id.txt_job_loc)
    TextView txtJobLoc;
    @BindView(R.id.txt_start_date)
    TextView txtStartDate;
    @BindView(R.id.txt_end_date)
    TextView txtEndDate;
    @BindView(R.id.txt_budget)
    TextView txtBudget;
    @BindView(R.id.txt_discription)
    TextView txtDiscription;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    Gson gson = new Gson();
    String jobId;
    TripModel model;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_machinary_job_details);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Job Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Job Detail");

        jobId = getIntent().getBundleExtra("bundle").getString("jobId");
        if (!TextUtils.isEmpty(jobId))
            getJob();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(JobDetailMachinary.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getJob() {
        String url = ApiService.GET_JOB_ID + "&jobId=" + jobId;
        ApiService.getInstance(JobDetailMachinary.this).makeGetCall(JobDetailMachinary.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("responseJobDetail", response.toString());
                model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), TripModel.class);

                if (model.getStatus().compareTo("end") == 0) {

                    if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareToIgnoreCase("customer") == 0) {
                        showCompleteDialog();
                    } else {

                    }
                }
                setDetail();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setDetail() {

        txtTitle.setText(model.getTitle());
        txtMachinaryType.setText(model.getTypeOfVehicle());
        txtBudget.setText("KSH " + model.getNetPrice());
        txtJobLoc.setText(model.getSource().getAddress());
        txtStartDate.setText(MyApplication.utcToDate(model.getDate()) + " , " + MyApplication.utcToTime(model.getDate()));
        txtEndDate.setText(MyApplication.utcToDate(model.getEndDate()) + " , " + MyApplication.utcToTime(model.getEndDate()));
        txtDiscription.setText(model.getDescription());

    }

    private void showCompleteDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View yourCustomView = inflater.inflate(R.layout.dialog_complete_job, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(yourCustomView)
                .create();

        TextView txtComplete = (TextView) yourCustomView.findViewById(R.id.txt_complete);
        TextView txtDispute = (TextView) yourCustomView.findViewById(R.id.txt_dispute);

        txtComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                jobComplete();
            }
        });
        txtDispute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                showDisputeDialog();

            }
        });
        dialog.show();
    }


    private void showDisputeDialog() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View yourCustomView = inflater.inflate(R.layout.dialog_dispute, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(yourCustomView)
                .create();
        final EditText editComment = (EditText) yourCustomView.findViewById(R.id.edit_comment);
        TextView txtSubmit = (TextView) yourCustomView.findViewById(R.id.txt_submit);

        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editComment.getText().toString().isEmpty()) {
                    MyApplication.showToast(JobDetailMachinary.this, "comment something!!!");
                } else {
                    dialog.dismiss();
                    jobDispute(editComment.getText().toString());
                }
            }
        });
        dialog.show();
    }

    private void jobComplete() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("status", "completed");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(JobDetailMachinary.this).makePostCallAuth(JobDetailMachinary.this, ApiService.COMPLETE_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(JobDetailMachinary.this, "Job completed!!!");

                Bundle bundle = new Bundle();
                bundle.putString("fare", model.getNetPrice().toString());
                bundle.putString("source", model.getSource().getAddress());
                bundle.putString("destination", model.getDestination().getAddress());
                bundle.putString("jobId", jobId);
                bundle.putSerializable("data", (Serializable) model);
                MyApplication.activityStart(JobDetailMachinary.this, TripEndActivity.class, false, bundle);
                finish();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void jobDispute(String comment) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("status", "dispute");
            param.put("comment", comment);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(JobDetailMachinary.this).makePostCallAuth(JobDetailMachinary.this, ApiService.COMPLETE_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(JobDetailMachinary.this, "Job disputed successfully!!!");
                finish();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
