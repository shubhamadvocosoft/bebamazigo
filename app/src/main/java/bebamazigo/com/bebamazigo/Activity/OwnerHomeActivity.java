package bebamazigo.com.bebamazigo.Activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import bebamazigo.com.bebamazigo.Adapter.DrawerListAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.Fragments.FragmentDocumentOwn;
import bebamazigo.com.bebamazigo.Fragments.FragmentEditProfile;
import bebamazigo.com.bebamazigo.Fragments.FragmentTrips;
import bebamazigo.com.bebamazigo.Fragments.FragmentVehicles;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class OwnerHomeActivity extends AppCompatActivity {
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.imageView)
    CircleImageView imageView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.contentContainer)
    FrameLayout contentContainer;
    FragmentTransaction transaction;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private DrawerListAdapter drawerListAdapter;
    private ListView lv_drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_owner_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        changeFragment(FragmentVehicles.getInstance(), "My Vehicles");

        init();
        setDrawer();
        setDrawerHeader();
    }

    public void changeFragment(Fragment fragment, String title) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.contentContainer, fragment).addToBackStack("fragback").commit();
        /*transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentContainer, fragment);
        transaction.commit();*/

        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.contentContainer);
            if (f instanceof FragmentVehicles) {
                /*super.onBackPressed();*/
                showExitDialog();
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                /*getSupportFragmentManager().popBackStack();*/
                changeFragment(FragmentVehicles.getInstance(), "My Vehicles");
            } else {
                showExitDialog();
            }
        }
    }

    private void showExitDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(OwnerHomeActivity.this)
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.activityFinish(OwnerHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.notification) {
            MyApplication.activityStart(OwnerHomeActivity.this, NotificationActivity.class, false);
        }
        return super.onOptionsItemSelected(item);
    }

    private void init() {
        lv_drawer = (ListView) findViewById(R.id.lv_drawer);
    }

    private void setDrawerHeader() {
        textName.setText(MyApplication.getSharedPrefString(StaticData.SP_USER_NAME));
        if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE))) {
            Log.d("profileImage", MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE));

            Glide.with(OwnerHomeActivity.this)
                    .load((MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)))
                    .centerCrop()
                    .into(imageView);
        }
    }

    private void setDrawer() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(OwnerHomeActivity.this);
        drawerListAdapter.addMenu("Home", R.drawable.home_white, R.drawable.home_white);
        drawerListAdapter.addMenu("View Trips", R.drawable.view_trip_white, R.drawable.view_trip_white);
        drawerListAdapter.addMenu("Update Profile", R.drawable.update_profile_white, R.drawable.update_profile_white);
        drawerListAdapter.addMenu("Documents", R.drawable.doc_white, R.drawable.doc_white);
        drawerListAdapter.addMenu("Wallet", R.drawable.wallet_white, R.drawable.wallet_white);
        drawerListAdapter.addMenu("Logout", R.drawable.logout_white, R.drawable.logout_white);

        lv_drawer.setAdapter(drawerListAdapter);

        lv_drawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                drawerListAdapter.setSelectedPosition(position);
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }

                if (position == 0) {
                    changeFragment(FragmentVehicles.getInstance(), "My Vehicles");
                } else if (position == 1) {
                    changeFragment(FragmentTrips.getInstance(), "Jobs");
                } else if (position == 2) {
                    changeFragment(FragmentEditProfile.getInstance(), "Update Profile");
                } else if (position == 3) {
                    changeFragment(FragmentDocumentOwn.getInstance(), "My Documents");
                } else if (position == 4) {
                    MyApplication.activityStart(OwnerHomeActivity.this, WalletActivity.class, false);
                } else if (position == 5) {
                    showLogOutDialog();
                }

            }
        });

    }

    @Override
    protected void onResume() {
        if (drawerLayout != null) {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        }
        setDrawerHeader();
        super.onResume();
    }

    private void showLogOutDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(OwnerHomeActivity.this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.logout(OwnerHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

}
