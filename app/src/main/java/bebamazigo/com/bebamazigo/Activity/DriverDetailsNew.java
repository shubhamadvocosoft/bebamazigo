package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import hyogeun.github.com.colorratingbarlib.ColorRatingBar;

public class DriverDetailsNew extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.img_profile)
    ImageView imgProfile;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.rating)
    ColorRatingBar rating;
    @BindView(R.id.txt_mobile)
    TextView txtMobile;
    @BindView(R.id.txt_email)
    TextView txtEmail;
    @BindView(R.id.ll_avialble)
    LinearLayout llAvialble;
    @BindView(R.id.img_dl)
    ImageView imgDl;
    @BindView(R.id.img_id)
    ImageView imgId;
    @BindView(R.id.btn_remove_driver)
    Button btnRemoveDriver;

    String driverId, vehicleId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_details_new);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Driver Detail");

        getPeopleInfo();

        btnRemoveDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDriver();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(DriverDetailsNew.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getPeopleInfo() {

        String url = ApiService.PeopleInfo + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&peopleId=" + getIntent().getBundleExtra("bundle").getString("id");
        ApiService.getInstance(DriverDetailsNew.this).makeGetCall(DriverDetailsNew.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("responseDriverDetail", response.toString());
                try {
                    JSONObject data = response.getJSONObject("success").getJSONObject("data");
                    JSONArray canDriveArr = data.getJSONArray("canDrive");
                    Picasso.get().load(ApiService.BASE_URL_IMAGE + data.optString("profileImage")).into(imgProfile);
                    txtName.setText(data.optString("name"));
                    txtMobile.setText(data.optString("mobile"));
                    txtEmail.setText(data.optString("email"));
                    rating.setRating((float) data.getJSONObject("rating").optDouble("avgRating"));

                    vehicleId = data.optString("vehicleId");

                    if (data.getJSONArray("licenceIds").length() != 0)
                        Picasso.get().load(ApiService.BASE_URL_IMAGE + data.getJSONArray("licenceIds").getString(0)).into(imgDl);
                    if (data.getJSONArray("governmentIds").length() != 0)
                        Picasso.get().load(ApiService.BASE_URL_IMAGE + data.getJSONArray("governmentIds").getString(0)).into(imgId);

                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    layoutParams.setMargins(10, 5, 10, 5);

                    LinearLayout.LayoutParams paramsWigit = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    paramsWigit.weight = 1;

                    for (int i = 0; i < canDriveArr.length(); i++) {
                        LinearLayout layout = new LinearLayout(DriverDetailsNew.this);
                        layout.setLayoutParams(layoutParams);
                        layout.setOrientation(LinearLayout.HORIZONTAL);
                        layout.setWeightSum(2);
                        for (int j = 0; j < 2; j++) {
                            i = i + j;

                            TextView tv = new TextView(DriverDetailsNew.this);
                            tv.setLayoutParams(paramsWigit);
                            tv.setText(canDriveArr.getString(i));
                            tv.setTextColor(getResources().getColor(R.color.black));

                            layout.addView(tv);
                        }

                        llAvialble.addView(layout);
                    }

                    if (MyApplication.getSharedPrefString(StaticData.SP_ID).compareTo(data.getJSONObject("vehicle").optString("ownerId")) == 0) {
                        btnRemoveDriver.setVisibility(View.VISIBLE);
                    } else {
                        btnRemoveDriver.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


    private void removeDriver() {
        JSONObject param = new JSONObject();
        try {
            Bundle bundle = getIntent().getBundleExtra("bundle");
            driverId = bundle.getString("id");
            param.put("driverId", driverId);
            param.put("vehicleId", vehicleId);

            Log.d("removeDriverParam"," driver " + driverId + " vehicle " + vehicleId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(DriverDetailsNew.this).makePostCallAuth(DriverDetailsNew.this, ApiService.Remove_DRIVER, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(DriverDetailsNew.this, "remove successful!!!");
                finish();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
