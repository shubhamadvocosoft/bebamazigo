package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VehicleJobDetail extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_vehicle)
    TextView txtVehicle;
    @BindView(R.id.txt_pickup_location)
    TextView txtPickupLocation;
    @BindView(R.id.txt_drop_location)
    TextView txtDropLocation;
    @BindView(R.id.txt_total_weight)
    TextView txtTotalWeight;
    @BindView(R.id.txt_total_distance)
    TextView txtTotalDistance;
    @BindView(R.id.txt_value_of_goods)
    TextView txtValueOfGoods;
    @BindView(R.id.txt_nature_of_goods)
    TextView txtNatureOfGoods;
    @BindView(R.id.txt_net_price)
    TextView txtNetPrice;
    @BindView(R.id.txt_job_date)
    TextView txtJobDate;
    @BindView(R.id.txt_discription)
    TextView txtDiscription;
    String jobId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_job_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Job Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Job Detail");

        jobId = getIntent().getBundleExtra("bundle").getString("jobId");
        if (!TextUtils.isEmpty(jobId)) {
            getJob();
        } else {
            MyApplication.showToast(this,"Job Not Found");
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(VehicleJobDetail.this);
        }
        return super.onOptionsItemSelected(item);
    }


    private void getJob() {
        String url = ApiService.GET_JOB_ID + "&jobId=" + jobId;
        ApiService.getInstance(VehicleJobDetail.this).makeGetCall(VehicleJobDetail.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                try {
                    Log.d("responseJobDetail", response.toString());

                    txtTitle.setText(response.optJSONObject("success").optJSONObject("data").optString("title"));
                    txtPickupLocation.setText(response.optJSONObject("success").optJSONObject("data").optJSONObject("source").optString("address"));
                    txtDropLocation.setText(response.optJSONObject("success").optJSONObject("data").optJSONObject("destination").optString("address"));
                    txtTotalWeight.setText(response.optJSONObject("success").optJSONObject("data").optString("weight"));
                    txtTotalDistance.setText(response.optJSONObject("success").optJSONObject("data").optJSONObject("distance").optString("text"));
                    txtValueOfGoods.setText(response.optJSONObject("success").optJSONObject("data").optString("valueOfGoods"));
                    txtNatureOfGoods.setText(response.optJSONObject("success").optJSONObject("data").optString("natureOfGoods"));
                    txtNetPrice.setText(response.optJSONObject("success").optJSONObject("data").optString("netPrice"));
                    txtJobDate.setText(
                            MyApplication.utcToDate(response.optJSONObject("success").optJSONObject("data").optString("date")
                                    + " , " +
                                    MyApplication.utcToTime(response.optJSONObject("success").optJSONObject("data").optString("date"))
                            ));
                    txtDiscription.setText(response.optJSONObject("success").optJSONObject("data").optString("description"));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

}
