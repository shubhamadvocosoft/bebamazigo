package bebamazigo.com.bebamazigo.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.VehicleType;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddJobActivity extends AppCompatActivity {
    int type;
    final static int ACTIVITY_CODE = 101;
    @BindView(R.id.edt_title)
    EditText edtTitle;
    @BindView(R.id.spn_vehicle_type)
    Spinner spnVehicleType;
    @BindView(R.id.places_source)
    PlacesAutocompleteTextView placesSource;
    @BindView(R.id.places_destination)
    PlacesAutocompleteTextView placesDestination;
    @BindView(R.id.txt_accept)
    TextView txtAccept;
    @BindView(R.id.txt_find)
    TextView txtFind;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    String sourceId, destinationId;
    JSONObject sourceJSON, destinationJSON;
    PlaceDetails sourcePlace, destinationPlace;
    ArrayList<VehicleType> vehiclesList = new ArrayList<>();
    ArrayList<VehicleType> machineryList = new ArrayList<>();
    @BindView(R.id.spn_weight)
    Spinner spnWeight;
    @BindView(R.id.spn_trailer_type)
    Spinner spnTrailerType;
    JSONArray selectedRoute = new JSONArray();
    JSONObject duration = new JSONObject();

    JSONObject distance = new JSONObject();
    @BindView(R.id.edt_weight)
    EditText edtWeight;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.edt_description)
    EditText edtDescription;
    @BindView(R.id.edt_nature)
    EditText edtNature;
    @BindView(R.id.edt_value)
    EditText edtValue;
    @BindView(R.id.edt_budget)
    EditText edtBudget;
    @BindView(R.id.edt_net)
    TextView edtNet;
    String title,
            weight,
            valueOfGoods,
            budget,
            netPrice,
            typeOfVehicle,
            date,
            description,
            natureOfGoods,
            vehicleBodyType,
            typeOfJob;
    int vehicleTonnage;
    @BindView(R.id.spn_job_type)
    Spinner spnJobType;
    @BindView(R.id.spn_machinery_type)
    Spinner spnMachineryType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);
        ButterKnife.bind(this);
        type = getIntent().getBundleExtra("bundle").getInt("type");
        typeOfJob = type == 0 ? "on demand" : "standard rate";

        if (type == 0) {
            edtNet.setVisibility(View.GONE);
        } else {
            edtNet.setVisibility(View.VISIBLE);
        }


        getVehicle(type);
        placesSource.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull final Place place) {
                sourceId = place.place_id;
//                MyApplication.showProgressDialog("Getting Location");
                placesSource.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                        sourcePlace = placeDetails;

                        sourceJSON = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            sourceJSON.put("address", placeDetails.formatted_address);
                            sourceJSON.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                    }
                });
            }
        });
        placesDestination.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
//                MyApplication.showProgressDialog("Getting Location");
                placesDestination.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                        destinationPlace = placeDetails;
                        destinationJSON = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            destinationJSON.put("address", placeDetails.formatted_address);
                            destinationJSON.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
//                        MyApplication.hideProgressDialog(AddJobActivity.this);
                    }
                });
            }
        });
        placesSource.setResultType(AutocompleteResultType.NO_TYPE);
        placesDestination.setLocationBiasEnabled(true);
        placesSource.setLocationBiasEnabled(true);


        placesSource.setOnClearListener(new PlacesAutocompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                sourcePlace = null;
            }
        });
        placesDestination.setOnClearListener(new PlacesAutocompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                destinationPlace = null;
            }
        });


        spnVehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                } else {
                    changeUI(position - 1, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnMachineryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                } else {
                    changeUI(position - 1, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void changeUI(int position, boolean isVeh) {
        VehicleType vehicleType;
        if (isVeh) {
            vehicleType = vehiclesList.get(position);
            txtDate.setVisibility(View.GONE);

            placesDestination.setVisibility(View.VISIBLE);
            edtNature.setVisibility(View.VISIBLE);
            edtValue.setVisibility(View.VISIBLE);
            placesDestination.setVisibility(View.VISIBLE);
            txtFind.setVisibility(View.VISIBLE);
            edtWeight.setVisibility(View.VISIBLE);
        } else {
            vehicleType = machineryList.get(position);
            txtDate.setVisibility(View.VISIBLE);

            placesDestination.setVisibility(View.GONE);
            edtNature.setVisibility(View.GONE);
            edtValue.setVisibility(View.GONE);
            placesDestination.setVisibility(View.GONE);
            txtFind.setVisibility(View.GONE);
            edtWeight.setVisibility(View.GONE);

        }
        if (vehicleType.getBodyType() != null) {
            spnTrailerType.setVisibility(View.VISIBLE);
            spnTrailerType.setAdapter(new HintAdapter(AddJobActivity.this, R.layout.item_spinner, vehiclesList.get(position).getBodyType()));
        } else {
            spnTrailerType.setVisibility(View.GONE);
        }

        if (vehicleType.getTon() != null) {
            spnWeight.setVisibility(View.VISIBLE);
            spnWeight.setAdapter(new HintAdapter(AddJobActivity.this, R.layout.item_spinner, (vehicleType.getTon())));
        } else {
            spnWeight.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(distance.optString("value"))) {
            getPrice();
        }
    }

    @OnClick({R.id.txt_accept, R.id.txt_find, R.id.txt_cancel, R.id.txt_date})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_accept:
                if (isValid()) {
                    postJob();
                }
                break;
            case R.id.txt_find:
                if (sourcePlace != null && destinationPlace != null) {
                    Bundle bundle = new Bundle();
                    bundle.putDouble("latS", sourcePlace.geometry.location.lat);
                    bundle.putDouble("lngS", sourcePlace.geometry.location.lng);
                    bundle.putString("addressS", sourcePlace.formatted_address);

                    bundle.putDouble("latD", destinationPlace.geometry.location.lat);
                    bundle.putDouble("lngD", destinationPlace.geometry.location.lng);
                    bundle.putString("addressD", destinationPlace.formatted_address);
                    startActivityForResult(new Intent(AddJobActivity.this, MapsActivity.class).putExtra("bundle", bundle), ACTIVITY_CODE);
                    overridePendingTransition(R.anim.exit, R.anim.enter);

                } else {
                    MyApplication.showToast(AddJobActivity.this, "Enter source & destination");
                }
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(AddJobActivity.this);
                break;
            case R.id.txt_date:
                selectDateTime();
        }
    }

    private void selectDateTime() {
        SwitchDateTimeDialogFragment dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                "Title example",
                "OK",
                "Cancel"
        );

// Assign values

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        dateTimeFragment.startAtCalendarView();
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setMaximumDateTime(cal.getTime());
        dateTimeFragment.setDefaultDateTime(Calendar.getInstance().getTime());
// Or assign each element, default element is the current moment
// dateTimeFragment.setDefaultHourOfDay(15);
// dateTimeFragment.setDefaultMinute(20);
// dateTimeFragment.setDefaultDay(4);
// dateTimeFragment.setDefaultMonth(Calendar.MARCH);
// dateTimeFragment.setDefaultYear(2017);

// Define new day and month format
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateTimeFragment.setSimpleDateMonthAndDayFormat(format);
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {

        }

// Set listener
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date selectedDate) {
                long millis = selectedDate.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
                date = sdf.format(new Date(millis));
                txtDate.setTextColor(Color.BLACK);
                txtDate.setText(MyApplication.utcToDate(date) + " " + MyApplication.utcToTime(date));

            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

// Show
        dateTimeFragment.show(getSupportFragmentManager(), "dialog_time");
    }

    private void postJob() {
        ApiService.getInstance(AddJobActivity.this).makePostCallAuth(AddJobActivity.this, ApiService.ADD_JOB,
                getParam(), new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            MyApplication.showToast(AddJobActivity.this, response.optJSONObject("success").optJSONObject("msg").optString("replyMessage"));
                            finish();
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private boolean isValid() {
        if (TextUtils.isEmpty(edtTitle.getText())) {
            edtTitle.setError(StaticData.BLANK);
            return false;
        } else {
            title = edtTitle.getText().toString();
        }

        if (TextUtils.isEmpty(edtDescription.getText())) {
            edtDescription.setError(StaticData.BLANK);
            return false;
        } else {
            description = edtDescription.getText().toString();
        }

        if (TextUtils.isEmpty(edtNature.getText())) {
            edtNature.setError(StaticData.BLANK);
            return false;
        } else {
            natureOfGoods = edtNature.getText().toString();
        }

        if (TextUtils.isEmpty(edtBudget.getText())) {
            edtBudget.setError(StaticData.BLANK);
            return false;
        } else {
            budget = edtBudget.getText().toString();
        }
        if (type == 1) {
            if (TextUtils.isEmpty(edtNet.getText())) {
                return false;
            } else {
                netPrice = edtNet.getText().toString();
            }
        }
        if (TextUtils.isEmpty(edtValue.getText())) {
            edtValue.setError(StaticData.BLANK);
            return false;
        } else {
            valueOfGoods = edtValue.getText().toString();
        }

        if (TextUtils.isEmpty(edtWeight.getText())) {
            edtWeight.setError(StaticData.BLANK);
            return false;
        } else {
            weight = edtWeight.getText().toString();
        }

        if (spnJobType.getSelectedItemPosition() == 0) {
            MyApplication.showToast(AddJobActivity.this, "Please Select Vehicle or Machinery Type");
            return false;
        }

        if (spnVehicleType.getVisibility() != View.GONE) {
            if (spnVehicleType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddJobActivity.this, "Please Select Vehicle Type");
                return false;
            } else {
                typeOfVehicle = spnVehicleType.getSelectedItem().toString();
            }
        }

        if (spnMachineryType.getVisibility() != View.GONE) {
            if (spnMachineryType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddJobActivity.this, "Please Select Machinery Type");
                return false;
            } else {
                typeOfVehicle = spnMachineryType.getSelectedItem().toString();
            }
        }

        if (spnTrailerType.getVisibility() == View.VISIBLE) {
            if (spnTrailerType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddJobActivity.this, "Please Select Body Type");
                return false;
            } else {
                vehicleBodyType = spnTrailerType.getSelectedItem().toString();
            }
        }

        if (spnWeight.getVisibility() == View.VISIBLE) {
            if (spnWeight.getSelectedItemPosition() == 0) {
                MyApplication.showToast(AddJobActivity.this, "Please Select Tonnage");
                return false;
            } else {
                vehicleTonnage = Integer.parseInt(spnWeight.getSelectedItem().toString());
            }
        }

        if (TextUtils.isEmpty(date)) {
            MyApplication.showToast(AddJobActivity.this, "Please Select Date");
            return false;
        }
        return true;
    }

    private void getVehicle(int type) {
        String url = "";
        if (type == 0) {
            url = ApiService.GET_VEHICLE_TYPE + "&typeOfJob=" + "standard rate";
        } else {
            url = ApiService.GET_VEHICLE_TYPE + "&typeOfJob=" + "on demand";
        }
        ApiService.getInstance(AddJobActivity.this).makeGetCall(AddJobActivity.this, url
                , new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            JSONArray vehicle = response.optJSONObject("success").optJSONObject("data").optJSONArray("vehicle");
                            JSONArray machinery = response.optJSONObject("success").optJSONObject("data").optJSONArray("machinery");
                            for (int i = 0; i < vehicle.length(); i++) {
                                vehiclesList.add(new VehicleType(vehicle.optJSONObject(i)));
                            }
                            for (int i = 0; i < machinery.length(); i++) {
                                machineryList.add(new VehicleType(machinery.optJSONObject(i)));
                            }
                        }
                        spnVehicleType.setVisibility(View.GONE);
                        spnMachineryType.setVisibility(View.GONE);

                        List<String> vehMach = new ArrayList<String>();
                        vehMach.add("Vehicle/Machinary");
                        if (vehiclesList.size() > 0) {
                            vehMach.add("Vehicle");
                        }
                        if (machineryList.size() > 0) {
                            vehMach.add("Machinary");
                        }

                        spnJobType.setAdapter(new HintAdapter(AddJobActivity.this, R.layout.item_spinner, vehMach.toArray(new String[vehMach.size()])));

                        spnJobType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 1) {
                                    spnVehicleType.setVisibility(View.VISIBLE);
                                    spnMachineryType.setVisibility(View.GONE);
                                    spnVehicleType.setAdapter(new HintAdapter(AddJobActivity.this, R.layout.item_spinner, getVehicleList()));
                                } else if (position == 2) {
                                    spnVehicleType.setVisibility(View.GONE);
                                    spnMachineryType.setVisibility(View.VISIBLE);
                                    spnMachineryType.setAdapter(new HintAdapter(AddJobActivity.this, R.layout.item_spinner, getMachineList()));
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private String[] getVehicleList() {
        List<String> vehicle = new ArrayList<>();
        vehicle.add("Vehicle");

        for (int i = 0; i < vehiclesList.size(); i++) {
            vehicle.add(vehiclesList.get(i).getName());
        }
        return vehicle.toArray(new String[vehicle.size()]);
    }

    private String[] getMachineList() {
        List<String> vehicle = new ArrayList<>();
        vehicle.add("Machine");

        for (int i = 0; i < machineryList.size(); i++) {
            vehicle.add(machineryList.get(i).getName());
        }
        return vehicle.toArray(new String[vehicle.size()]);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_CODE && resultCode == RESULT_OK) {

            try {
                distance.put("text", StaticData.CURRENT_ROUTE.getDistance());
                distance.put("value", StaticData.CURRENT_ROUTE.getValueDistance());
                duration.put("text", StaticData.CURRENT_ROUTE.getDuration());
                duration.put("value", StaticData.CURRENT_ROUTE.getValueDuration());


                if (spnVehicleType.getSelectedItemPosition() != 0) {
                    getPrice();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    public JSONObject getParam() {
        JSONObject param = new JSONObject();
        try {
            param.put("title", title);
            param.put("weight", weight);
            param.put("valueOfGoods", Integer.parseInt(valueOfGoods));
            param.put("source", sourceJSON);
            param.put("destination", destinationJSON);
            param.put("budget", Integer.parseInt(budget));
            if (type == 1) {
                param.put("netPrice", Integer.parseInt(netPrice));
            } else
                param.put("netPrice", Integer.parseInt(budget));
            param.put("typeOfVehicle", typeOfVehicle);
            param.put("date", date);
            param.put("description", description);
            param.put("natureOfGoods", natureOfGoods);
            param.put("typeOfJob", typeOfJob);
            param.put("duration", duration);
            param.put("distance", distance);
            param.put("selectedRoute", selectedRoute);
            param.put("vehicleTonnage", vehicleTonnage);
            param.put("vehicleBodyType", vehicleBodyType);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private void getPrice() {
        String url = ApiService.JOB_PRICING + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) +
                "&vehicleName=" + spnVehicleType.getSelectedItem() + "&distanceInMeter=" + distance.optString("value");
        if (spnWeight.getVisibility() == View.VISIBLE) {
            url = url + "&tonnage=" + spnWeight.getSelectedItem();
        }
        ApiService.getInstance(AddJobActivity.this).makeGetCall(AddJobActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                edtNet.setText(response.optJSONObject("success").optString("data"));
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
