package bebamazigo.com.bebamazigo.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;

import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Adapter.DrawerListAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.Service.LocationTracker;
import bebamazigo.com.bebamazigo.Fragments.FragmentDocument;
import bebamazigo.com.bebamazigo.Fragments.FragmentDriverHome;
import bebamazigo.com.bebamazigo.Fragments.FragmentEditProfile;
import bebamazigo.com.bebamazigo.Fragments.FragmentMyVehicles;
import bebamazigo.com.bebamazigo.Fragments.FragmentTrips;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class DriverHomeActivity extends AppCompatActivity {

    @BindView(R.id.contentContainer)
    FrameLayout contentContainer;
    @BindView(R.id.boundry)
    ImageView boundry;
    @BindView(R.id.imageView)
    CircleImageView imageView;
    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.rl_profile)
    LinearLayout rlProfile;
    Gson gson = new Gson();
    FragmentTransaction transaction;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.lv_drawer)
    ListView lvDrawer;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    float lastTranslate = 0.0f;
    DrawerListAdapter drawerListAdapter;
    String jobId;
    String frag_status = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getJobId();

        setDrawer();
        frag_status = "Home";

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            startForegroundService(new Intent(DriverHomeActivity.this, LocationTracker.class));
        } else {
            startService(new Intent(DriverHomeActivity.this, LocationTracker.class));
        }

    }

    private void getJobId() {
        ApiService.getInstance(DriverHomeActivity.this).makeGetCall(this, ApiService.GET_INFO +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response_info", response.toString());
                if (response.optJSONObject("success") != null) {
                    jobId = response.optJSONObject("success").optJSONObject("data").optString("jobId");

                    changeFragment(FragmentDriverHome.getInstance(jobId), "Home");
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notification, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.notification) {
            MyApplication.activityStart(DriverHomeActivity.this, NotificationActivity.class, false);
        }
        return super.onOptionsItemSelected(item);
    }


    private void setDrawer() {
        textName.setText(MyApplication.getSharedPrefString(StaticData.SP_USER_NAME));
        if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE))) {
            Glide.with(DriverHomeActivity.this)
                    .load((MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)))
                    .centerCrop()
                    .into(imageView);
        }


        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                super.onDrawerSlide(drawerView, slideOffset);
                float moveFactor = (lvDrawer.getWidth() * slideOffset);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                    contentContainer.setTranslationX(moveFactor);
                } else {
                    TranslateAnimation anim = new TranslateAnimation(lastTranslate, moveFactor, 0.0f, 0.0f);
                    anim.setDuration(0);
                    anim.setFillAfter(true);
                    contentContainer.startAnimation(anim);

                    lastTranslate = moveFactor;
                }
            }
        };


        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        drawerListAdapter = new DrawerListAdapter(DriverHomeActivity.this);
        drawerListAdapter.addMenu("Home", R.drawable.home_white, R.drawable.home_white);
        drawerListAdapter.addMenu("My Trips", R.drawable.view_trip_white, R.drawable.view_trip_white);
        drawerListAdapter.addMenu("My Vehicle", R.drawable.vehicle_white, R.drawable.vehicle_white);
        drawerListAdapter.addMenu("Document", R.drawable.doc_white, R.drawable.doc_white);
        drawerListAdapter.addMenu("Update Profile", R.drawable.update_profile_white, R.drawable.update_profile_white);
        drawerListAdapter.addMenu("Logout", R.drawable.logout_white, R.drawable.logout_white);

        lvDrawer.setAdapter(drawerListAdapter);

        lvDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                    drawerLayout.closeDrawer(GravityCompat.START);
                }

                if (position == 0) {
                    changeFragment(FragmentDriverHome.getInstance(jobId), "Home");
                } else if (position == 1) {
                    changeFragment(FragmentTrips.getInstance(), "My Trips");
                } else if (position == 2) {
                    changeFragment(FragmentMyVehicles.getInstance(), "My Vehicle");
                } else if (position == 3) {
                    changeFragment(FragmentDocument.getInstance(), "My Documents");
                } else if (position == 4) {
                    changeFragment(FragmentEditProfile.getInstance(), "Update Profile");
                } /*else if (position == 6) {
                    MyApplication.activityStart(DriverHomeActivity.this, WalletActivity.class, false);
                }*/ else if (position == 5) {
                    showLogOutDialog();
                }
            }
        });

    }

    public void changeFragment(Fragment fragment, String title) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.contentContainer, fragment).addToBackStack("fragback").commit();
        /*transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.contentContainer, fragment);
        transaction.commit();*/

        toolbar.setTitle(title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
           /* int count = getSupportFragmentManager().getBackStackEntryCount();
            if (count>1){
                getSupportFragmentManager().popBackStack("Jobs",
                        FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }else {
                showExitDialog();
            }*/
            // Toast.makeText(DriverHomeActivity.this,count+"",Toast.LENGTH_SHORT).show();
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.contentContainer);
            if (f instanceof FragmentDriverHome) {
                showExitDialog();
            } else if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                //getSupportFragmentManager().popBackStack();
                getJobId();
            } else {
                showExitDialog();
            }

        }
    }

    private void showExitDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(DriverHomeActivity.this)
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.activityFinish(DriverHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }

    private void showLogOutDialog() {
        final AlertDialog dialog = new AlertDialog.Builder(DriverHomeActivity.this)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MyApplication.logout(DriverHomeActivity.this);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create();
        dialog.show();
    }
}
