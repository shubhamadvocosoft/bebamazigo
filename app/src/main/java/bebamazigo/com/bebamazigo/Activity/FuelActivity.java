package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.FuelAdapter;
import bebamazigo.com.bebamazigo.Adapter.VehicleAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.FuelModel;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FuelActivity extends AppCompatActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.btn_req_money)
    Button btnReqMoney;
    Gson gson = new Gson();
    ArrayList<FuelModel> fuelList = new ArrayList<>();
    FuelAdapter fuelAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fuel_req);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Fuel Request");

        fuelAdapter = new FuelAdapter(new ArrayList<FuelModel>(), FuelActivity.this);
        recyclerView.setLayoutManager(new GridLayoutManager(FuelActivity.this, 1, GridLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(fuelAdapter);
        getFuelReq();

        btnReqMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFuelReq();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(FuelActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }

    private void getFuelReq() {

        String url = ApiService.FuelReq + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&jobId=" + getIntent().getBundleExtra("bundle").getString("jobId");
        ApiService.getInstance(FuelActivity.this).makeGetCall(FuelActivity.this, url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                try {
                    if (response.optJSONObject("success") != null) {
                        fuelList = new ArrayList<FuelModel>();
                        JSONArray data = response.optJSONObject("success").optJSONArray("data");
                        for (int i = 0; i < data.length(); i++) {
                            FuelModel model = null;
                            try {
                                model = gson.fromJson(data.getJSONObject(i).toString(), FuelModel.class);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            fuelList.add(model);
                        }
                        fuelAdapter.setData(fuelList);
                    }

                } catch (Exception e) {

                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


    private void showFuelReq() {
        LayoutInflater inflater = LayoutInflater.from(this);
        final View yourCustomView = inflater.inflate(R.layout.dialog_fuel_money, null);
        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(yourCustomView)
                .create();

        final EditText edit_mob = (EditText) yourCustomView.findViewById(R.id.edit_mob);
        final EditText edit_amount = (EditText) yourCustomView.findViewById(R.id.edit_amount);
        TextView txt_req = (TextView) yourCustomView.findViewById(R.id.txt_req);
        edit_mob.setText(MyApplication.getSharedPrefString(StaticData.SP_MOBILE));

        txt_req.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edit_amount.getText().toString().isEmpty()){
                    MyApplication.showToast(FuelActivity.this,"please enter amount");
                }else {
                    apiFuelReq(edit_mob.getText().toString(),edit_amount.getText().toString());
                }

            }
        });


        dialog.show();
    }

    private void apiFuelReq(String mob,String amount) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", getIntent().getBundleExtra("bundle").getString("jobId"));
            param.put("mobileNumber", mob);
            param.put("amount", Integer.parseInt(amount));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(FuelActivity.this).makePostCallAuth(FuelActivity.this, ApiService.MakeFuelReq, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(FuelActivity.this, "Fuel Requested successfully!!!");

                finish();


            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}
