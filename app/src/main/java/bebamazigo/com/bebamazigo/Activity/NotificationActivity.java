package bebamazigo.com.bebamazigo.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.NotificationAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.NotificationModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationActivity extends AppCompatActivity {

    Gson gson = new Gson();

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerRequest)
    RecyclerView recyclerRequest;
    @BindView(R.id.tvNoNotification)
    TextView tvNoNotification;

    private NotificationAdapter requestAdapter;
    private ArrayList<NotificationModel> list;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            MyApplication.activityFinish(NotificationActivity.this);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle("Notification");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notification");

        requestAdapter = new NotificationAdapter(new ArrayList<NotificationModel>(), NotificationActivity.this);
        recyclerRequest.setLayoutManager(new GridLayoutManager(NotificationActivity.this, 1, GridLayoutManager.VERTICAL, false));
        recyclerRequest.setAdapter(requestAdapter);

        getRequest();
    }

    private void getRequest() {
        //ApiService.GET_REQUEST
        ApiService.getInstance(null).makeGetCall(null, ApiService.GET_Notification + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {

                if (response.optJSONObject("success") != null) {
                    list = new ArrayList<NotificationModel>();
                    JSONArray data = response.optJSONObject("success").optJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        NotificationModel model = null;
                        try {
                            model = gson.fromJson(data.getJSONObject(i).toString(), NotificationModel.class);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        list.add(model);
                    }
                    if (list.isEmpty()) {
                        recyclerRequest.setVisibility(View.GONE);
                        tvNoNotification.setVisibility(View.VISIBLE);
                    } else {
                        recyclerRequest.setVisibility(View.VISIBLE);
                        tvNoNotification.setVisibility(View.GONE);
                        requestAdapter.setData(list);
                    }
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

}
