package bebamazigo.com.bebamazigo.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.ReceiptAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.ReceiptModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;

public class FragmentReceipt extends Fragment {

    static FragmentReceipt fragmentTrips;
    private View view;
    private RecyclerView recyclerView;
    private TextView nodata;
    private Gson gson = new Gson();
    private ArrayList<ReceiptModel> jobList = new ArrayList<>();
    private ReceiptAdapter walletAdapter;

    public static synchronized FragmentReceipt getInstance() {
        if (fragmentTrips == null) {
            fragmentTrips = new FragmentReceipt();
        }
        return fragmentTrips;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.notification);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.receipt_fragment, container, false);
        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        nodata = (TextView) view.findViewById(R.id.txt_no_data);

        walletAdapter = new ReceiptAdapter(jobList, getActivity());
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(walletAdapter);
        getTransaction();
        return view;
    }

    private void getTransaction() {
        Log.d("auth", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), ApiService.GET_Recipt +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                if (response.optJSONObject("success") != null) {
                    JSONArray data = response.optJSONObject("success").optJSONArray("data");
                    if (data.length() == 0) {
                        jobList.clear();
                        recyclerView.setVisibility(View.GONE);
                        nodata.setVisibility(View.VISIBLE);
                    }
                    jobList.clear();
                    for (int i = 0; i < data.length(); i++) {
                        ReceiptModel model = null;
                        try {
                            model = gson.fromJson(data.getJSONObject(i).toString(), ReceiptModel.class);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        jobList.add(model);
                    }

                    walletAdapter.setData(jobList);

                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


}
