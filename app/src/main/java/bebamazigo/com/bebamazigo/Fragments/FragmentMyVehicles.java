package bebamazigo.com.bebamazigo.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Adapter.MyVehicleAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Advosoft2 on 1/29/2018.
 */

public class FragmentMyVehicles extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    static FragmentMyVehicles fragmentVehicles;
    View view;
    RecyclerView recJob;
    Gson gson = new Gson();
    ArrayList<Vehicle> vehicleList = new ArrayList<>();
    MyVehicleAdapter vehicleAdapter;
    SwipeRefreshLayout swipe;
    ScrollView scroll_view;
    String vehicleId;
    Vehicle model = null;
    TextView txtVehicleNo;

    TextView txtRegNo;
    TextView txtCategory;
    TextView txtType;
    TextView txtModel;
    TextView txtManufacture;
    LinearLayout llManufacture;
    TextView txtAssment;
    LinearLayout llAssesment, model_ll;
    ImageView imgLogBook;
    ImageView imgInst;
    ImageView imgInsurence;
    Button btnRemove;
    Unbinder unbinder;


    public static synchronized FragmentMyVehicles getInstance() {
        if (fragmentVehicles == null) {
            fragmentVehicles = new FragmentMyVehicles();
        }
        return fragmentVehicles;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fre_my_vehicles, container, false);

        recJob = view.findViewById(R.id.rec_job);
        swipe = view.findViewById(R.id.swipe_refresh);
        scroll_view = view.findViewById(R.id.scroll_view);
        txtVehicleNo = view.findViewById(R.id.txt_vehicle_no);
        txtRegNo = view.findViewById(R.id.txt_reg_no);
        txtCategory = view.findViewById(R.id.txt_category);
        txtType = view.findViewById(R.id.txt_type);
        txtModel = view.findViewById(R.id.txt_model);
        txtManufacture = view.findViewById(R.id.txt_manufacture);
        llManufacture = view.findViewById(R.id.ll_manufacture);
        txtAssment = view.findViewById(R.id.txt_assment);
        llAssesment = view.findViewById(R.id.ll_assesment);
        imgLogBook = view.findViewById(R.id.img_log_book);
        imgInst = view.findViewById(R.id.img_inst);
        imgInsurence = view.findViewById(R.id.img_insurence);
        btnRemove = view.findViewById(R.id.btn_remove);
        model_ll = view.findViewById(R.id.model_ll);

        //getVehicles();
        getPeopleInfo();
        btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeDriver();
            }
        });

        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setHasOptionsMenu(true);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_driver, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("list", vehicleList);
            MyApplication.activityStart(getActivity(), HireDriverActivity.class, false, bundle);

        }
        return super.onOptionsItemSelected(item);
    }*/

    private void getVehicles() {
        Log.d("accec_token", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
        ApiService.getInstance(getActivity()).makeGetCall(getContext(), ApiService.GET_VEHICLE_Req
                        + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN),
                new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("responseGetVehicle", response.toString());
                        stopRefresh();
                        if (response.optJSONObject("success") != null) {
                            vehicleList = new ArrayList<Vehicle>();
                            JSONArray data = response.optJSONObject("success").optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                Vehicle model = null;
                                try {
                                    model = gson.fromJson(data.getJSONObject(i).toString(), Vehicle.class);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                vehicleList.add(model);
                            }
                            vehicleAdapter.setData(vehicleList);
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        stopRefresh();
                    }
                });
    }

    private void getVehiclesDetail() {
        Log.d("accec_token", MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN));
        String url = ApiService.Get_VehicalById + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&vehicleId=" + vehicleId;
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("responseVehicleDetail", response.toString());

                model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), Vehicle.class);

                vehicleId = response.optJSONObject("success").optJSONObject("data").optJSONObject("driver").optString("vehicleId");

                setDetail();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }


    private void getPeopleInfo() {

        String url = ApiService.PeopleInfo + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&peopleId=" + MyApplication.getSharedPrefString(StaticData.SP_ID);
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("responseGetPeopleInfo", response.toString());
                try {
                    JSONObject data = response.getJSONObject("success").getJSONObject("data");
                    if (data.optBoolean("isVehicleAlloted")) {
                        scroll_view.setVisibility(View.VISIBLE);
                        swipe.setVisibility(View.GONE);
                        vehicleId = data.optString("vehicleId");
                        getVehiclesDetail();
                    } else {
                        scroll_view.setVisibility(View.GONE);
                        swipe.setVisibility(View.VISIBLE);

                        vehicleAdapter = new MyVehicleAdapter(new ArrayList<Vehicle>(), getActivity());
                        recJob.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
                        recJob.setAdapter(vehicleAdapter);
                        swipe.setOnRefreshListener(FragmentMyVehicles.this);

                        getVehicles();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setDetail() {
        try {
            txtVehicleNo.setText(model.getVehicleNumber());
            txtRegNo.setText(model.getRegistrationNumber());
            txtType.setText(model.getType());
            txtCategory.setText(model.getVehicleType().getType());
            txtModel.setText(model.getVehicleModel());
            txtManufacture.setText(MyApplication.utcToDate(model.getVehicleMake()));

            if (model.getVehicleType().getType().compareTo("Vehicle") == 0) {
                llManufacture.setVisibility(View.GONE);
            }

            if (model.getAssessmentEnd() == null) {
                llAssesment.setVisibility(View.GONE);
            } else {
                txtAssment.setText(MyApplication.utcToDate(model.getAssessmentEnd()));
            }

            if (model.getVehicleModel() == null) {
                model_ll.setVisibility(View.GONE);
            } else {
                model_ll.setVisibility(View.VISIBLE);
            }

            if (model.getLogBook().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getLogBook().get(0)).into(imgLogBook);
            if (model.getNtsaIns().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getNtsaIns().get(0)).into(imgInst);
            if (model.getInsuranceCopy().size() != 0)
                Picasso.get().load(ApiService.BASE_URL_IMAGE + model.getInsuranceCopy().get(0)).into(imgInsurence);
        } catch (Exception e) {
            Log.d("exception...", e.toString());
        }


    }

    private void removeDriver() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driverId", model.getDriverId());
            jsonObject.put("vehicleId", vehicleId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("param", "vehicleId ->" + vehicleId + "driverId ->" + model.getDriverId());

        Log.d("removeVehicleParam", jsonObject.toString());
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.Remove_DRIVER, jsonObject, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response", response.toString());
                MyApplication.showToast(getActivity(), "Remove Successful !!!");

                getPeopleInfo();

            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void stopRefresh() {
        if (swipe.isRefreshing()) {
            swipe.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onRefresh() {
        getVehicles();
    }
}
