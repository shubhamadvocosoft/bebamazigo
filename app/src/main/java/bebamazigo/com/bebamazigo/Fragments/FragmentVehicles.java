package bebamazigo.com.bebamazigo.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Activity.AddVehicleMachineryActivity;
import bebamazigo.com.bebamazigo.Adapter.VehicleAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.Vehicle;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;

/**
 * Created by Advosoft2 on 1/29/2018.
 */

public class FragmentVehicles extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    static FragmentVehicles fragmentVehicles;
    View view;
    RecyclerView recJob;
    Gson gson = new Gson();
    ArrayList<Vehicle> vehicleList = new ArrayList<>();
    VehicleAdapter vehicleAdapter;
    SwipeRefreshLayout swipe;
    FloatingActionButton fab;

    public static synchronized FragmentVehicles getInstance() {
        if (fragmentVehicles == null) {
            fragmentVehicles = new FragmentVehicles();
        }
        return fragmentVehicles;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fre_vehicles, container, false);
        recJob = view.findViewById(R.id.rec_job);
        swipe = view.findViewById(R.id.swipe_refresh);
        fab = view.findViewById(R.id.fab);
        vehicleAdapter = new VehicleAdapter(new ArrayList<Vehicle>(), getActivity());
        recJob.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
        recJob.setAdapter(vehicleAdapter);
        swipe.setOnRefreshListener(FragmentVehicles.this);

        getVehicles();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyApplication.activityStart(getActivity(), AddVehicleMachineryActivity.class, false);
            }
        });


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setHasOptionsMenu(true);
    }

   /* @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.search_driver, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {

            Bundle bundle = new Bundle();
            bundle.putSerializable("list", vehicleList);
            MyApplication.activityStart(getActivity(), HireDriverActivity.class, false, bundle);

        }
        return super.onOptionsItemSelected(item);
    }*/

    private void getVehicles() {
        ApiService.getInstance(getActivity()).makeGetCall(getContext(), ApiService.GET_MY_VEHICLE
                        + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN),
                new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("Response", response.toString());
                        stopRefresh();
                        if (response.optJSONObject("success") != null) {
                            vehicleList = new ArrayList<Vehicle>();
                            JSONArray data = response.optJSONObject("success").optJSONArray("data");
                            for (int i = 0; i < data.length(); i++) {
                                Vehicle model = null;
                                try {
                                    model = gson.fromJson(data.getJSONObject(i).toString(), Vehicle.class);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                vehicleList.add(model);
                            }
                            vehicleAdapter.setData(vehicleList);
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        stopRefresh();
                    }
                });
    }

    private void stopRefresh() {
        if (swipe.isRefreshing()) {
            swipe.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        getVehicles();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
//            getVehicles();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getVehicles();
    }

}
