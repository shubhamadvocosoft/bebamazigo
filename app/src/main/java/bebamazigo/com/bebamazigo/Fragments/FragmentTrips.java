package bebamazigo.com.bebamazigo.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bebamazigo.com.bebamazigo.Activity.CustomerHomeActivity;
import bebamazigo.com.bebamazigo.Adapter.MultiViewTypeAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;

/**
 * Created by Advosoft2 on 1/29/2018.
 */

public class FragmentTrips extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    static FragmentTrips fragmentTrips;
    private View view;
    private RecyclerView recJob;
    private Gson gson = new Gson();
    private ArrayList<TripModel> jobList = new ArrayList<>();
    private MultiViewTypeAdapter tripAdapter;
    private SwipeRefreshLayout swipe;
    private TextView noData;
    private FloatingActionMenu FloatingActionMenu1;
    private FloatingActionButton on_demand;
    private FloatingActionButton standrad;

    public static synchronized FragmentTrips getInstance() {
        if (fragmentTrips == null) {
            fragmentTrips = new FragmentTrips();
        }
        return fragmentTrips;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.notification);
        if (item != null)
            item.setVisible(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fre_trips, container, false);
        recJob = view.findViewById(R.id.rec_job);
        swipe = view.findViewById(R.id.swipe_refresh);
        noData = view.findViewById(R.id.txt_no_data);
        FloatingActionMenu1 = view.findViewById(R.id.FloatingActionMenu1);
        on_demand = view.findViewById(R.id.on_demand);
        standrad = view.findViewById(R.id.standrad);
        tripAdapter = new MultiViewTypeAdapter(new ArrayList<TripModel>(), getActivity());
        recJob.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));

        recJob.setAdapter(tripAdapter);

        view.findViewById(R.id.ll_near_job).setVisibility(View.GONE);

        swipe.setOnRefreshListener(FragmentTrips.this);

        if (MyApplication.getSharedPrefString(StaticData.SP_REALM).compareToIgnoreCase("customer") == 0) {
            FloatingActionMenu1.setVisibility(View.VISIBLE);
        } else {
            FloatingActionMenu1.setVisibility(View.GONE);
        }

        on_demand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerHomeActivity.getmInstance().changeFragment(FragmentAddJob.getInstance(1), "Add OnDemand Job");
            }
        });
        standrad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerHomeActivity.getmInstance().changeFragment(FragmentAddJob.getInstance(0), "Add Standard Job");
            }
        });

        getTrips();

        return view;
    }

    private void getTrips() {

        Log.d("get_url", ApiService.GET_JOB
                + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN)
                + "&realm=" +
                MyApplication.getSharedPrefString(StaticData.SP_REALM));

        ApiService.getInstance(getActivity()).makeGetCall(getContext(), ApiService.GET_JOB
                        + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN)
                        + "&realm=" +
                        MyApplication.getSharedPrefString(StaticData.SP_REALM),
                new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("response", response.toString());
                        stopRefresh();
                        if (response.optJSONObject("success") != null) {
                            jobList = new ArrayList<>();
                            JSONArray data = response.optJSONObject("success").optJSONArray("data");
                            if (data.length() == 0) {
                                swipe.setVisibility(View.GONE);

                                noData.setVisibility(View.VISIBLE);
                            }
                            for (int i = 0; i < data.length(); i++) {
                                TripModel model = null;
                                try {
                                    model = gson.fromJson(data.getJSONObject(i).toString(), TripModel.class);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                jobList.add(model);
                            }
                            tripAdapter.setData(jobList);
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {
                        stopRefresh();
                    }
                });
    }


    private void stopRefresh() {
        if (swipe.isRefreshing()) {
            swipe.setRefreshing(false);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        getTrips();
    }
}
