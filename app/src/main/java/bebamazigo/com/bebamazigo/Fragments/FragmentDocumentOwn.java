package bebamazigo.com.bebamazigo.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Advosoft2 on 3/5/2018.
 */

public class FragmentDocumentOwn extends Fragment {
    static FragmentDocumentOwn fragmentDocument;
    View view;
    @BindView(R.id.img_log_book)
    ImageView imgLogBook;
    @BindView(R.id.img_pin)
    ImageView imgPin;
    @BindView(R.id.img_certificate)
    ImageView imgCertificate;

    public static synchronized FragmentDocumentOwn getInstance() {
        fragmentDocument = new FragmentDocumentOwn();
        return fragmentDocument;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fre_document_own, container, false);
        ButterKnife.bind(this, view);
        getJobId();
        return view;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void getJobId() {
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), ApiService.GET_INFO +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response_info", response.toString());
                if (response.optJSONObject("success") != null) {
                    try {
                        JSONObject data = response.optJSONObject("success").getJSONObject("data");
                        if (data.getJSONArray("logBook").length() != 0)
                            Picasso.get().load(ApiService.BASE_URL_IMAGE + data.getJSONArray("logBook").getString(0)).into(imgLogBook);
                        if (data.getJSONArray("copyOfPin").length() != 0)
                            Picasso.get().load(ApiService.BASE_URL_IMAGE + data.getJSONArray("copyOfPin").getString(0)).into(imgPin);
                        if (data.getJSONArray("companyCertificate").length() != 0)
                            Picasso.get().load(ApiService.BASE_URL_IMAGE + data.getJSONArray("companyCertificate").getString(0)).into(imgCertificate);
                    } catch (Exception e) {

                    }
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }
}