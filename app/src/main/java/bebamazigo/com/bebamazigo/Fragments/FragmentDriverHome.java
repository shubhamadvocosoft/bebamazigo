package bebamazigo.com.bebamazigo.Fragments;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;

import bebamazigo.com.bebamazigo.Activity.FuelActivity;
import bebamazigo.com.bebamazigo.Activity.TripEndActivity;
import bebamazigo.com.bebamazigo.Activity.VehicleJobDetail;
import bebamazigo.com.bebamazigo.Adapter.NearTripAdapter;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.CircularImageView;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.FirebaseLocation;
import bebamazigo.com.bebamazigo.Model.TripModel;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.DirectionsJSONParser;
import bebamazigo.com.bebamazigo.util.ParserTask;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import in.shadowfax.proswipebutton.ProSwipeButton;

public class FragmentDriverHome extends Fragment implements OnMapReadyCallback,
        GoogleMap.OnMapLoadedCallback, ParserTask.ParserCallback {

    private static FragmentDriverHome fragmentDriverHome;

    @BindView(R.id.swipe_start)
    ProSwipeButton swipeStart;
    @BindView(R.id.ll_trip)
    LinearLayout ll_trip;
    @BindView(R.id.swipe_end)
    ProSwipeButton swipeEnd;
    @BindView(R.id.txt_job_detail)
    TextView txt_job_detail;
    @BindView(R.id.txt_cancel_job)
    TextView txt_cancel_job;

    @BindView(R.id.relativeLayout1)
    RelativeLayout relativeLayout1;
    @BindView(R.id.rec_near_job)
    RecyclerView recNearJob;

    @BindView(R.id.txt_status)
    TextView txtStatus;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_machinary_type)
    TextView txtMachinaryType;
    @BindView(R.id.txt_job_loc)
    TextView txtJobLoc;
    @BindView(R.id.txt_start_date)
    TextView txtStartDate;
    @BindView(R.id.txt_end_date)
    TextView txtEndDate;
    @BindView(R.id.txt_budget)
    TextView txtBudget;
    @BindView(R.id.txt_discription)
    TextView txtDiscription;

    @BindView(R.id.ll_machinery)
    LinearLayout llMachinery;

    @BindView(R.id.iv_cust_image_machine)
    CircleImageView ivCustImageMachine;
    @BindView(R.id.tv_cust_name_machine)
    TextView tvCustNameMachine;
    @BindView(R.id.tv_cust_rating_machine)
    TextView tvCustRatingMachine;
    @BindView(R.id.rating_bar_cust_machine)
    RatingBar ratingBarCustMachine;
    @BindView(R.id.tv_cust_number_machine)
    TextView tvCustNumberMachine;

    @BindView(R.id.btn_complete)
    Button btnComplete;
    @BindView(R.id.btn_cancel)
    Button btnCancel;
    @BindView(R.id.txt_fuel)
    TextView txtFuel;

    Polyline polyline;
    int counter = 2;
    Marker sourceMarker, carMarker;
    Gson gson = new Gson();
    String jobId;
    Timer timer;
    List<HashMap<String, String>> routePoints;
    int test = 0;
    ViewGroup view;
    ArrayList<TripModel> nearJobList = new ArrayList<>();
    NearTripAdapter nearJobAdapter;

    TripModel model;
    ArrayList markerPoints = new ArrayList();
    LatLng source = new LatLng(0, 0);
    LatLng destination = new LatLng(0, 0);
    String sourceAddress, destinationAddress;
    private GoogleMap mMap;

    BroadcastReceiver locationUpdated = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            Location newLocation = intent.getParcelableExtra("location");

            String lat = String.valueOf(newLocation.getLatitude());
            String lng = String.valueOf(newLocation.getLongitude());
            if (!MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT).equalsIgnoreCase(lat) ||
                    !MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG).equalsIgnoreCase(lng)) {

                Location location = new Location("");
                location.setLatitude(Double.parseDouble(lat));
                location.setLongitude(Double.parseDouble(lng));

                if (TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT))) {
                    location.setBearing(bearingBetweenLocations(new LatLng(model.getSource().getLocation().getLat(), (model.getSource().getLocation().getLng())),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                } else {
                    location.setBearing(bearingBetweenLocations(new LatLng(Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT)), Double.parseDouble(MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG))),
                            new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                }
                animateMarker(location, carMarker);
                MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LAT, lat);
                MyApplication.setSharedPrefString(StaticData.SP_CURRENT_LONG, lng);
            }
        }
    };
    private int mInterval = 1000; // 5 seconds by default, can be changed later
    private Handler mHandler;
    private BottomSheetBehavior bottomSheetBehavior;

    public static synchronized FragmentDriverHome getInstance(String jobId) {
        fragmentDriverHome = new FragmentDriverHome();
        Bundle bundle = new Bundle();
        bundle.putString("jobId", jobId);
        fragmentDriverHome.setArguments(bundle);

        return fragmentDriverHome;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = (ViewGroup) inflater.inflate(R.layout.fre_driver_home, container, false);
        ButterKnife.bind(this, view);

        swipeStart.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                showOTPDialog("vehicle");
            }
        });

        swipeEnd.setOnSwipeListener(new ProSwipeButton.OnSwipeListener() {
            @Override
            public void onSwipeConfirm() {
                endTrip();
            }
        });

        jobId = getArguments().getString("jobId", "");

        if (!TextUtils.isEmpty(jobId) || jobId != "") {
            recNearJob.setVisibility(View.GONE);
            relativeLayout1.setVisibility(View.VISIBLE);
            getJob();
        } else {
            setUpUi();
        }

        btnComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnComplete.getText().toString().trim().compareTo("End Job") == 0) {
                    jobEnd();
                } else {
                    showOTPDialog("machinary");
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jobCancel();
            }
        });
        txtFuel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("jobId", jobId);
                MyApplication.activityStart(getActivity(), FuelActivity.class, false, bundle);
            }
        });
        txt_job_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(jobId)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("jobId", jobId);
                    MyApplication.activityStart(getActivity(), VehicleJobDetail.class, false, bundle);
                }
            }
        });
        txt_cancel_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                jobCancel();
            }
        });
        return view;
    }

    private void setUpUi() {
        recNearJob.setVisibility(View.VISIBLE);
        relativeLayout1.setVisibility(View.GONE);
        llMachinery.setVisibility(View.GONE);
        txtFuel.setVisibility(View.GONE);
        nearJobAdapter = new NearTripAdapter(new ArrayList<TripModel>(), getActivity());
        recNearJob.setLayoutManager(new GridLayoutManager(getActivity(), 1, GridLayoutManager.VERTICAL, false));
        recNearJob.setAdapter(nearJobAdapter);
        updateDriverLocation();
    }

    private void getJob() {
        String url = ApiService.GET_JOB_ID + "&jobId=" + jobId;
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response_job", response.toString());
                if (response.optJSONObject("success").optJSONObject("data") != null) {
                    model = gson.fromJson(response.optJSONObject("success").optJSONObject("data").toString(), TripModel.class);

                    source = new LatLng(model.getSource().getLocation().getLat(), model.getSource().getLocation().getLng());
                    sourceAddress = model.getSource().getAddress();
                    if (model.getVehicalType().getType().compareTo("Vehicle") == 0) {
                        destination = new LatLng(model.getDestination().getLocation().getLat(), model.getDestination().getLocation().getLng());
                        destinationAddress = model.getDestination().getAddress();

                        if (model.getStatus().equals("accepted")) {
                            ll_trip.setVisibility(View.VISIBLE);
                            swipeStart.setVisibility(View.VISIBLE);
                            txtFuel.setVisibility(View.GONE);
                        } else if (model.getStatus().equals("on trip")) {
                            ll_trip.setVisibility(View.VISIBLE);
                            swipeEnd.setVisibility(View.VISIBLE);
                            txtFuel.setVisibility(View.VISIBLE);
                        } else if (model.getStatus().equals("end")) {
                            if (!model.getDidDriveGiveRate()) {
                                txtFuel.setVisibility(View.GONE);
                                Bundle bundle = new Bundle();
                                bundle.putString("fare", model.getNetPrice().toString());
                                bundle.putString("source", model.getSource().getAddress());
                                bundle.putString("destination", model.getDestination().getAddress());
                                bundle.putString("jobId", jobId);
                                bundle.putSerializable("data", model);
                                MyApplication.activityStart(getActivity(), TripEndActivity.class, false, bundle);
                            } else {
                                setUpUi();
                            }
                        }

                        setMap();

                        getRoute();

                    } else {
                        relativeLayout1.setVisibility(View.GONE);
                        recNearJob.setVisibility(View.GONE);
                        llMachinery.setVisibility(View.VISIBLE);
                        txtStatus.setText(model.getStatus());
                        txtTitle.setText(model.getTitle());
                        txtMachinaryType.setText(model.getTypeOfVehicle());
                        txtBudget.setText("KSH " + model.getNetPrice());
                        txtJobLoc.setText(model.getSource().getAddress());
                        txtStartDate.setText(MyApplication.utcToDate(model.getDate()) + "," + MyApplication.utcToTime(model.getDate()));
                        txtEndDate.setText(MyApplication.utcToDate(model.getEndDate()) + "," + MyApplication.utcToTime(model.getEndDate()));
                        txtDiscription.setText(model.getDescription());

                        try {
                            Glide.with(FragmentDriverHome.this)
                                    .load(ApiService.BASE_URL_IMAGE + model.getCreator().getProfileImage())
                                    .centerCrop()
                                    .into(ivCustImageMachine);
                            tvCustNameMachine.setText("Name - " + model.getCreator().getName());
                            tvCustNumberMachine.setText("Mobile - " + model.getCreator().getMobile());
                            tvCustRatingMachine.setText("Rating - " + String.valueOf(new DecimalFormat("##.##").format(response.optJSONObject("success").optJSONObject("data").optJSONObject("creator").optJSONObject("rating").optDouble("avgRating"))));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if (model.getStatus().equals("accepted")) {
                            btnComplete.setText("Start Job");
                        } else if (model.getStatus().equals("on trip")) {
                            txtFuel.setVisibility(View.VISIBLE);
                            btnComplete.setText("End Job");
                        } else {
                            btnComplete.setText("End Job");
                            setUpUi();
                        }
                    }

                } else {
                    setUpUi();
                }
            }

            @Override
            public void onError(VolleyError volleyError) {
                setUpUi();
            }
        });
    }

    private void updateDriverLocation() {
        JSONObject param = new JSONObject();
        JSONObject locationObject = new JSONObject();
        SharedPreferences sp = getActivity().getSharedPreferences("RS_PREF", 0);
        Log.d("data_value", sp.getString(StaticData.SP_NEW_LAT, ""));
        try {
            locationObject.put("lat", MyApplication.getDouble(StaticData.SP_NEW_LAT));
            locationObject.put("lng", MyApplication.getDouble(StaticData.SP_NEW_LONG));
            param.put("location", locationObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiService.getInstance().makePostCallAuth(getActivity(), ApiService.UPDATE_DRIVER_LOCATION, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                getNearByJob();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void getNearByJob() {
        JSONObject position = new JSONObject();
        try {
            position.put("lat", MyApplication.getDouble(StaticData.SP_NEW_LAT));
            position.put("lng", MyApplication.getDouble(StaticData.SP_NEW_LONG));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String uri = Uri.parse(ApiService.GET_NEARBY_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN))
                .buildUpon()
                .appendQueryParameter("location", position.toString())
                .build().toString();

        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), uri, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response_near", response.toString());
                if (response.optJSONObject("success") != null) {
                    nearJobList = new ArrayList<TripModel>();
                    JSONArray data = response.optJSONObject("success").optJSONArray("data");
                    for (int i = 0; i < data.length(); i++) {
                        TripModel model = null;
                        try {
                            model = gson.fromJson(data.getJSONObject(i).toString(), TripModel.class);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        nearJobList.add(model);
                    }
                    if (nearJobList.size() > 0) {
                        nearJobAdapter.setData(nearJobList);
                    }

                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();
        mapFragment.getMapAsync(FragmentDriverHome.this);
    }

    private void getRoute() {
        String url = MyApplication.getDirectionsUrl(source, destination);

        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ParserTask parserTask = new ParserTask(FragmentDriverHome.this);
                parserTask.execute(response.toString());
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));

        if (markerPoints.size() > 1) {
            markerPoints.clear();
            mMap.clear();
        }

        // Adding new item to the ArrayList
        markerPoints.add(source);
        markerPoints.add(destination);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(source);
        options.position(destination);

        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }

        sourceMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                .title(sourceAddress));
        carMarker = mMap.addMarker(new MarkerOptions().position(source)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_truck))
                .title(sourceAddress));

        mMap.addMarker(new MarkerOptions().position(destination)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                .title(destinationAddress));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 18.0f));
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(locationUpdated, new IntentFilter("locationUpdated"));
    }

    private void showRoute(List<HashMap<String, String>> data) {

        if (polyline != null) {
            polyline.remove();
        }

        ArrayList points = null;
        PolylineOptions lineOptions = null;
        MarkerOptions markerOptions = new MarkerOptions();
        //LatLngBounds.Builder builder = new LatLngBounds.Builder();
        points = new ArrayList();
        lineOptions = new PolylineOptions();

        for (int j = 0; j < data.size(); j++) {
            HashMap<String, String> point = data.get(j);

            double lat = Double.parseDouble(point.get("lat").toString());
            double lng = Double.parseDouble(point.get("lng").toString());

            LatLng position = new LatLng(lat, lng);

            points.add(position);

            //builder.include(position);

        }

        lineOptions.addAll(points);
        lineOptions.width(12);
        lineOptions.color(getResources().getColor(R.color.orange_dark));
        lineOptions.geodesic(true);
        // Drawing polyline in the Google Map for the i-th route
        polyline = mMap.addPolyline(lineOptions);
        //LatLngBounds bounds = builder.build();

        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.12);


        //CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        //mMap.animateCamera(cu);
    }

    @Override
    public void onMapLoaded() {
    }

    private float bearingBetweenLocations(LatLng latLng1, LatLng latLng2) {

        double PI = 3.14159;
        double lat1 = latLng1.latitude * PI / 180;
        double long1 = latLng1.longitude * PI / 180;
        double lat2 = latLng2.latitude * PI / 180;
        double long2 = latLng2.longitude * PI / 180;

        double dLon = (long2 - long1);

        double y = Math.sin(dLon) * Math.cos(lat2);
        double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1)
                * Math.cos(lat2) * Math.cos(dLon);

        Double brng = Math.atan2(y, x);

        brng = Math.toDegrees(brng);
        brng = (brng + 360) % 360;

        return brng.floatValue();
    }

    void animateMarker(final Location destination, final Marker marker) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = new LatLng(destination.getLatitude(), destination.getLongitude());

            final float startRotation = marker.getRotation();

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.LinearFixed();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 0.5f);
            valueAnimator.setDuration(1000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    try {
                        float v = animation.getAnimatedFraction();
                        LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                        marker.setPosition(newPosition);
                        marker.setRotation(computeRotation(v, startRotation, destination.getBearing()));
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            });
            valueAnimator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(marker.getPosition(), 18.0f));
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });

            valueAnimator.start();
            counter++;
        }
    }

    float computeRotation(float fraction, float start, float end) {
        float normalizeEnd = end - start; // rotate start to 0
        float normalizedEndAbs = (normalizeEnd + 360) % 360;

        float direction = (normalizedEndAbs > 180) ? -1 : 1; // -1 = anticlockwise, 1 = clockwise
        float rotation;
        if (direction > 0) {
            rotation = normalizedEndAbs;
        } else {
            rotation = normalizedEndAbs - 360;
        }

        float result = fraction * rotation + start;
        return (result + 360) % 360;
    }


    @Override
    public void onParserResult(List<List<HashMap<String, String>>> result, DirectionsJSONParser parser) {
        for (int i = 0; i < parser.getRouteList().size(); i++) {
            if (parser.getRouteList().get(i).getDistance().equalsIgnoreCase(model.getDistance().getText())) {
                routePoints = result.get(i);
                showRoute(routePoints);
            }
        }
    }

    @Override
    public void onDestroyView() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(locationUpdated);
        super.onDestroyView();
    }

    private void showOTPDialog(final String type) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View yourCustomView = inflater.inflate(R.layout.dialog_otp, null);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setView(yourCustomView)
                .setCancelable(false)
                .create();
        final EditText edtOTP = (EditText) yourCustomView.findViewById(R.id.edt_otp);
        final TextView txtSubmit = (TextView) yourCustomView.findViewById(R.id.txt_submit);
        final TextView txtCancel = (TextView) yourCustomView.findViewById(R.id.txt_cancel);
        txtSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtOTP.getText().toString().equals(model.getJobOtp().toString())) {
                    if (type.compareTo("vehicle") == 0) {
                        startTrip(model.getJobOtp());
                    } else {
                        jobComplete(model.getJobOtp());
                    }
                    dialog.dismiss();
                } else {
                    MyApplication.showToast(getActivity(), "OTP Does Not Match");
                }
            }
        });
        txtCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeStart.showResultIcon(false);
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void endTrip() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiService.getInstance(getActivity()).makePostCallAuth(getContext(), ApiService.END_JOB, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Bundle bundle = new Bundle();
                bundle.putString("fare", model.getNetPrice().toString());
                bundle.putString("source", model.getSource().getAddress());
                bundle.putString("destination", model.getDestination().getAddress());
                bundle.putString("jobId", jobId);
                bundle.putSerializable("data", model);
                MyApplication.activityStart(getActivity(), TripEndActivity.class, true, bundle);
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void startTrip(final int jobOtp) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("jobOtp", jobOtp);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ApiService.getInstance(getActivity()).makePostCallAuth(getContext(), ApiService.START_JOB, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                ll_trip.setVisibility(View.VISIBLE);
                swipeStart.setVisibility(View.GONE);
                swipeEnd.setVisibility(View.VISIBLE);
                txtFuel.setVisibility(View.VISIBLE);

                MyApplication.setSharedPrefString(StaticData.SP_CURRENT_JOB, jobId);

                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference("Locations");
                FirebaseLocation firebaseLocation = new FirebaseLocation(jobId,
                        MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LAT),
                        MyApplication.getSharedPrefString(StaticData.SP_CURRENT_LONG),
                        "running",
                        MyApplication.getSharedPrefString(StaticData.SP_USER_ID),
                        System.currentTimeMillis()
                );
                mDatabase.child(jobId).setValue(firebaseLocation);
            }

            @Override
            public void onError(VolleyError volleyError) {
                ll_trip.setVisibility(View.VISIBLE);
                swipeEnd.setVisibility(View.GONE);
                swipeStart.setVisibility(View.VISIBLE);
            }
        });
    }

    public double distance(double lat_a, double lng_a, double lat_b, double lng_b) {
        Location startPoint = new Location("locationA");
        startPoint.setLatitude(lat_a);
        startPoint.setLongitude(lng_a);

        Location endPoint = new Location("locationA");
        endPoint.setLatitude(lat_b);
        endPoint.setLongitude(lng_b);

        double distanceResult = startPoint.distanceTo(endPoint);
        return distanceResult;
    }

    private void jobComplete(int otp) {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
            param.put("jobOtp", otp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.START_JOB, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(getActivity(), "Job Started!!!");
                btnComplete.setText("End Job");
                getJob();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void jobEnd() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.END_JOB, param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(getActivity(), "Job Finished !!");
                btnComplete.setText("End Job");
                jobId = "";
                setUpUi();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void jobCancel() {
        JSONObject param = new JSONObject();
        try {
            param.put("jobId", jobId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.CANCEL_JOB + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), param, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                MyApplication.showToast(getActivity(), "Job Cancelled !!");
                jobId = "";
                setUpUi();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private interface LatLngInterpolator {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolator {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

}