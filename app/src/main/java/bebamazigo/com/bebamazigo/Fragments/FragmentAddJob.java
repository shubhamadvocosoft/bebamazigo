package bebamazigo.com.bebamazigo.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.kunzisoft.switchdatetime.SwitchDateTimeDialogFragment;
import com.seatgeek.placesautocomplete.DetailsCallback;
import com.seatgeek.placesautocomplete.OnPlaceSelectedListener;
import com.seatgeek.placesautocomplete.PlacesAutocompleteTextView;
import com.seatgeek.placesautocomplete.model.AutocompleteResultType;
import com.seatgeek.placesautocomplete.model.Place;
import com.seatgeek.placesautocomplete.model.PlaceDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import bebamazigo.com.bebamazigo.Activity.CustomerHomeActivity;
import bebamazigo.com.bebamazigo.Activity.MapsActivity;
import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Custom.HintAdapter;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Model.VehicleType;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FragmentAddJob extends Fragment {

    final static int ACTIVITY_CODE = 101;
    static int type;
    static FragmentAddJob fragmentAddJob;
    String sourceId, destinationId;
    JSONObject sourceJSON, destinationJSON;
    PlaceDetails sourcePlace, destinationPlace;
    ArrayList<VehicleType> vehiclesList = new ArrayList<>();
    ArrayList<VehicleType> machineryList = new ArrayList<>();
    String selectedRoute;
    JSONObject duration = new JSONObject();
    JSONObject distance = new JSONObject();
    String title,
            weight,
            valueOfGoods,
            budget,
            netPrice,
            typeOfVehicle,
            date,
            endDate,
            description,
            natureOfGoods,
            vehicleBodyType,
            typeOfJob;
    int vehicleTonnage;
    @BindView(R.id.txt_)
    TextView txt;
    @BindView(R.id.edt_title)
    EditText edtTitle;
    @BindView(R.id.edt_weight)
    EditText edtWeight;
    @BindView(R.id.spn_job_type)
    Spinner spnJobType;
    @BindView(R.id.spn_vehicle_type)
    Spinner spnVehicleType;
    @BindView(R.id.spn_machinery_type)
    Spinner spnMachineryType;
    @BindView(R.id.spn_weight)
    Spinner spnWeight;
    @BindView(R.id.spn_trailer_type)
    Spinner spnTrailerType;
    @BindView(R.id.ll_vehicle)
    LinearLayout llVehicle;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.edt_description)
    EditText edtDescription;
    @BindView(R.id.places_source)
    PlacesAutocompleteTextView placesSource;
    @BindView(R.id.places_destination)
    PlacesAutocompleteTextView placesDestination;
    @BindView(R.id.edt_nature)
    EditText edtNature;
    @BindView(R.id.edt_value)
    EditText edtValue;
    @BindView(R.id.edt_budget)
    EditText edtBudget;
    @BindView(R.id.edt_net)
    TextView edtNet;
    @BindView(R.id.txt_accept)
    TextView txtAccept;
    @BindView(R.id.txt_find)
    TextView txtFind;
    @BindView(R.id.txt_cancel)
    TextView txtCancel;
    JobAdded callback;
    @BindView(R.id.txt_date2)
    TextView txtDate2;
    private View view;

    public static synchronized FragmentAddJob getInstance(int jobType) {
        fragmentAddJob = new FragmentAddJob();
        type = jobType;
        return fragmentAddJob;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.notification);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_add_job, container, false);
        typeOfJob = type == 0 ? "on demand" : "standard rate";
        ButterKnife.bind(this, view);

        if (type == 0) {
            edtNet.setVisibility(View.GONE);
            edtBudget.setVisibility(View.VISIBLE);
        } else {
            edtNet.setVisibility(View.VISIBLE);
            edtBudget.setVisibility(View.GONE);
        }

        getVehicle(type);

        placesSource.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull final Place place) {
                sourceId = place.place_id;

                placesSource.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {

                        sourcePlace = placeDetails;

                        sourceJSON = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            sourceJSON.put("address", placeDetails.formatted_address);
                            sourceJSON.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });
        placesDestination.setOnPlaceSelectedListener(new OnPlaceSelectedListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {

                placesDestination.getDetailsFor(place, new DetailsCallback() {
                    @Override
                    public void onSuccess(PlaceDetails placeDetails) {

                        destinationPlace = placeDetails;
                        destinationJSON = new JSONObject();
                        try {
                            JSONObject location = new JSONObject();
                            location.put("lat", placeDetails.geometry.location.lat);
                            location.put("lng", placeDetails.geometry.location.lng);
                            destinationJSON.put("address", placeDetails.formatted_address);
                            destinationJSON.put("location", location);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {

                    }
                });
            }
        });
        placesSource.setResultType(AutocompleteResultType.NO_TYPE);
        placesDestination.setResultType(AutocompleteResultType.NO_TYPE);

        Location targetLocation = new Location("");//provider name is unnecessary
        targetLocation.setLatitude(-1.2920659);//your coords of course
        targetLocation.setLongitude(36.82194619999996);


        placesSource.setCurrentLocation(targetLocation);
        placesDestination.setCurrentLocation(targetLocation);

        placesDestination.setLocationBiasEnabled(true);
        placesSource.setLocationBiasEnabled(true);


        placesSource.setOnClearListener(new PlacesAutocompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                sourcePlace = null;
            }
        });
        placesDestination.setOnClearListener(new PlacesAutocompleteTextView.OnClearListener() {
            @Override
            public void onClear() {
                destinationPlace = null;
            }
        });


        spnVehicleType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                } else {
                    changeUI(position - 1, true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spnMachineryType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    spnTrailerType.setVisibility(View.GONE);
                    spnWeight.setVisibility(View.GONE);
                } else {
                    changeUI(position - 1, false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }


    private void changeUI(int position, boolean isVeh) {
        VehicleType vehicleType;
        if (isVeh) {
            vehicleType = vehiclesList.get(position);
            txtDate2.setVisibility(View.GONE);

            placesDestination.setVisibility(View.VISIBLE);
            edtNature.setVisibility(View.VISIBLE);
            edtValue.setVisibility(View.VISIBLE);
            placesDestination.setVisibility(View.VISIBLE);
            txtFind.setVisibility(View.VISIBLE);
            edtWeight.setVisibility(View.VISIBLE);
        } else {
            vehicleType = machineryList.get(position);
            txtDate2.setVisibility(View.VISIBLE);

            placesDestination.setVisibility(View.GONE);
            edtNature.setVisibility(View.GONE);
            edtValue.setVisibility(View.GONE);
            placesDestination.setVisibility(View.GONE);
            txtFind.setVisibility(View.GONE);
            edtWeight.setVisibility(View.GONE);

        }
        if (vehicleType.getBodyType() != null) {
            spnTrailerType.setVisibility(View.VISIBLE);
            spnTrailerType.setAdapter(new HintAdapter(getActivity(), R.layout.item_spinner, vehiclesList.get(position).getBodyType()));
        } else {
            spnTrailerType.setVisibility(View.GONE);
        }

        if (vehicleType.getTon() != null) {
            spnWeight.setVisibility(View.VISIBLE);
            spnWeight.setAdapter(new HintAdapter(getActivity(), R.layout.item_spinner, (vehicleType.getTon())));
        } else {
            spnWeight.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(distance.optString("value"))) {
            getPrice();
        }
    }

    @OnClick({R.id.txt_accept, R.id.txt_find, R.id.txt_cancel, R.id.txt_date, R.id.txt_date2})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_accept:
                if (isValid()) {
                    if (distance.length() == 0) {
                        Toast.makeText(getActivity(), "Please Select a Route First", Toast.LENGTH_SHORT).show();
                    } else {
                        postJob();
                    }
                }
                break;
            case R.id.txt_find:
                if (sourcePlace != null && destinationPlace != null) {
                    Bundle bundle = new Bundle();
                    bundle.putDouble("latS", sourcePlace.geometry.location.lat);
                    bundle.putDouble("lngS", sourcePlace.geometry.location.lng);
                    bundle.putString("addressS", sourcePlace.formatted_address);

                    bundle.putDouble("latD", destinationPlace.geometry.location.lat);
                    bundle.putDouble("lngD", destinationPlace.geometry.location.lng);
                    bundle.putString("addressD", destinationPlace.formatted_address);
                    startActivityForResult(new Intent(getActivity(), MapsActivity.class).putExtra("bundle", bundle), ACTIVITY_CODE);
                    getActivity().overridePendingTransition(R.anim.exit, R.anim.enter);

                } else {
                    MyApplication.showToast(getActivity(), "Enter source & destination");
                }
                break;
            case R.id.txt_cancel:
                MyApplication.activityFinish(getActivity());
                break;
            case R.id.txt_date:
                selectDateTime(txtDate);
                break;
            case R.id.txt_date2:
                selectDateTime(txtDate2);
        }
    }

    private void selectDateTime(final TextView textView) {
        SwitchDateTimeDialogFragment dateTimeFragment = SwitchDateTimeDialogFragment.newInstance(
                "Select Date Time",
                "OK",
                "Cancel"
        );

        // Assign values

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);

        dateTimeFragment.startAtCalendarView();
        dateTimeFragment.set24HoursMode(true);
        dateTimeFragment.setMaximumDateTime(cal.getTime());
        dateTimeFragment.setDefaultDateTime(Calendar.getInstance().getTime());
        dateTimeFragment.setMinimumDateTime(Calendar.getInstance().getTime());


        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            format.setTimeZone(TimeZone.getTimeZone("UTC"));
            dateTimeFragment.setSimpleDateMonthAndDayFormat(format);
        } catch (SwitchDateTimeDialogFragment.SimpleDateMonthAndDayFormatException e) {

        }

        // Set listener
        dateTimeFragment.setOnButtonClickListener(new SwitchDateTimeDialogFragment.OnButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Date selectedDate) {
                long millis = selectedDate.getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

                if (textView == txtDate)
                    date = sdf.format(new Date(millis));
                else
                    endDate = sdf.format(new Date(millis));
                textView.setTextColor(Color.BLACK);
                textView.setText(MyApplication.utcToDate(sdf.format(new Date(millis))));

            }

            @Override
            public void onNegativeButtonClick(Date date) {
                // Date is get on negative button click
            }
        });

        // Show
        dateTimeFragment.show(getChildFragmentManager(), "dialog_time");
    }

    private void postJob() {
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.ADD_JOB,
                getParam(), new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            MyApplication.showToast(getActivity(), response.optJSONObject("success").optJSONObject("msg").optString("replyMessage"));
                            ((CustomerHomeActivity) getActivity()).changeFragment(FragmentTrips.getInstance(), "Home");
                        }
                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private boolean isValid() {
        if (edtTitle.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtTitle.getText().toString().trim())) {
                edtTitle.setError(StaticData.BLANK);
                return false;
            } else {
                title = edtTitle.getText().toString();
            }
        }

        if (edtDescription.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtDescription.getText().toString().trim())) {
                edtDescription.setError(StaticData.BLANK);
                return false;
            } else {
                description = edtDescription.getText().toString();
            }
        }
        if (edtNature.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtNature.getText().toString().trim())) {
                edtNature.setError(StaticData.BLANK);
                return false;
            } else {
                natureOfGoods = edtNature.getText().toString();
            }
        }
        if (edtBudget.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtBudget.getText().toString().trim())) {
                edtBudget.setError(StaticData.BLANK);
                return false;
            } else {
                budget = edtBudget.getText().toString();
            }
        }
        if (type == 1) {
            if (edtNet.getVisibility() != View.GONE) {
                if (TextUtils.isEmpty(edtNet.getText().toString().trim())) {
                    return false;
                } else {
                    netPrice = edtNet.getText().toString();
                }
            }
        }
        if (edtValue.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtValue.getText().toString().trim())) {
                edtValue.setError(StaticData.BLANK);
                return false;
            } else {
                valueOfGoods = edtValue.getText().toString();
            }
        }
        if (edtWeight.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(edtWeight.getText().toString().trim())) {
                edtWeight.setError(StaticData.BLANK);
                return false;
            } else {
                weight = edtWeight.getText().toString();
            }
        }
        if (spnJobType.getVisibility() != View.GONE) {
            if (spnJobType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(getActivity(), "Please Select Vehicle or Machinery Type");
                return false;
            }
        }

        if (spnVehicleType.getVisibility() != View.GONE) {
            if (spnVehicleType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(getActivity(), "Please Select Vehicle Type");
                return false;
            } else {
                typeOfVehicle = spnVehicleType.getSelectedItem().toString();
            }
        }


        if (spnMachineryType.getVisibility() != View.GONE) {
            if (spnMachineryType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(getActivity(), "Please Select Machinery Type");
                return false;
            } else {
                typeOfVehicle = spnMachineryType.getSelectedItem().toString();
            }

        }

        if (spnTrailerType.getVisibility() == View.VISIBLE) {
            if (spnTrailerType.getSelectedItemPosition() == 0) {
                MyApplication.showToast(getActivity(), "Please Select Body Type");
                return false;
            } else {
                vehicleBodyType = spnTrailerType.getSelectedItem().toString();
            }
        }

        if (spnWeight.getVisibility() == View.VISIBLE) {
            if (spnWeight.getSelectedItemPosition() == 0) {
                MyApplication.showToast(getActivity(), "Please Select Tonnage");
                return false;
            } else {
                vehicleTonnage = Integer.parseInt(spnWeight.getSelectedItem().toString());
            }
        }
        if (txtDate.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(date)) {
                MyApplication.showToast(getActivity(), "Please Select Date");
                return false;
            }
        }
        if (txtDate2.getVisibility() != View.GONE) {
            if (TextUtils.isEmpty(endDate)) {
                MyApplication.showToast(getActivity(), "Please Select End Date");
                return false;
            }
        }


        return true;

    }

    private void getVehicle(int type) {
        String url = "";
        if (type == 0) {
            url = ApiService.GET_VEHICLE_TYPE + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&typeOfJob=" + "standard%20rate";
        } else {
            url = ApiService.GET_VEHICLE_TYPE + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) + "&typeOfJob=" + "on%20demand";
        }
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url
                , new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        if (response.optJSONObject("success") != null) {
                            JSONArray vehicle = response.optJSONObject("success").optJSONObject("data").optJSONArray("vehicle");
                            JSONArray machinery = response.optJSONObject("success").optJSONObject("data").optJSONArray("machinery");
                            for (int i = 0; i < vehicle.length(); i++) {
                                vehiclesList.add(new VehicleType(vehicle.optJSONObject(i)));
                            }
                            for (int i = 0; i < machinery.length(); i++) {
                                machineryList.add(new VehicleType(machinery.optJSONObject(i)));
                            }
                        }
                        spnVehicleType.setVisibility(View.GONE);
                        spnMachineryType.setVisibility(View.GONE);

                        List<String> vehMach = new ArrayList<String>();
                        vehMach.add("Vehicle/Machinery");
                        if (vehiclesList.size() > 0) {
                            vehMach.add("Vehicle");
                        }
                        if (machineryList.size() > 0) {
                            vehMach.add("Machinery");
                        }

                        spnJobType.setAdapter(new HintAdapter(getActivity(), R.layout.item_spinner, vehMach.toArray(new String[vehMach.size()])));

                        spnJobType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                if (position == 1) {
                                    spnVehicleType.setVisibility(View.VISIBLE);
                                    spnMachineryType.setVisibility(View.GONE);
                                    spnVehicleType.setAdapter(new HintAdapter(getActivity(), R.layout.item_spinner, getVehicleList()));
                                    changeUI(0, true);
                                } else if (position == 2) {
                                    spnVehicleType.setVisibility(View.GONE);
                                    spnMachineryType.setVisibility(View.VISIBLE);
                                    spnMachineryType.setAdapter(new HintAdapter(getActivity(), R.layout.item_spinner, getMachineList()));
                                    changeUI(0, false);
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });


                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });
    }

    private String[] getVehicleList() {
        List<String> vehicle = new ArrayList<>();
        vehicle.add("Vehicle");

        for (int i = 0; i < vehiclesList.size(); i++) {
            vehicle.add(vehiclesList.get(i).getName());
        }
        return vehicle.toArray(new String[vehicle.size()]);
    }

    private String[] getMachineList() {
        List<String> vehicle = new ArrayList<>();
        vehicle.add("Machine");

        for (int i = 0; i < machineryList.size(); i++) {
            vehicle.add(machineryList.get(i).getName());
        }
        return vehicle.toArray(new String[vehicle.size()]);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_CODE && resultCode == Activity.RESULT_OK) {

            try {
                distance.put("text", StaticData.CURRENT_ROUTE.getDistance());
                distance.put("value", StaticData.CURRENT_ROUTE.getValueDistance());
                duration.put("text", StaticData.CURRENT_ROUTE.getDuration());
                duration.put("value", StaticData.CURRENT_ROUTE.getValueDuration());

                selectedRoute = StaticData.CURRENT_ROUTE.getSelectedRoute();
                if (spnVehicleType.getSelectedItemPosition() != 0) {
                    getPrice();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public JSONObject getParam() {
        JSONObject param = new JSONObject();
        try {
            param.put("title", title);
            param.put("weight", weight);
            if (valueOfGoods != null)
                param.put("valueOfGoods", Integer.parseInt(valueOfGoods));
            param.put("source", sourceJSON);
            param.put("destination", destinationJSON);

            /*if (type == 1) {
                if (netPrice != null)
                    param.put("netPrice", Integer.parseInt(netPrice));
            } else {
                if (budget != null)
                    param.put("netPrice", Integer.parseInt(budget));
                param.put("budget", Integer.parseInt(budget));
            }*/

            if (netPrice != null) {
                param.put("netPrice", Integer.parseInt(netPrice));
                param.put("budget", Integer.parseInt(netPrice));
            } else if (budget != null) {
                param.put("netPrice", Integer.parseInt(budget));
                param.put("budget", Integer.parseInt(budget));
            } else {
                param.put("netPrice", 0);
                param.put("budget", 0);
            }

            param.put("typeOfVehicle", typeOfVehicle);
            param.put("date", date);
            param.put("description", description);
            param.put("natureOfGoods", natureOfGoods);
            param.put("typeOfJob", typeOfJob);
            param.put("duration", duration);
            param.put("distance", distance);
            param.put("selectedRoute", selectedRoute);
            param.put("vehicleTonnage", vehicleTonnage);
            param.put("vehicleBodyType", vehicleBodyType);
            param.put("requiredType", spnJobType.getSelectedItem().toString());
            param.put("endDate", endDate);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return param;
    }

    private void getPrice() {
        String url = ApiService.JOB_PRICING + MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN) +
                "&vehicleName=" + spnVehicleType.getSelectedItem() + "&distanceInMeter=" + distance.optString("value");
        if (spnWeight.getVisibility() == View.VISIBLE) {
            url = url + "&tonnage=" + spnWeight.getSelectedItem();
        }
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), url, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                edtNet.setText(response.optJSONObject("success").optString("data"));
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    public interface JobAdded {
        void onJobAdded();
    }
}
