package bebamazigo.com.bebamazigo.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import bebamazigo.com.bebamazigo.Application.MyApplication;
import bebamazigo.com.bebamazigo.Data.ApiService;
import bebamazigo.com.bebamazigo.Data.DataPart;
import bebamazigo.com.bebamazigo.R;
import bebamazigo.com.bebamazigo.util.MarshMallowPermission;
import bebamazigo.com.bebamazigo.util.StaticData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentEditProfile extends Fragment {

    public static final int PICK_PROFILE_PIC = 7;
    static FragmentEditProfile fragmentEditProfile;
    @BindView(R.id.edt_name)
    EditText edtName;
    @BindView(R.id.edt_company)
    EditText edtCompany;
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.edt_email)
    EditText edtEmail;
    @BindView(R.id.edt_vehicle_type)
    EditText edt_vehicle_type;
    @BindView(R.id.txt_submit)
    TextView txtSubmit;
    @BindView(R.id.img_profile_pic)
    ImageView imgProfilePic;
    @BindView(R.id.img_delete)
    CircleImageView imgDelete;
    @BindView(R.id.user_rating)
    RatingBar userRating;
    String name, email, mobile, companyName, userType;
    Bitmap bmpProfile;
    private View view;

    @BindView(R.id.ll_vehicle_pref)
    LinearLayout ll_vehicle_pref;
    @BindView(R.id.ll_checkbox_main)
    LinearLayout ll_checkbox_main;

    ArrayList<String> stringArray;

    public static synchronized FragmentEditProfile getInstance() {
        fragmentEditProfile = new FragmentEditProfile();
        return fragmentEditProfile;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.notification);
        if (item != null)
            item.setVisible(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_edit_profile, container, false);
        ButterKnife.bind(this, view);
        getInfo();

        return view;
    }


    private void getInfo() {
        ApiService.getInstance(getActivity()).makeGetCall(getActivity(), ApiService.GET_INFO +
                MyApplication.getSharedPrefString(StaticData.SP_ACCESS_TOKEN), new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                Log.d("response_info", response.toString());
                if (response.optJSONObject("success") != null) {
                    setUpUi(response.optJSONObject("success").optJSONObject("data"));
                }
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

    private void setUpUi(JSONObject object) {

        userType = MyApplication.getSharedPrefString(StaticData.SP_REALM);
        name = MyApplication.getSharedPrefString(StaticData.SP_USER_NAME);
        email = MyApplication.getSharedPrefString(StaticData.SP_EMAIL);
        mobile = MyApplication.getSharedPrefString(StaticData.SP_MOBILE);

        edtName.setText(object.optString("name"));
        edtMobile.setText(object.optString("mobile"));
        edtEmail.setText(object.optString("email"));

        if(userType.equalsIgnoreCase("driver")) {
            ll_vehicle_pref.setVisibility(View.VISIBLE);

            if(object.optBoolean("isVehicleAlloted")) {
                try {
                    edt_vehicle_type.setVisibility(View.VISIBLE);
                    edt_vehicle_type.setText(object.optJSONObject("vehicle").optString("type"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                stringArray = new ArrayList<String>();
                JSONArray jsonArray = object.getJSONArray("canDrive");
                for (int i = 0; i < jsonArray.length(); i++) {
                    stringArray.add(jsonArray.getString(i));
                    CheckBox checkBox = new CheckBox(getActivity());
                    checkBox.setText(stringArray.get(i));
                    checkBox.setPadding(5,5,5,5);
                    checkBox.setChecked(true);
                    checkBox.setClickable(false);
                    checkBox.setFocusable(false);
                    ll_checkbox_main.addView(checkBox);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            edt_vehicle_type.setVisibility(View.GONE);
        }

        if (userType.equalsIgnoreCase("owner")) {
            edtCompany.setVisibility(View.VISIBLE);
            companyName = MyApplication.getSharedPrefString(StaticData.SP_COMPANY_NAME);
            if (!TextUtils.isEmpty(companyName)) {
                edtCompany.setText(companyName);
            }
        } else {
            edtCompany.setVisibility(View.GONE);
        }

        edtEmail.setEnabled(false);
        edtEmail.setFocusable(false);
        edtMobile.setEnabled(false);
        edtMobile.setFocusable(false);
        edt_vehicle_type.setEnabled(false);
        edt_vehicle_type.setFocusable(false);

        userRating.setRating((float) object.optJSONObject("rating").optInt("avgRating"));

        if (!object.optString("profileImage").isEmpty()) {
            String imagePath = ApiService.BASE_URL_IMAGE + object.optString("profileImage");
            Log.d("ImagePath", imagePath);
            Glide.with(getActivity())
                    .load(imagePath)
                    .centerCrop()
                    .into(imgProfilePic);
        }
    }


    private boolean isValid() {
        if (TextUtils.isEmpty(edtName.getText())) {
            edtName.setError(StaticData.BLANK);
            return false;
        }
        if (edtName.getText().toString().trim().matches("")) {
            edtName.setError(StaticData.BLANK);
            return false;
        }
        if (edtCompany.getText().toString().trim().matches("")) {
            edtCompany.setError(StaticData.BLANK);
            return false;
        }
        if (userType.equalsIgnoreCase("owner")) {
            if (TextUtils.isEmpty(edtCompany.getText())) {
                edtCompany.setError(StaticData.BLANK);
                return false;
            }
        }
        if (TextUtils.isEmpty(edtMobile.getText())) {
            edtMobile.setError(StaticData.BLANK);
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText())) {
            edtEmail.setError(StaticData.BLANK);
            return false;
        }

        /*if (edtMobile.length() != 10) {
            edtMobile.setError("Invalid mobile no.");
            return false;
        }*/

        if (!MyApplication.isEmailValid(edtEmail.getText().toString())) {
            edtEmail.setError("Invalid Email address");
            return false;
        }
        return true;
    }

    private void editProfile() {
        JSONObject data = new JSONObject();
        try {
            data.put("realm", MyApplication.getSharedPrefString(StaticData.SP_REALM));
            data.put("name", edtName.getText().toString());
            data.put("email", edtEmail.getText().toString());
            data.put("mobile", edtMobile.getText().toString());
            if (userType.equalsIgnoreCase("owner")) {
                data.put("companyName", edtCompany.getText().toString());
            }
            data.put("mobile", edtMobile.getText().toString());

            Log.d("EditProfileParam", data.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        ApiService.getInstance(getActivity()).makePostCallAuth(getActivity(), ApiService.EDIT_PROFILE,
                data,
                new ApiService.OnResponse() {
                    @Override
                    public void onResponseSuccess(JSONObject response) {
                        Log.d("UpdateProfileRes", response.toString());
                        JSONObject data = response.optJSONObject("success").optJSONObject("data");
                        MyApplication.saveLogInInfo(getActivity(), data);
                    }

                    @Override
                    public void onError(VolleyError volleyError) {

                    }
                });

    }

    private void openGallery(int PICK_IMAGE) {
        MarshMallowPermission marshMallowPermission = new MarshMallowPermission(getActivity());
        if (marshMallowPermission.checkPermissionForREAD_EXTERNAL_STORAGE()) {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        } else {
            marshMallowPermission.requestPermissionForREAD_EXTERNAL_STORAGE();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK && data.getData() != null) {
            Uri selectedImage = data.getData();
            try {
                if (requestCode == PICK_PROFILE_PIC) {
                    imgDelete.setVisibility(View.VISIBLE);
                    bmpProfile = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    Glide.with(getActivity())
                            .loadFromMediaStore(selectedImage)
                            .centerCrop()
                            .into(imgProfilePic);
                }
            } catch (IOException error) {
                error.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }

    @OnClick({R.id.img_profile_pic, R.id.img_delete, R.id.txt_submit})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_profile_pic:
                openGallery(PICK_PROFILE_PIC);
                break;
            case R.id.img_delete:
                if (!TextUtils.isEmpty(MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE))) {
                    Glide.with(getActivity())
                            .load((MyApplication.getSharedPrefString(StaticData.SP_USER_IMAGE)))
                            .centerCrop()
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .placeholder(R.drawable.ic_dummy_profile)
                            .into(imgProfilePic);
                }
                imgDelete.setVisibility(View.GONE);
                bmpProfile = null;
                break;
            case R.id.txt_submit:

                if (isValid()) {
                    if (MyApplication.isConnectingToInternet(getActivity())) {
                        if (bmpProfile != null)
                            uploadProfilePic();
                        else
                            editProfile();
                    } else {
                        Toast.makeText(getActivity(), "Please Check Your Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    private void uploadProfilePic() {
        Map<String, DataPart> params = new HashMap<>();
        if (bmpProfile != null) {
            params.put("profileImage", new DataPart(edtName.getText() + "_ProfileImage" + ".jpg", MyApplication.getBytesFromBitmap(bmpProfile), "image/jpeg"));
        }
        ApiService.getInstance(getActivity()).multipartCall(getActivity(), ApiService.UPLOAD_PROFILE_PIC, params, new ApiService.OnResponse() {
            @Override
            public void onResponseSuccess(JSONObject response) {
                editProfile();
            }

            @Override
            public void onError(VolleyError volleyError) {

            }
        });
    }

}
